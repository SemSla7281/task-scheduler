﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Tasks.Client.Model
{
    class Task
    {
        public string Name { set; get; }
        public string About { set; get; }
        public bool Enable { set; get; }
        public BindingList<Trigger> Triggers { set; get; }
        public BindingList<Action> Actions { set; get; }

        public Task()
        {
            Triggers = new BindingList<Trigger>();
            Actions = new BindingList<Action>();
            Name = "";
            About = "";
            Enable = true;
        }
    }
}
