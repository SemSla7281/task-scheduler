﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Tasks.Client.Model
{
    public enum TriggerType
    { 
        Oneshot = 0,
        Daily = 1,
        Weekly = 2,
        Monthly = 3
    }

    public enum TriggerMonthType
    {
        WeekDays = 0,
        MonthDays = 1
    }

    public class Trigger
    {
        public bool Enable { get; set; }
        public TriggerType Type { get; set; }
        public TriggerMonthType MonthType { get; set; }
        public DateTime Date { get; set; }
        public DateTime Time { get; set; }
        public bool[] Months { get; set; }
        public bool[] WeekDays { get; set; }
        public bool[] MonthDays { get; set; }
        public bool[] WeekMonthlyDays { get; set; }
        public int Frequancy { set; get; }


        public string Deteil
        {
            get 
            {
                string data = "";
                switch (Type)
                {
                    case TriggerType.Oneshot:
                        data += "Дата: " + Date.ToString("dd.MM.yyyy");
                        data += " Время: " + Time.ToString("HH:mm");
                        break;
                    case TriggerType.Daily:
                        if (Frequancy > 0)
                            data +=  $"Каждые: {Frequancy} дня.";
                        data += " Время: " + Time.ToString("HH:mm");
                        break;
                    case TriggerType.Weekly:
                        if (Frequancy > 0)
                            data += $"Каждые: {Frequancy} недели.";
                        data += " Дни: ";
                        for (int i = 0; i < 7; i++)
                            if (WeekDays[i])
                                data += new string[] { "ПН", "ВТ", "СР", "ТЧ", "ПТ", "CБ", "ВС" }[i] + ", ";
                        data = data.Substring(0, data.Length - 2);
                        data += ".";
                        data += " Время: " + Time.ToString("HH:mm");
                        break;
                    case TriggerType.Monthly:
                        data += " Месяцы: ";
                        for (int i = 0; i < 12; i++)
                            if (Months[i])
                                data += new string[] { "Янв.", "Фев.", "Мар.", "Апр.", "Май.", "Июн.",
                                        "Июл.", "Авг.", "Сен.", "Окт.", "Ноя.", "Дек." }[i] + ", ";
                        data = data.Substring(0, data.Length - 2);
                        data += ".";
                        if (MonthType == TriggerMonthType.WeekDays)
                        {
                            data += " Дни недели: ";
                            for (int i = 0; i < 7; i++)
                                if (WeekMonthlyDays[i])
                                    data += new string[] { "ПН", "ВТ", "СР", "ТЧ", "ПТ", "CБ", "ВС" }[i] + ", ";
                            data = data.Substring(0, data.Length - 2);
                            data += ".";
                        }
                        else
                        {
                            data += " Дни месяца: ";
                            for (int i = 0; i < 31; i++)
                                if (MonthDays[i])
                                    data += i.ToString() + ", ";
                            data = data.Substring(0, data.Length - 2);
                            data += ".";
                        }
                        data += " Время: " + Time.ToString("HH:mm");
                        break;
                }
                return data;
            }
        }

        public Trigger()
        {
            Enable = true;
            Type = TriggerType.Oneshot;
            MonthType = TriggerMonthType.MonthDays;
            Date = DateTime.Now;
            Time = DateTime.Now;
            Months = Enumerable.Repeat(false, 12).ToArray();
            WeekDays = Enumerable.Repeat(false, 7).ToArray();
            WeekMonthlyDays = Enumerable.Repeat(false, 7).ToArray();
            MonthDays = Enumerable.Repeat(false, 31).ToArray();
            Frequancy = 0;
        }
    }
}
