﻿namespace Tasks.Client
{
    public enum ActionType
    { 
        Binary = 0,
        Script = 1
    }

    public class Action
    {
        public bool Enable { get; set; }
        public string Path { get; set; }
        public string Arguments { get; set; }
        public ActionType ActionType { get; set; }
        public bool Wait { get; set; }
        public bool Show { get; set; }
        public bool AsUser { get; set; }
        public string User { get; set; }
        public string Domain { get; set; }
        public string Password { get; set; }
    }
}
