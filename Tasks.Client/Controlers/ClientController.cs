﻿using System;
using System.Net.Sockets;
using System.Text;
using Tasks.Client.Model;

namespace Tasks.Client.Controls
{
    public enum Command : int
    { 
        GetTaskList = 0,
        AddTask = 1,
        EditTask = 2,
        DeleteTask = 3,
        TakeTaskList = 4,
        GetTaskByName = 5,
        TakeTask = 6,
        TaskAdded = 7,
        TaskEdited = 8,
        TaskDeleted = 9,
        TaskNotAdded = 10,
        TaskNotEdited = 11,
        TaskNotDeleted = 12 
    }

    class ClientController : TcpClient
    {
        public Result<bool> AddTaskOnServer(Task task)
        {
            var command = SendCommand(Command.AddTask);
            if (!command.Success)
                return new Result<bool>(command.Exception, $"Error sending task to server (command)");
            var taskSecded = SendTask(task);
            if (!taskSecded.Success)
                return new Result<bool>(taskSecded.Exception, $"Error sending task to server (task)");
            return new Result<bool>(true);
        }

        public Result<bool> EditTaskOnServer(string name, Task task)
        {
            var command = SendCommand(Command.EditTask);
            if (!command.Success)
                return new Result<bool>(command.Exception, $"Error sending task to server (command)");
            var nameSended = SendString(name);
            if (!nameSended.Success)
                return new Result<bool>(nameSended.Exception, $"Error sending task to server (name)");
            var taskSecded = SendTask(task);
            if (!taskSecded.Success)
                return new Result<bool>(taskSecded.Exception, $"Error sending task to server (task)");
            return new Result<bool>(true);
        }

        public Result<bool> DeleteTaskOnServer(string name)
        {
            var command = SendCommand(Command.DeleteTask);
            if (!command.Success)
                return new Result<bool>(command.Exception, $"Error sending task to server (command)");
            var nameSended = SendString(name);
            if (!nameSended.Success)
                return new Result<bool>(nameSended.Exception, $"Error sending task to server (name)");
            return new Result<bool>(true);
        }

        public Result<Command> AcceptCommand()
        {
            try
            {
                var stream = GetStream();
                var bytes = new byte[sizeof(int)];
                stream.Read(bytes, 0, sizeof(int));
                var value = BitConverter.ToInt32(bytes, 0);
                return new Result<Command>((Command)value);
            }
            catch (Exception exception)
            {
                return new Result<Command>(exception, " Command accepting error");
            }
        }

        public Result<bool> SendCommand(Command data)
        {
            try
            {
                var stream = GetStream();
                var bytes = BitConverter.GetBytes((int)data);
                stream.Write(bytes, 0, sizeof(int));
                return new Result<bool>(true);
            }
            catch (Exception exception)
            {
                return new Result<bool>(exception);
            }
        }

        public Result<bool> SendDateTime(DateTime data)
        {
            var sendSize = SendInt(data.Year);
            if (!sendSize.Success)
                return new Result<bool>(sendSize.Exception, $"Sending datetime error ({sendSize.ErrorMessage})");
            sendSize = SendInt(data.Month);
            if (!sendSize.Success)
                return new Result<bool>(sendSize.Exception, $"Sending datetime error ({sendSize.ErrorMessage})");
            sendSize = SendInt(data.Day);
            if (!sendSize.Success)
                return new Result<bool>(sendSize.Exception, $"Sending datetime error ({sendSize.ErrorMessage})");
            sendSize = SendInt(data.Hour);
            if (!sendSize.Success)
                return new Result<bool>(sendSize.Exception, $"Sending datetime error ({sendSize.ErrorMessage})");
            sendSize = SendInt(data.Minute);
            if (!sendSize.Success)
                return new Result<bool>(sendSize.Exception, $"Sending datetime error ({sendSize.ErrorMessage})");
            sendSize = SendInt(data.Second);
            if (!sendSize.Success)
                return new Result<bool>(sendSize.Exception, $"Sending datetime error ({sendSize.ErrorMessage})");
            return new Result<bool>(true);
        }

        public Result<DateTime> AcceptDateTime()
        {
            var year = AcceptInt();
            if (!year.Success)
                return new Result<DateTime>(year.Exception, $"Accepting datetime error (accepting year error)");
            var month = AcceptInt();
            if (!month.Success)
                return new Result<DateTime>(month.Exception, $"Accepting datetime error (accepting month error)");
            var day = AcceptInt();
            if (!day.Success)
                return new Result<DateTime>(day.Exception, $"Accepting datetime error (accepting day error)");
            var hour = AcceptInt();
            if (!hour.Success)
                return new Result<DateTime>(hour.Exception, $"Accepting datetime error (accepting hour error)");
            var minute = AcceptInt();
            if (!minute.Success)
                return new Result<DateTime>(minute.Exception, $"Accepting datetime error (accepting minute error)");
            var second = AcceptInt();
            if (!second.Success)
                return new Result<DateTime>(second.Exception, $"Accepting datetime error (accepting second error)");
            return new Result<DateTime>(new DateTime(year.Value, month.Value, day.Value, hour.Value, minute.Value, second.Value));
        }

        public Result<bool> SendString(string data)
        {
            var bytes = Encoding.Unicode.GetBytes(data);
            var sendSize = SendInt(bytes.Length);
            if (sendSize.Success)
            {
                try
                {
                    var stream = GetStream();
                    if (bytes.Length > 0)
                        stream.Write(bytes, 0, bytes.Length);
                    return new Result<bool>(true);
                }
                catch (Exception exception)
                {
                    return new Result<bool>(exception, "String sending error");
                }
            }
            else
                return new Result<bool>(sendSize.Exception, $"String sending error ({sendSize.ErrorMessage})");
        }

        public Result<string> AcceptString()
        {
            var acceptSize = AcceptInt();
            if (acceptSize.Success)
            {
                try
                {
                    var bytes = new byte[acceptSize.Value];
                    var stream = GetStream();
                    if (acceptSize.Value > 0)
                        stream.Read(bytes, 0, bytes.Length);
                    var data = Encoding.Unicode.GetString(bytes);
                    return new Result<string>(data);
                }
                catch (Exception exception)
                {
                    return new Result<string>(exception, "String accepting error");
                }
            }
            else
                return new Result<string>(acceptSize.Exception, $"String accepting error({acceptSize.ErrorMessage})");
        }

        public Result<bool> SendInt(int data)
        {
            try
            {
                var stream = GetStream();
                var bytes = BitConverter.GetBytes(data);
                stream.Write(bytes, 0, sizeof(int));
                return new Result<bool>(true);
            }
            catch (Exception exception)
            {
                return new Result<bool>(exception);
            }
        }

        public Result<int> AcceptInt()
        {
            try
            {
                var stream = GetStream();
                var bytes = new byte[sizeof(int)];
                stream.Read(bytes, 0, sizeof(int));
                var value = BitConverter.ToInt32(bytes, 0);
                return new Result<int>(value);
            }
            catch (Exception exception)
            {
                return new Result<int>(exception, "Bool accepting error");
            }
        }

        public Result<bool> AcceptBool()
        {
            try
            {
                var stream = GetStream();
                var bytes = new byte[1];
                stream.Read(bytes, 0, 1);
                var value = BitConverter.ToBoolean(bytes, 0);
                return new Result<bool>(value);
            }
            catch (Exception exception)
            {
                return new Result<bool>(exception, "Int accepting error");
            }
        }

        public Result<bool> SendBool(bool data)
        {
            try
            {
                var stream = GetStream();
                var bytes = BitConverter.GetBytes(data);
                stream.Write(bytes, 0, 1);
                return new Result<bool>(true);
            }
            catch (Exception exception)
            {
                return new Result<bool>(exception, "Bool sending error");
            }
        }

        public Result<bool> SendTrigger(Trigger trigger)
        {
            var enable = SendBool(trigger.Enable);
            if (!enable.Success)
                return new Result<bool>(enable.Exception, $"Sending trigger error (enable)");

            var type = SendInt((int)trigger.Type);
            if (!type.Success)
                return new Result<bool>(type.Exception, $"Sending trigger error (type)");

            var monthType = SendInt((int)trigger.MonthType);
            if (!monthType.Success)
                return new Result<bool>(monthType.Exception, $"Sending trigger error (month type)");

            var time = SendDateTime(trigger.Time);
            if (!time.Success)
                return new Result<bool>(time.Exception, $"Sending trigger error (time)");

            var date = SendDateTime(trigger.Date);
            if (!date.Success)
                return new Result<bool>(date.Exception, $"Sending trigger error (date)");

            var frequancy = SendInt(trigger.Frequancy);
            if (!frequancy.Success)
                return new Result<bool>(frequancy.Exception, $"Sending trigger error (frequancy)");

            for (int i = 0; i < 7; i++)
            {
                var weekDay = SendBool(trigger.WeekDays[i]);
                if (!weekDay.Success)
                    return new Result<bool>(weekDay.Exception, $"Sending trigger error (week day ({i}))");
            }

            for (int i = 0; i < 7; i++)
            {
                var weekMonthlyDay = SendBool(trigger.WeekMonthlyDays[i]);
                if (!weekMonthlyDay.Success)
                    return new Result<bool>(weekMonthlyDay.Exception, $"Sending trigger error (week day monthly ({i}))");
            }

            for (int i = 0; i < 31; i++)
            {
                var monthDay = SendBool(trigger.MonthDays[i]);
                if (!monthDay.Success)
                    return new Result<bool>(monthDay.Exception, $"Sending trigger error (month day ({i}))");
            }

            for (int i = 0; i < 12; i++)
            {
                var month = SendBool(trigger.Months[i]);
                if (!month.Success)
                    return new Result<bool>(month.Exception, $"Sending trigger error (month ({i}))");
            }
            return new Result<bool>(true);
        }

        public Result<Trigger> AcceptTrigger()
        {
            Trigger trigger = new Trigger();

            var enable = AcceptBool();
            if (!enable.Success)
                return new Result<Trigger>(enable.Exception, $"Accepting trigger error (enable)");
            trigger.Enable = enable.Value;

            var type = AcceptInt();
            if (!type.Success)
                return new Result<Trigger>(type.Exception, $"Accepting trigger error (type)");
            trigger.Type = (TriggerType)type.Value;

            var monthType = AcceptInt();
            if (!monthType.Success)
                return new Result<Trigger>(monthType.Exception, $"Accepting trigger error (month type)");
            trigger.MonthType = (TriggerMonthType)monthType.Value;

            var time = AcceptDateTime();
            if (!time.Success)
                return new Result<Trigger>(time.Exception, $"Accepting trigger error (time)");
            trigger.Time = time.Value;

            var date = AcceptDateTime();
            if (!date.Success)
                return new Result<Trigger>(date.Exception, $"Accepting trigger error (date)");
            trigger.Date = date.Value;

            var frequancy = AcceptInt();
            if (!frequancy.Success)
                return new Result<Trigger>(frequancy.Exception, $"Accepting trigger error (frequancy)");
            trigger.Frequancy = frequancy.Value;

            trigger.WeekDays = new bool[7];
            for (int i = 0; i < 7; i++)
            {
                var weekDay = AcceptBool();
                if (!weekDay.Success)
                    return new Result<Trigger>(weekDay.Exception, $"Accepting trigger error (week day ({i}))");
                trigger.WeekDays[i] = weekDay.Value;
            }

            trigger.WeekMonthlyDays = new bool[7];
            for (int i = 0; i < 7; i++)
            {
                var weekDayMonthly = AcceptBool();
                if (!weekDayMonthly.Success)
                    return new Result<Trigger>(weekDayMonthly.Exception, $"Accepting trigger error (week day monthly ({i}))");
                trigger.WeekMonthlyDays[i] = weekDayMonthly.Value;
            }

            trigger.MonthDays = new bool[31];
            for (int i = 0; i < 31; i++)
            {
                var monthDay = AcceptBool();
                if (!monthDay.Success)
                    return new Result<Trigger>(monthDay.Exception, $"Accepting trigger error (month day ({i}))");
                trigger.MonthDays[i] = monthDay.Value;
            }

            trigger.Months = new bool[12];
            for (int i = 0; i < 12; i++)
            {
                var month = AcceptBool();
                if (!month.Success)
                    return new Result<Trigger>(month.Exception, $"Accepting trigger error (month ({i}))");
                trigger.Months[i] = month.Value;
            }

            return new Result<Trigger>(trigger);
        }

        public Result<bool> SendAction(Action action)
        {
            var enable = SendBool(action.Enable);
            if (!enable.Success)
                return new Result<bool>(enable.Exception, $"Sending action error (enable)");

            var type = SendInt((int)action.ActionType);
            if (!type.Success)
                return new Result<bool>(type.Exception, $"Sending action error (type)");

            var path = SendString(action.Path);
            if (!path.Success)
                return new Result<bool>(path.Exception, $"Sending action error (path)");

            var arguments = SendString(action.Arguments);
            if (!arguments.Success)
                return new Result<bool>(arguments.Exception, $"Sending action error (arguments)");

            var wait = SendBool(action.Wait);
            if (!wait.Success)
                return new Result<bool>(wait.Exception, $"Sending action error (wait)");

            var show = SendBool(action.Show);
            if (!show.Success)
                return new Result<bool>(show.Exception, $"Sending action error (show)");

            var asUser = SendBool(action.AsUser);
            if (!asUser.Success)
                return new Result<bool>(asUser.Exception, $"Sending action error (as user)");

            var user = SendString(action.User);
            if (!user.Success)
                return new Result<bool>(path.Exception, $"Sending action error (user)");

            var domain = SendString(action.Domain);
            if (!domain.Success)
                return new Result<bool>(domain.Exception, $"Sending action error (domain)");

            var password = SendString(action.Password);
            if (!password.Success)
                return new Result<bool>(password.Exception, $"Sending action error (password)");

            return new Result<bool>(true);
        }

        public Result<Action> AcceptAction()
        {
            Action action = new Action();

            var enable = AcceptBool();
            if (!enable.Success)
                return new Result<Action>(enable.Exception, $"Accepting action error (enable)");
            action.Enable = enable.Value;

            var type = AcceptInt();
            if (!type.Success)
                return new Result<Action>(type.Exception, $"Accepting action error (type)");
            action.ActionType = (ActionType)type.Value;

            var path = AcceptString();
            if (!path.Success)
                return new Result<Action>(path.Exception, $"Accepting action error (path)");
            action.Path = path.Value;

            var arguments = AcceptString();
            if (!arguments.Success)
                return new Result<Action>(arguments.Exception, $"Accepting action error (arguments)");
            action.Arguments = arguments.Value;

            var wait = AcceptBool();
            if (!wait.Success)
                return new Result<Action>(wait.Exception, $"Accepting action error (frequancy)");
            action.Wait = wait.Value;

            var show = AcceptBool();
            if (!show.Success)
                return new Result<Action>(show.Exception, $"Accepting action error (show)");
            action.Show = show.Value;

            var asUser = AcceptBool();
            if (!asUser.Success)
                return new Result<Action>(asUser.Exception, $"Accepting action error (as user)");
            action.AsUser = asUser.Value;

            var user = AcceptString();
            if (!user.Success)
                return new Result<Action>(user.Exception, $"Accepting action error (user)");
            action.User = user.Value;

            var domain = AcceptString();
            if (!domain.Success)
                return new Result<Action>(domain.Exception, $"Accepting action error (domain)");
            action.Domain = domain.Value;

            var password = AcceptString();
            if (!password.Success)
                return new Result<Action>(password.Exception, $"Accepting action error (password)");
            action.Password = password.Value;

            return new Result<Action>(action);
        }

        public Result<bool> SendTask(Task task)
        {
            var enable = SendBool(task.Enable);
            if (!enable.Success)
                return new Result<bool>(enable.Exception, $"Sending task error (enable)");

            var name = SendString(task.Name);
            if (!name.Success)
                return new Result<bool>(name.Exception, $"Sending task error (name)");

            var about = SendString(task.About); 
            if (!about.Success)
                return new Result<bool>(about.Exception, $"Sending task error (about)");

            var triggersSize = SendInt(task.Triggers.Count);
            if (!triggersSize.Success)
                return new Result<bool>(triggersSize.Exception, $"Sending task error (triggers size)");

            for (int i = 0; i < task.Triggers.Count; i++)
            {
                var trigger = SendTrigger(task.Triggers[i]);
                if (!trigger.Success)
                    return new Result<bool>(trigger.Exception, $"Sending task error (trigger ({i}))");
            }

            var actionSize = SendInt(task.Actions.Count);
            if (!actionSize.Success)
                return new Result<bool>(actionSize.Exception, $"Sending task error (actions size)");

            for (int i = 0; i < task.Actions.Count; i++)
            {
                var action = SendAction(task.Actions[i]);
                if (!action.Success)
                    return new Result<bool>(action.Exception, $"Sending task error (action ({i}))");
            }

            return new Result<bool>(true);
        }

        public Result<Task> AcceptTask()
        {
            Task task = new Task();

            var enable = AcceptBool();
            if (!enable.Success)
                return new Result<Task>(enable.Exception, $"Accepting task error (enable)");
            task.Enable = enable.Value;

            var name = AcceptString();
            if (!name.Success)
                return new Result<Task>(name.Exception, $"Accepting task error (name)");
            task.Name = name.Value;

            var about = AcceptString();
            if (!about.Success)
                return new Result<Task>(about.Exception, $"Accepting task error (about)");
            task.About = about.Value;

            var triggersSize = AcceptInt();
            if (!triggersSize.Success)
                return new Result<Task>(triggersSize.Exception, $"Accepting task error (triggers size)");

            for (int i = 0; i < triggersSize.Value; i++)
            {
                var trigger = AcceptTrigger();
                if (!trigger.Success)
                    return new Result<Task>(trigger.Exception, $"Accepting task error (trigger ({i}))");
                task.Triggers.Add(trigger.Value);
            }

            var actionSize = AcceptInt();
            if (!actionSize.Success)
                return new Result<Task>(actionSize.Exception, $"Accepting task error (actions size)");

            for (int i = 0; i < actionSize.Value; i++)
            {
                var action = AcceptAction();
                if (!action.Success)
                    return new Result<Task>(action.Exception, $"Accepting task error (action ({i}))");
                task.Actions.Add(action.Value);
            }

            return new Result<Task>(task);
        }
    }
}
