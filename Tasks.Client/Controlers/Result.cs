﻿using System;

namespace Tasks.Client.Controls
{
    class Result<TypeResult>
    {
        public TypeResult Value { set; get; }
        public bool Success { set; get; }
        public Exception Exception { set; get; }
        public string ErrorMessage { set; get; }

        public Result()
        {
            Success = false;
            Exception = null;
        }

        public Result(TypeResult value)
        {
            Value = value;
            Success = true;
            Exception = null;
        }

        public Result(Exception exception, string message = "")
        {
            Success = false;
            ErrorMessage = message;
            Exception = exception;
        }
    }
}
