﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tasks.Client
{
    public partial class ActionForm : Form
    {
        public Action Action { private set; get; }

        public ActionForm()
        {
            InitializeComponent();
            Action = null;
        }

        public ActionForm(Action action)
        {
            InitializeComponent();
            PathTextBox.Text = action.Path;
            BinaryRadioButton.Checked = action.ActionType == ActionType.Binary;
            ScriptRadioButton.Checked = action.ActionType == ActionType.Script;
            ArgumentsTextBox.Text = action.Arguments;
            WaitCheckBox.Checked = action.Wait;
            ShowWindowsCheckBox.Checked = action.Show;
            AsUserCheckBox.Checked = action.AsUser;
            UserTextBox.Text = action.User;
            DomainTextBox.Text = action.Domain;
            PasswordTextBox.Text = action.Password;
            AddButton.Text = "Изменить";
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(PathTextBox.Text))
                MessageBox.Show("Введите полный путь к файлу", "Некорректный ввод", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else if (string.IsNullOrEmpty(UserTextBox.Text) && AsUserCheckBox.Checked)
                MessageBox.Show("Введите имя пользователя!", "Некорректный ввод", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
            {
                Action = new Action
                {
                    Enable = true,
                    Path = PathTextBox.Text,
                    Arguments = ArgumentsTextBox.Text,
                    ActionType = BinaryRadioButton.Checked ?
                        ActionType.Binary : ActionType.Script,
                    Wait = WaitCheckBox.Checked,
                    Show = ShowWindowsCheckBox.Checked,
                    AsUser = AsUserCheckBox.Checked,
                    User = UserTextBox.Text,
                    Domain = DomainTextBox.Text,
                    Password = PasswordTextBox.Text
                };
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void ChangeFileButton_Click(object sender, EventArgs e)
        {
            using (var form = new OpenFileDialog() { Filter = "Все файлы (*.*)|*.*" })
            {
                if (form.ShowDialog() == DialogResult.OK)
                    PathTextBox.Text = form.FileName;
            }
        }

        private void AsUserCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            UserTextBox.Enabled = AsUserCheckBox.Checked;
            PasswordTextBox.Enabled = AsUserCheckBox.Checked;
            DomainTextBox.Enabled = AsUserCheckBox.Checked;
        }
    }
}
