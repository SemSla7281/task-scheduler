﻿
namespace Tasks.Client
{
    partial class TriggerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TriggerTypeLabel = new System.Windows.Forms.Label();
            this.OneshotRadioButton = new System.Windows.Forms.RadioButton();
            this.WeeklyRadioButton = new System.Windows.Forms.RadioButton();
            this.DailyRadioButton = new System.Windows.Forms.RadioButton();
            this.MonthlyRadioButton = new System.Windows.Forms.RadioButton();
            this.OneshotPanel = new System.Windows.Forms.Panel();
            this.OneshotTimePicker = new System.Windows.Forms.DateTimePicker();
            this.OneshotTimeLabel = new System.Windows.Forms.Label();
            this.OneshotDateLabel = new System.Windows.Forms.Label();
            this.OneshotDatePicker = new System.Windows.Forms.DateTimePicker();
            this.AddButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.DailyPanel = new System.Windows.Forms.Panel();
            this.DaysDailyLabel = new System.Windows.Forms.Label();
            this.FreauncyDailyLabel = new System.Windows.Forms.Label();
            this.FreauncyDailyNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.DailyTimePicker = new System.Windows.Forms.DateTimePicker();
            this.DailyTimeLabel = new System.Windows.Forms.Label();
            this.WeeklyPanel = new System.Windows.Forms.Panel();
            this.WeekDaysLabel = new System.Windows.Forms.Label();
            this.SundayCheckBox = new System.Windows.Forms.CheckBox();
            this.SaturdayCheckBox = new System.Windows.Forms.CheckBox();
            this.FridayCheckBox = new System.Windows.Forms.CheckBox();
            this.ThursdayCheckBox = new System.Windows.Forms.CheckBox();
            this.WednesdayCheckbox = new System.Windows.Forms.CheckBox();
            this.TuesdayCheckbox = new System.Windows.Forms.CheckBox();
            this.MondayCheckBox = new System.Windows.Forms.CheckBox();
            this.WeeksWeeklyLabel = new System.Windows.Forms.Label();
            this.FreauncyWeeklyLabel = new System.Windows.Forms.Label();
            this.FrequancyWeekNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.WeeklyTimePicker = new System.Windows.Forms.DateTimePicker();
            this.TimeDailyLabel = new System.Windows.Forms.Label();
            this.MonthlyPanel = new System.Windows.Forms.Panel();
            this.MayСheckBox = new System.Windows.Forms.CheckBox();
            this.AprileCheckBox = new System.Windows.Forms.CheckBox();
            this.MonthlyTimePicker = new System.Windows.Forms.DateTimePicker();
            this.TimeMonthlyLabel = new System.Windows.Forms.Label();
            this.MonthDaysRadioButton = new System.Windows.Forms.RadioButton();
            this.WeekDaysRadioButton = new System.Windows.Forms.RadioButton();
            this.TypeMonthlyLabel = new System.Windows.Forms.Label();
            this.MarchCheckBox = new System.Windows.Forms.CheckBox();
            this.DecemberCheckBox = new System.Windows.Forms.CheckBox();
            this.NonemberCheckBox = new System.Windows.Forms.CheckBox();
            this.OctemberCheckBox = new System.Windows.Forms.CheckBox();
            this.SeptemberCheckBox = new System.Windows.Forms.CheckBox();
            this.MonthsLabel = new System.Windows.Forms.Label();
            this.AugustCheckBox = new System.Windows.Forms.CheckBox();
            this.JuleCheckBox = new System.Windows.Forms.CheckBox();
            this.JuneCheckBox = new System.Windows.Forms.CheckBox();
            this.FebruaryCheckBox = new System.Windows.Forms.CheckBox();
            this.JanuaryCheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDaysPanel = new System.Windows.Forms.Panel();
            this.MonthDay31CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay30CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay29CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay28CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay27CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay26CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay25CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay24CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay23CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay22CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay21CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay20CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay19CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay18CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay17CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay16CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay15CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay14CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay13CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay12CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay11CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay10CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay9CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay8CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay7CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay6CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay5CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay4CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay3CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay2CheckBox = new System.Windows.Forms.CheckBox();
            this.MonthDay1CheckBox = new System.Windows.Forms.CheckBox();
            this.WeekDaysMonthlyPanel = new System.Windows.Forms.Panel();
            this.SundayMonthlyCheckBox = new System.Windows.Forms.CheckBox();
            this.SaturdayMonthlyCheckBox = new System.Windows.Forms.CheckBox();
            this.FridayMonthlyCheckBox = new System.Windows.Forms.CheckBox();
            this.ThursdayMonthlyCheckBox = new System.Windows.Forms.CheckBox();
            this.WednesdayMonthlyCheckbox = new System.Windows.Forms.CheckBox();
            this.TuesdayMonthlyCheckbox = new System.Windows.Forms.CheckBox();
            this.MondayMonthlyCheckBox = new System.Windows.Forms.CheckBox();
            this.OneshotPanel.SuspendLayout();
            this.DailyPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FreauncyDailyNumericUpDown)).BeginInit();
            this.WeeklyPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FrequancyWeekNumericUpDown)).BeginInit();
            this.MonthlyPanel.SuspendLayout();
            this.MonthDaysPanel.SuspendLayout();
            this.WeekDaysMonthlyPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // TriggerTypeLabel
            // 
            this.TriggerTypeLabel.AutoSize = true;
            this.TriggerTypeLabel.Location = new System.Drawing.Point(12, 18);
            this.TriggerTypeLabel.Name = "TriggerTypeLabel";
            this.TriggerTypeLabel.Size = new System.Drawing.Size(81, 15);
            this.TriggerTypeLabel.TabIndex = 0;
            this.TriggerTypeLabel.Text = "Тип триггера:";
            // 
            // OneshotRadioButton
            // 
            this.OneshotRadioButton.AutoSize = true;
            this.OneshotRadioButton.Checked = true;
            this.OneshotRadioButton.Location = new System.Drawing.Point(99, 18);
            this.OneshotRadioButton.Name = "OneshotRadioButton";
            this.OneshotRadioButton.Size = new System.Drawing.Size(104, 19);
            this.OneshotRadioButton.TabIndex = 1;
            this.OneshotRadioButton.TabStop = true;
            this.OneshotRadioButton.Text = "Одноразовый";
            this.OneshotRadioButton.UseVisualStyleBackColor = true;
            this.OneshotRadioButton.CheckedChanged += new System.EventHandler(this.OneshotRadioButton_CheckedChanged);
            // 
            // WeeklyRadioButton
            // 
            this.WeeklyRadioButton.AutoSize = true;
            this.WeeklyRadioButton.Location = new System.Drawing.Point(99, 43);
            this.WeeklyRadioButton.Name = "WeeklyRadioButton";
            this.WeeklyRadioButton.Size = new System.Drawing.Size(109, 19);
            this.WeeklyRadioButton.TabIndex = 2;
            this.WeeklyRadioButton.Text = "Еженедельный";
            this.WeeklyRadioButton.UseVisualStyleBackColor = true;
            this.WeeklyRadioButton.CheckedChanged += new System.EventHandler(this.WeeklyRadioButton_CheckedChanged);
            // 
            // DailyRadioButton
            // 
            this.DailyRadioButton.AutoSize = true;
            this.DailyRadioButton.Location = new System.Drawing.Point(209, 18);
            this.DailyRadioButton.Name = "DailyRadioButton";
            this.DailyRadioButton.Size = new System.Drawing.Size(97, 19);
            this.DailyRadioButton.TabIndex = 3;
            this.DailyRadioButton.Text = "Ежедневный";
            this.DailyRadioButton.UseVisualStyleBackColor = true;
            this.DailyRadioButton.CheckedChanged += new System.EventHandler(this.DailyRadioButton_CheckedChanged);
            // 
            // MonthlyRadioButton
            // 
            this.MonthlyRadioButton.AutoSize = true;
            this.MonthlyRadioButton.Location = new System.Drawing.Point(209, 43);
            this.MonthlyRadioButton.Name = "MonthlyRadioButton";
            this.MonthlyRadioButton.Size = new System.Drawing.Size(104, 19);
            this.MonthlyRadioButton.TabIndex = 4;
            this.MonthlyRadioButton.Text = "Ежемесячный";
            this.MonthlyRadioButton.UseVisualStyleBackColor = true;
            this.MonthlyRadioButton.CheckedChanged += new System.EventHandler(this.MonthlyRadioButton_CheckedChanged);
            // 
            // OneshotPanel
            // 
            this.OneshotPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OneshotPanel.Controls.Add(this.OneshotTimePicker);
            this.OneshotPanel.Controls.Add(this.OneshotTimeLabel);
            this.OneshotPanel.Controls.Add(this.OneshotDateLabel);
            this.OneshotPanel.Controls.Add(this.OneshotDatePicker);
            this.OneshotPanel.Location = new System.Drawing.Point(-1, 68);
            this.OneshotPanel.Name = "OneshotPanel";
            this.OneshotPanel.Size = new System.Drawing.Size(322, 42);
            this.OneshotPanel.TabIndex = 5;
            // 
            // OneshotTimePicker
            // 
            this.OneshotTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.OneshotTimePicker.Location = new System.Drawing.Point(201, 9);
            this.OneshotTimePicker.Name = "OneshotTimePicker";
            this.OneshotTimePicker.ShowUpDown = true;
            this.OneshotTimePicker.Size = new System.Drawing.Size(106, 23);
            this.OneshotTimePicker.TabIndex = 10;
            // 
            // OneshotTimeLabel
            // 
            this.OneshotTimeLabel.AutoSize = true;
            this.OneshotTimeLabel.Location = new System.Drawing.Point(150, 14);
            this.OneshotTimeLabel.Name = "OneshotTimeLabel";
            this.OneshotTimeLabel.Size = new System.Drawing.Size(45, 15);
            this.OneshotTimeLabel.TabIndex = 9;
            this.OneshotTimeLabel.Text = "Время:";
            // 
            // OneshotDateLabel
            // 
            this.OneshotDateLabel.AutoSize = true;
            this.OneshotDateLabel.Location = new System.Drawing.Point(13, 14);
            this.OneshotDateLabel.Name = "OneshotDateLabel";
            this.OneshotDateLabel.Size = new System.Drawing.Size(36, 15);
            this.OneshotDateLabel.TabIndex = 8;
            this.OneshotDateLabel.Text = "Дата:";
            // 
            // OneshotDatePicker
            // 
            this.OneshotDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.OneshotDatePicker.Location = new System.Drawing.Point(55, 9);
            this.OneshotDatePicker.Name = "OneshotDatePicker";
            this.OneshotDatePicker.Size = new System.Drawing.Size(89, 23);
            this.OneshotDatePicker.TabIndex = 0;
            // 
            // AddButton
            // 
            this.AddButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.AddButton.Location = new System.Drawing.Point(238, 116);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(75, 23);
            this.AddButton.TabIndex = 6;
            this.AddButton.Text = "Добавить";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelButton.Location = new System.Drawing.Point(157, 116);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(75, 23);
            this.CancelButton.TabIndex = 7;
            this.CancelButton.Text = "Отмена";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // DailyPanel
            // 
            this.DailyPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DailyPanel.Controls.Add(this.DaysDailyLabel);
            this.DailyPanel.Controls.Add(this.FreauncyDailyLabel);
            this.DailyPanel.Controls.Add(this.FreauncyDailyNumericUpDown);
            this.DailyPanel.Controls.Add(this.DailyTimePicker);
            this.DailyPanel.Controls.Add(this.DailyTimeLabel);
            this.DailyPanel.Location = new System.Drawing.Point(-1, 68);
            this.DailyPanel.Name = "DailyPanel";
            this.DailyPanel.Size = new System.Drawing.Size(322, 42);
            this.DailyPanel.TabIndex = 11;
            // 
            // DaysDailyLabel
            // 
            this.DaysDailyLabel.AutoSize = true;
            this.DaysDailyLabel.Location = new System.Drawing.Point(120, 14);
            this.DaysDailyLabel.Name = "DaysDailyLabel";
            this.DaysDailyLabel.Size = new System.Drawing.Size(34, 15);
            this.DaysDailyLabel.TabIndex = 13;
            this.DaysDailyLabel.Text = "дней";
            // 
            // FreauncyDailyLabel
            // 
            this.FreauncyDailyLabel.AutoSize = true;
            this.FreauncyDailyLabel.Location = new System.Drawing.Point(13, 13);
            this.FreauncyDailyLabel.Name = "FreauncyDailyLabel";
            this.FreauncyDailyLabel.Size = new System.Drawing.Size(40, 15);
            this.FreauncyDailyLabel.TabIndex = 12;
            this.FreauncyDailyLabel.Text = "Через";
            // 
            // FreauncyDailyNumericUpDown
            // 
            this.FreauncyDailyNumericUpDown.Location = new System.Drawing.Point(55, 9);
            this.FreauncyDailyNumericUpDown.Name = "FreauncyDailyNumericUpDown";
            this.FreauncyDailyNumericUpDown.Size = new System.Drawing.Size(62, 23);
            this.FreauncyDailyNumericUpDown.TabIndex = 11;
            // 
            // DailyTimePicker
            // 
            this.DailyTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.DailyTimePicker.Location = new System.Drawing.Point(215, 9);
            this.DailyTimePicker.Name = "DailyTimePicker";
            this.DailyTimePicker.ShowUpDown = true;
            this.DailyTimePicker.Size = new System.Drawing.Size(92, 23);
            this.DailyTimePicker.TabIndex = 10;
            // 
            // DailyTimeLabel
            // 
            this.DailyTimeLabel.AutoSize = true;
            this.DailyTimeLabel.Location = new System.Drawing.Point(164, 14);
            this.DailyTimeLabel.Name = "DailyTimeLabel";
            this.DailyTimeLabel.Size = new System.Drawing.Size(45, 15);
            this.DailyTimeLabel.TabIndex = 9;
            this.DailyTimeLabel.Text = "Время:";
            // 
            // WeeklyPanel
            // 
            this.WeeklyPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.WeeklyPanel.Controls.Add(this.WeekDaysLabel);
            this.WeeklyPanel.Controls.Add(this.SundayCheckBox);
            this.WeeklyPanel.Controls.Add(this.SaturdayCheckBox);
            this.WeeklyPanel.Controls.Add(this.FridayCheckBox);
            this.WeeklyPanel.Controls.Add(this.ThursdayCheckBox);
            this.WeeklyPanel.Controls.Add(this.WednesdayCheckbox);
            this.WeeklyPanel.Controls.Add(this.TuesdayCheckbox);
            this.WeeklyPanel.Controls.Add(this.MondayCheckBox);
            this.WeeklyPanel.Controls.Add(this.WeeksWeeklyLabel);
            this.WeeklyPanel.Controls.Add(this.FreauncyWeeklyLabel);
            this.WeeklyPanel.Controls.Add(this.FrequancyWeekNumericUpDown);
            this.WeeklyPanel.Controls.Add(this.WeeklyTimePicker);
            this.WeeklyPanel.Controls.Add(this.TimeDailyLabel);
            this.WeeklyPanel.Location = new System.Drawing.Point(-1, 68);
            this.WeeklyPanel.Name = "WeeklyPanel";
            this.WeeklyPanel.Size = new System.Drawing.Size(322, 42);
            this.WeeklyPanel.TabIndex = 14;
            this.WeeklyPanel.Visible = false;
            // 
            // WeekDaysLabel
            // 
            this.WeekDaysLabel.AutoSize = true;
            this.WeekDaysLabel.Location = new System.Drawing.Point(13, 7);
            this.WeekDaysLabel.Name = "WeekDaysLabel";
            this.WeekDaysLabel.Size = new System.Drawing.Size(76, 15);
            this.WeekDaysLabel.TabIndex = 26;
            this.WeekDaysLabel.Text = "Дни недели:";
            // 
            // SundayCheckBox
            // 
            this.SundayCheckBox.AutoSize = true;
            this.SundayCheckBox.Location = new System.Drawing.Point(277, 30);
            this.SundayCheckBox.Name = "SundayCheckBox";
            this.SundayCheckBox.Size = new System.Drawing.Size(40, 19);
            this.SundayCheckBox.TabIndex = 25;
            this.SundayCheckBox.Text = "ВС";
            this.SundayCheckBox.UseVisualStyleBackColor = true;
            this.SundayCheckBox.CheckedChanged += new System.EventHandler(this.WeekDayCheckBox_CheckedChanged);
            // 
            // SaturdayCheckBox
            // 
            this.SaturdayCheckBox.AutoSize = true;
            this.SaturdayCheckBox.Location = new System.Drawing.Point(235, 30);
            this.SaturdayCheckBox.Name = "SaturdayCheckBox";
            this.SaturdayCheckBox.Size = new System.Drawing.Size(40, 19);
            this.SaturdayCheckBox.TabIndex = 24;
            this.SaturdayCheckBox.Text = "СБ";
            this.SaturdayCheckBox.UseVisualStyleBackColor = true;
            this.SaturdayCheckBox.CheckedChanged += new System.EventHandler(this.WeekDayCheckBox_CheckedChanged);
            // 
            // FridayCheckBox
            // 
            this.FridayCheckBox.AutoSize = true;
            this.FridayCheckBox.Location = new System.Drawing.Point(191, 30);
            this.FridayCheckBox.Name = "FridayCheckBox";
            this.FridayCheckBox.Size = new System.Drawing.Size(42, 19);
            this.FridayCheckBox.TabIndex = 23;
            this.FridayCheckBox.Text = "ПТ";
            this.FridayCheckBox.UseVisualStyleBackColor = true;
            this.FridayCheckBox.CheckedChanged += new System.EventHandler(this.WeekDayCheckBox_CheckedChanged);
            // 
            // ThursdayCheckBox
            // 
            this.ThursdayCheckBox.AutoSize = true;
            this.ThursdayCheckBox.Location = new System.Drawing.Point(147, 30);
            this.ThursdayCheckBox.Name = "ThursdayCheckBox";
            this.ThursdayCheckBox.Size = new System.Drawing.Size(41, 19);
            this.ThursdayCheckBox.TabIndex = 22;
            this.ThursdayCheckBox.Text = "ЧТ";
            this.ThursdayCheckBox.UseVisualStyleBackColor = true;
            this.ThursdayCheckBox.CheckedChanged += new System.EventHandler(this.WeekDayCheckBox_CheckedChanged);
            // 
            // WednesdayCheckbox
            // 
            this.WednesdayCheckbox.AutoSize = true;
            this.WednesdayCheckbox.Location = new System.Drawing.Point(104, 30);
            this.WednesdayCheckbox.Name = "WednesdayCheckbox";
            this.WednesdayCheckbox.Size = new System.Drawing.Size(40, 19);
            this.WednesdayCheckbox.TabIndex = 21;
            this.WednesdayCheckbox.Text = "СР";
            this.WednesdayCheckbox.UseVisualStyleBackColor = true;
            this.WednesdayCheckbox.CheckedChanged += new System.EventHandler(this.WeekDayCheckBox_CheckedChanged);
            // 
            // TuesdayCheckbox
            // 
            this.TuesdayCheckbox.AutoSize = true;
            this.TuesdayCheckbox.Location = new System.Drawing.Point(62, 30);
            this.TuesdayCheckbox.Name = "TuesdayCheckbox";
            this.TuesdayCheckbox.Size = new System.Drawing.Size(40, 19);
            this.TuesdayCheckbox.TabIndex = 20;
            this.TuesdayCheckbox.Text = "ВТ";
            this.TuesdayCheckbox.UseVisualStyleBackColor = true;
            this.TuesdayCheckbox.CheckedChanged += new System.EventHandler(this.WeekDayCheckBox_CheckedChanged);
            // 
            // MondayCheckBox
            // 
            this.MondayCheckBox.AutoSize = true;
            this.MondayCheckBox.Location = new System.Drawing.Point(16, 30);
            this.MondayCheckBox.Name = "MondayCheckBox";
            this.MondayCheckBox.Size = new System.Drawing.Size(44, 19);
            this.MondayCheckBox.TabIndex = 19;
            this.MondayCheckBox.Text = "ПН";
            this.MondayCheckBox.UseVisualStyleBackColor = true;
            this.MondayCheckBox.CheckedChanged += new System.EventHandler(this.WeekDayCheckBox_CheckedChanged);
            // 
            // WeeksWeeklyLabel
            // 
            this.WeeksWeeklyLabel.AutoSize = true;
            this.WeeksWeeklyLabel.Location = new System.Drawing.Point(119, 62);
            this.WeeksWeeklyLabel.Name = "WeeksWeeklyLabel";
            this.WeeksWeeklyLabel.Size = new System.Drawing.Size(46, 15);
            this.WeeksWeeklyLabel.TabIndex = 18;
            this.WeeksWeeklyLabel.Text = "недель";
            // 
            // FreauncyWeeklyLabel
            // 
            this.FreauncyWeeklyLabel.AutoSize = true;
            this.FreauncyWeeklyLabel.Location = new System.Drawing.Point(9, 62);
            this.FreauncyWeeklyLabel.Name = "FreauncyWeeklyLabel";
            this.FreauncyWeeklyLabel.Size = new System.Drawing.Size(40, 15);
            this.FreauncyWeeklyLabel.TabIndex = 17;
            this.FreauncyWeeklyLabel.Text = "Через";
            // 
            // FrequancyWeekNumericUpDown
            // 
            this.FrequancyWeekNumericUpDown.Location = new System.Drawing.Point(55, 57);
            this.FrequancyWeekNumericUpDown.Name = "FrequancyWeekNumericUpDown";
            this.FrequancyWeekNumericUpDown.Size = new System.Drawing.Size(62, 23);
            this.FrequancyWeekNumericUpDown.TabIndex = 16;
            // 
            // WeeklyTimePicker
            // 
            this.WeeklyTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.WeeklyTimePicker.Location = new System.Drawing.Point(235, 57);
            this.WeeklyTimePicker.Name = "WeeklyTimePicker";
            this.WeeklyTimePicker.ShowUpDown = true;
            this.WeeklyTimePicker.Size = new System.Drawing.Size(75, 23);
            this.WeeklyTimePicker.TabIndex = 15;
            // 
            // TimeDailyLabel
            // 
            this.TimeDailyLabel.AutoSize = true;
            this.TimeDailyLabel.Location = new System.Drawing.Point(184, 62);
            this.TimeDailyLabel.Name = "TimeDailyLabel";
            this.TimeDailyLabel.Size = new System.Drawing.Size(45, 15);
            this.TimeDailyLabel.TabIndex = 14;
            this.TimeDailyLabel.Text = "Время:";
            // 
            // MonthlyPanel
            // 
            this.MonthlyPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MonthlyPanel.Controls.Add(this.MayСheckBox);
            this.MonthlyPanel.Controls.Add(this.AprileCheckBox);
            this.MonthlyPanel.Controls.Add(this.MonthlyTimePicker);
            this.MonthlyPanel.Controls.Add(this.TimeMonthlyLabel);
            this.MonthlyPanel.Controls.Add(this.MonthDaysRadioButton);
            this.MonthlyPanel.Controls.Add(this.WeekDaysRadioButton);
            this.MonthlyPanel.Controls.Add(this.TypeMonthlyLabel);
            this.MonthlyPanel.Controls.Add(this.MarchCheckBox);
            this.MonthlyPanel.Controls.Add(this.DecemberCheckBox);
            this.MonthlyPanel.Controls.Add(this.NonemberCheckBox);
            this.MonthlyPanel.Controls.Add(this.OctemberCheckBox);
            this.MonthlyPanel.Controls.Add(this.SeptemberCheckBox);
            this.MonthlyPanel.Controls.Add(this.MonthsLabel);
            this.MonthlyPanel.Controls.Add(this.AugustCheckBox);
            this.MonthlyPanel.Controls.Add(this.JuleCheckBox);
            this.MonthlyPanel.Controls.Add(this.JuneCheckBox);
            this.MonthlyPanel.Controls.Add(this.FebruaryCheckBox);
            this.MonthlyPanel.Controls.Add(this.JanuaryCheckBox);
            this.MonthlyPanel.Controls.Add(this.MonthDaysPanel);
            this.MonthlyPanel.Controls.Add(this.WeekDaysMonthlyPanel);
            this.MonthlyPanel.Location = new System.Drawing.Point(-1, 68);
            this.MonthlyPanel.Name = "MonthlyPanel";
            this.MonthlyPanel.Size = new System.Drawing.Size(322, 42);
            this.MonthlyPanel.TabIndex = 15;
            this.MonthlyPanel.Visible = false;
            // 
            // MayСheckBox
            // 
            this.MayСheckBox.AutoSize = true;
            this.MayСheckBox.Location = new System.Drawing.Point(213, 30);
            this.MayСheckBox.Name = "MayСheckBox";
            this.MayСheckBox.Size = new System.Drawing.Size(50, 19);
            this.MayСheckBox.TabIndex = 35;
            this.MayСheckBox.Text = "Май";
            this.MayСheckBox.UseVisualStyleBackColor = true;
            this.MayСheckBox.CheckedChanged += new System.EventHandler(this.MonthCheckBox_CheckedChanged);
            // 
            // AprileCheckBox
            // 
            this.AprileCheckBox.AutoSize = true;
            this.AprileCheckBox.Location = new System.Drawing.Point(163, 30);
            this.AprileCheckBox.Name = "AprileCheckBox";
            this.AprileCheckBox.Size = new System.Drawing.Size(51, 19);
            this.AprileCheckBox.TabIndex = 34;
            this.AprileCheckBox.Text = "Апр.";
            this.AprileCheckBox.UseVisualStyleBackColor = true;
            this.AprileCheckBox.CheckedChanged += new System.EventHandler(this.MonthCheckBox_CheckedChanged);
            // 
            // MonthlyTimePicker
            // 
            this.MonthlyTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.MonthlyTimePicker.Location = new System.Drawing.Point(64, 79);
            this.MonthlyTimePicker.Name = "MonthlyTimePicker";
            this.MonthlyTimePicker.ShowUpDown = true;
            this.MonthlyTimePicker.Size = new System.Drawing.Size(75, 23);
            this.MonthlyTimePicker.TabIndex = 28;
            // 
            // TimeMonthlyLabel
            // 
            this.TimeMonthlyLabel.AutoSize = true;
            this.TimeMonthlyLabel.Location = new System.Drawing.Point(13, 85);
            this.TimeMonthlyLabel.Name = "TimeMonthlyLabel";
            this.TimeMonthlyLabel.Size = new System.Drawing.Size(45, 15);
            this.TimeMonthlyLabel.TabIndex = 27;
            this.TimeMonthlyLabel.Text = "Время:";
            // 
            // MonthDaysRadioButton
            // 
            this.MonthDaysRadioButton.AutoSize = true;
            this.MonthDaysRadioButton.Checked = true;
            this.MonthDaysRadioButton.Location = new System.Drawing.Point(49, 108);
            this.MonthDaysRadioButton.Name = "MonthDaysRadioButton";
            this.MonthDaysRadioButton.Size = new System.Drawing.Size(91, 19);
            this.MonthDaysRadioButton.TabIndex = 46;
            this.MonthDaysRadioButton.TabStop = true;
            this.MonthDaysRadioButton.Text = "Дни месяца";
            this.MonthDaysRadioButton.UseVisualStyleBackColor = true;
            this.MonthDaysRadioButton.CheckedChanged += new System.EventHandler(this.MonthDaysRadioButton_CheckedChanged);
            // 
            // WeekDaysRadioButton
            // 
            this.WeekDaysRadioButton.AutoSize = true;
            this.WeekDaysRadioButton.Location = new System.Drawing.Point(143, 108);
            this.WeekDaysRadioButton.Name = "WeekDaysRadioButton";
            this.WeekDaysRadioButton.Size = new System.Drawing.Size(91, 19);
            this.WeekDaysRadioButton.TabIndex = 16;
            this.WeekDaysRadioButton.Text = "Дни недели";
            this.WeekDaysRadioButton.UseVisualStyleBackColor = true;
            this.WeekDaysRadioButton.CheckedChanged += new System.EventHandler(this.WeekDaysRadioButton_CheckedChanged);
            // 
            // TypeMonthlyLabel
            // 
            this.TypeMonthlyLabel.AutoSize = true;
            this.TypeMonthlyLabel.Location = new System.Drawing.Point(13, 110);
            this.TypeMonthlyLabel.Name = "TypeMonthlyLabel";
            this.TypeMonthlyLabel.Size = new System.Drawing.Size(30, 15);
            this.TypeMonthlyLabel.TabIndex = 16;
            this.TypeMonthlyLabel.Text = "Тип:";
            // 
            // MarchCheckBox
            // 
            this.MarchCheckBox.AutoSize = true;
            this.MarchCheckBox.Location = new System.Drawing.Point(114, 30);
            this.MarchCheckBox.Name = "MarchCheckBox";
            this.MarchCheckBox.Size = new System.Drawing.Size(53, 19);
            this.MarchCheckBox.TabIndex = 45;
            this.MarchCheckBox.Text = "Мар.";
            this.MarchCheckBox.UseVisualStyleBackColor = true;
            this.MarchCheckBox.CheckedChanged += new System.EventHandler(this.MonthCheckBox_CheckedChanged);
            // 
            // DecemberCheckBox
            // 
            this.DecemberCheckBox.AutoSize = true;
            this.DecemberCheckBox.Location = new System.Drawing.Point(263, 53);
            this.DecemberCheckBox.Name = "DecemberCheckBox";
            this.DecemberCheckBox.Size = new System.Drawing.Size(50, 19);
            this.DecemberCheckBox.TabIndex = 43;
            this.DecemberCheckBox.Text = "Дек.";
            this.DecemberCheckBox.UseVisualStyleBackColor = true;
            this.DecemberCheckBox.CheckedChanged += new System.EventHandler(this.MonthCheckBox_CheckedChanged);
            // 
            // NonemberCheckBox
            // 
            this.NonemberCheckBox.AutoSize = true;
            this.NonemberCheckBox.Location = new System.Drawing.Point(213, 53);
            this.NonemberCheckBox.Name = "NonemberCheckBox";
            this.NonemberCheckBox.Size = new System.Drawing.Size(51, 19);
            this.NonemberCheckBox.TabIndex = 42;
            this.NonemberCheckBox.Text = "Ноя.";
            this.NonemberCheckBox.UseVisualStyleBackColor = true;
            this.NonemberCheckBox.CheckedChanged += new System.EventHandler(this.MonthCheckBox_CheckedChanged);
            // 
            // OctemberCheckBox
            // 
            this.OctemberCheckBox.AutoSize = true;
            this.OctemberCheckBox.Location = new System.Drawing.Point(163, 53);
            this.OctemberCheckBox.Name = "OctemberCheckBox";
            this.OctemberCheckBox.Size = new System.Drawing.Size(49, 19);
            this.OctemberCheckBox.TabIndex = 41;
            this.OctemberCheckBox.Text = "Окт.";
            this.OctemberCheckBox.UseVisualStyleBackColor = true;
            this.OctemberCheckBox.CheckedChanged += new System.EventHandler(this.MonthCheckBox_CheckedChanged);
            // 
            // SeptemberCheckBox
            // 
            this.SeptemberCheckBox.AutoSize = true;
            this.SeptemberCheckBox.Location = new System.Drawing.Point(114, 53);
            this.SeptemberCheckBox.Name = "SeptemberCheckBox";
            this.SeptemberCheckBox.Size = new System.Drawing.Size(49, 19);
            this.SeptemberCheckBox.TabIndex = 40;
            this.SeptemberCheckBox.Text = "Сен.";
            this.SeptemberCheckBox.UseVisualStyleBackColor = true;
            this.SeptemberCheckBox.CheckedChanged += new System.EventHandler(this.MonthCheckBox_CheckedChanged);
            // 
            // MonthsLabel
            // 
            this.MonthsLabel.AutoSize = true;
            this.MonthsLabel.Location = new System.Drawing.Point(13, 7);
            this.MonthsLabel.Name = "MonthsLabel";
            this.MonthsLabel.Size = new System.Drawing.Size(55, 15);
            this.MonthsLabel.TabIndex = 39;
            this.MonthsLabel.Text = "Месяцы:";
            // 
            // AugustCheckBox
            // 
            this.AugustCheckBox.AutoSize = true;
            this.AugustCheckBox.Location = new System.Drawing.Point(67, 53);
            this.AugustCheckBox.Name = "AugustCheckBox";
            this.AugustCheckBox.Size = new System.Drawing.Size(49, 19);
            this.AugustCheckBox.TabIndex = 38;
            this.AugustCheckBox.Text = "Авг.";
            this.AugustCheckBox.UseVisualStyleBackColor = true;
            this.AugustCheckBox.CheckedChanged += new System.EventHandler(this.MonthCheckBox_CheckedChanged);
            // 
            // JuleCheckBox
            // 
            this.JuleCheckBox.AutoSize = true;
            this.JuleCheckBox.Location = new System.Drawing.Point(16, 53);
            this.JuleCheckBox.Name = "JuleCheckBox";
            this.JuleCheckBox.Size = new System.Drawing.Size(55, 19);
            this.JuleCheckBox.TabIndex = 37;
            this.JuleCheckBox.Text = "Июл.";
            this.JuleCheckBox.UseVisualStyleBackColor = true;
            this.JuleCheckBox.CheckedChanged += new System.EventHandler(this.MonthCheckBox_CheckedChanged);
            // 
            // JuneCheckBox
            // 
            this.JuneCheckBox.AutoSize = true;
            this.JuneCheckBox.Location = new System.Drawing.Point(263, 30);
            this.JuneCheckBox.Name = "JuneCheckBox";
            this.JuneCheckBox.Size = new System.Drawing.Size(55, 19);
            this.JuneCheckBox.TabIndex = 36;
            this.JuneCheckBox.Text = "Июн.";
            this.JuneCheckBox.UseVisualStyleBackColor = true;
            this.JuneCheckBox.CheckedChanged += new System.EventHandler(this.MonthCheckBox_CheckedChanged);
            // 
            // FebruaryCheckBox
            // 
            this.FebruaryCheckBox.AutoSize = true;
            this.FebruaryCheckBox.Location = new System.Drawing.Point(67, 30);
            this.FebruaryCheckBox.Name = "FebruaryCheckBox";
            this.FebruaryCheckBox.Size = new System.Drawing.Size(51, 19);
            this.FebruaryCheckBox.TabIndex = 33;
            this.FebruaryCheckBox.Text = "Фев.";
            this.FebruaryCheckBox.UseVisualStyleBackColor = true;
            this.FebruaryCheckBox.CheckedChanged += new System.EventHandler(this.MonthCheckBox_CheckedChanged);
            // 
            // JanuaryCheckBox
            // 
            this.JanuaryCheckBox.AutoSize = true;
            this.JanuaryCheckBox.Location = new System.Drawing.Point(16, 30);
            this.JanuaryCheckBox.Name = "JanuaryCheckBox";
            this.JanuaryCheckBox.Size = new System.Drawing.Size(50, 19);
            this.JanuaryCheckBox.TabIndex = 32;
            this.JanuaryCheckBox.Text = "Янв.";
            this.JanuaryCheckBox.UseVisualStyleBackColor = true;
            this.JanuaryCheckBox.CheckedChanged += new System.EventHandler(this.MonthCheckBox_CheckedChanged);
            // 
            // MonthDaysPanel
            // 
            this.MonthDaysPanel.Controls.Add(this.MonthDay31CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay30CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay29CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay28CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay27CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay26CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay25CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay24CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay23CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay22CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay21CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay20CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay19CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay18CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay17CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay16CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay15CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay14CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay13CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay12CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay11CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay10CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay9CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay8CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay7CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay6CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay5CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay4CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay3CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay2CheckBox);
            this.MonthDaysPanel.Controls.Add(this.MonthDay1CheckBox);
            this.MonthDaysPanel.Location = new System.Drawing.Point(3, 133);
            this.MonthDaysPanel.Name = "MonthDaysPanel";
            this.MonthDaysPanel.Size = new System.Drawing.Size(311, 96);
            this.MonthDaysPanel.TabIndex = 48;
            // 
            // MonthDay31CheckBox
            // 
            this.MonthDay31CheckBox.AutoSize = true;
            this.MonthDay31CheckBox.Location = new System.Drawing.Point(232, 74);
            this.MonthDay31CheckBox.Name = "MonthDay31CheckBox";
            this.MonthDay31CheckBox.Size = new System.Drawing.Size(38, 19);
            this.MonthDay31CheckBox.TabIndex = 56;
            this.MonthDay31CheckBox.Text = "31";
            this.MonthDay31CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay31CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay30CheckBox
            // 
            this.MonthDay30CheckBox.AutoSize = true;
            this.MonthDay30CheckBox.Location = new System.Drawing.Point(196, 74);
            this.MonthDay30CheckBox.Name = "MonthDay30CheckBox";
            this.MonthDay30CheckBox.Size = new System.Drawing.Size(40, 19);
            this.MonthDay30CheckBox.TabIndex = 55;
            this.MonthDay30CheckBox.Text = "30";
            this.MonthDay30CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay30CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay29CheckBox
            // 
            this.MonthDay29CheckBox.AutoSize = true;
            this.MonthDay29CheckBox.Location = new System.Drawing.Point(160, 74);
            this.MonthDay29CheckBox.Name = "MonthDay29CheckBox";
            this.MonthDay29CheckBox.Size = new System.Drawing.Size(40, 19);
            this.MonthDay29CheckBox.TabIndex = 54;
            this.MonthDay29CheckBox.Text = "29";
            this.MonthDay29CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay29CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay28CheckBox
            // 
            this.MonthDay28CheckBox.AutoSize = true;
            this.MonthDay28CheckBox.Location = new System.Drawing.Point(121, 74);
            this.MonthDay28CheckBox.Name = "MonthDay28CheckBox";
            this.MonthDay28CheckBox.Size = new System.Drawing.Size(40, 19);
            this.MonthDay28CheckBox.TabIndex = 53;
            this.MonthDay28CheckBox.Text = "28";
            this.MonthDay28CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay28CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay27CheckBox
            // 
            this.MonthDay27CheckBox.AutoSize = true;
            this.MonthDay27CheckBox.Location = new System.Drawing.Point(84, 74);
            this.MonthDay27CheckBox.Name = "MonthDay27CheckBox";
            this.MonthDay27CheckBox.Size = new System.Drawing.Size(39, 19);
            this.MonthDay27CheckBox.TabIndex = 52;
            this.MonthDay27CheckBox.Text = "27";
            this.MonthDay27CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay27CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay26CheckBox
            // 
            this.MonthDay26CheckBox.AutoSize = true;
            this.MonthDay26CheckBox.Location = new System.Drawing.Point(48, 74);
            this.MonthDay26CheckBox.Name = "MonthDay26CheckBox";
            this.MonthDay26CheckBox.Size = new System.Drawing.Size(40, 19);
            this.MonthDay26CheckBox.TabIndex = 51;
            this.MonthDay26CheckBox.Text = "26";
            this.MonthDay26CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay26CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay25CheckBox
            // 
            this.MonthDay25CheckBox.AutoSize = true;
            this.MonthDay25CheckBox.Location = new System.Drawing.Point(13, 74);
            this.MonthDay25CheckBox.Name = "MonthDay25CheckBox";
            this.MonthDay25CheckBox.Size = new System.Drawing.Size(40, 19);
            this.MonthDay25CheckBox.TabIndex = 50;
            this.MonthDay25CheckBox.Text = "25";
            this.MonthDay25CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay25CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay24CheckBox
            // 
            this.MonthDay24CheckBox.AutoSize = true;
            this.MonthDay24CheckBox.Location = new System.Drawing.Point(270, 51);
            this.MonthDay24CheckBox.Name = "MonthDay24CheckBox";
            this.MonthDay24CheckBox.Size = new System.Drawing.Size(40, 19);
            this.MonthDay24CheckBox.TabIndex = 49;
            this.MonthDay24CheckBox.Text = "24";
            this.MonthDay24CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay24CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay23CheckBox
            // 
            this.MonthDay23CheckBox.AutoSize = true;
            this.MonthDay23CheckBox.Location = new System.Drawing.Point(232, 51);
            this.MonthDay23CheckBox.Name = "MonthDay23CheckBox";
            this.MonthDay23CheckBox.Size = new System.Drawing.Size(40, 19);
            this.MonthDay23CheckBox.TabIndex = 48;
            this.MonthDay23CheckBox.Text = "23";
            this.MonthDay23CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay23CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay22CheckBox
            // 
            this.MonthDay22CheckBox.AutoSize = true;
            this.MonthDay22CheckBox.Location = new System.Drawing.Point(196, 51);
            this.MonthDay22CheckBox.Name = "MonthDay22CheckBox";
            this.MonthDay22CheckBox.Size = new System.Drawing.Size(40, 19);
            this.MonthDay22CheckBox.TabIndex = 47;
            this.MonthDay22CheckBox.Text = "22";
            this.MonthDay22CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay22CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay21CheckBox
            // 
            this.MonthDay21CheckBox.AutoSize = true;
            this.MonthDay21CheckBox.Location = new System.Drawing.Point(160, 51);
            this.MonthDay21CheckBox.Name = "MonthDay21CheckBox";
            this.MonthDay21CheckBox.Size = new System.Drawing.Size(38, 19);
            this.MonthDay21CheckBox.TabIndex = 46;
            this.MonthDay21CheckBox.Text = "21";
            this.MonthDay21CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay21CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay20CheckBox
            // 
            this.MonthDay20CheckBox.AutoSize = true;
            this.MonthDay20CheckBox.Location = new System.Drawing.Point(121, 51);
            this.MonthDay20CheckBox.Name = "MonthDay20CheckBox";
            this.MonthDay20CheckBox.Size = new System.Drawing.Size(40, 19);
            this.MonthDay20CheckBox.TabIndex = 45;
            this.MonthDay20CheckBox.Text = "20";
            this.MonthDay20CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay20CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay19CheckBox
            // 
            this.MonthDay19CheckBox.AutoSize = true;
            this.MonthDay19CheckBox.Location = new System.Drawing.Point(84, 51);
            this.MonthDay19CheckBox.Name = "MonthDay19CheckBox";
            this.MonthDay19CheckBox.Size = new System.Drawing.Size(38, 19);
            this.MonthDay19CheckBox.TabIndex = 44;
            this.MonthDay19CheckBox.Text = "19";
            this.MonthDay19CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay19CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay18CheckBox
            // 
            this.MonthDay18CheckBox.AutoSize = true;
            this.MonthDay18CheckBox.Location = new System.Drawing.Point(48, 51);
            this.MonthDay18CheckBox.Name = "MonthDay18CheckBox";
            this.MonthDay18CheckBox.Size = new System.Drawing.Size(38, 19);
            this.MonthDay18CheckBox.TabIndex = 43;
            this.MonthDay18CheckBox.Text = "18";
            this.MonthDay18CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay18CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay17CheckBox
            // 
            this.MonthDay17CheckBox.AutoSize = true;
            this.MonthDay17CheckBox.Location = new System.Drawing.Point(13, 51);
            this.MonthDay17CheckBox.Name = "MonthDay17CheckBox";
            this.MonthDay17CheckBox.Size = new System.Drawing.Size(37, 19);
            this.MonthDay17CheckBox.TabIndex = 42;
            this.MonthDay17CheckBox.Text = "17";
            this.MonthDay17CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay17CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay16CheckBox
            // 
            this.MonthDay16CheckBox.AutoSize = true;
            this.MonthDay16CheckBox.Location = new System.Drawing.Point(270, 28);
            this.MonthDay16CheckBox.Name = "MonthDay16CheckBox";
            this.MonthDay16CheckBox.Size = new System.Drawing.Size(38, 19);
            this.MonthDay16CheckBox.TabIndex = 41;
            this.MonthDay16CheckBox.Text = "16";
            this.MonthDay16CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay16CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay15CheckBox
            // 
            this.MonthDay15CheckBox.AutoSize = true;
            this.MonthDay15CheckBox.Location = new System.Drawing.Point(232, 28);
            this.MonthDay15CheckBox.Name = "MonthDay15CheckBox";
            this.MonthDay15CheckBox.Size = new System.Drawing.Size(38, 19);
            this.MonthDay15CheckBox.TabIndex = 40;
            this.MonthDay15CheckBox.Text = "15";
            this.MonthDay15CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay15CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay14CheckBox
            // 
            this.MonthDay14CheckBox.AutoSize = true;
            this.MonthDay14CheckBox.Location = new System.Drawing.Point(196, 28);
            this.MonthDay14CheckBox.Name = "MonthDay14CheckBox";
            this.MonthDay14CheckBox.Size = new System.Drawing.Size(38, 19);
            this.MonthDay14CheckBox.TabIndex = 39;
            this.MonthDay14CheckBox.Text = "14";
            this.MonthDay14CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay14CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay13CheckBox
            // 
            this.MonthDay13CheckBox.AutoSize = true;
            this.MonthDay13CheckBox.Location = new System.Drawing.Point(160, 28);
            this.MonthDay13CheckBox.Name = "MonthDay13CheckBox";
            this.MonthDay13CheckBox.Size = new System.Drawing.Size(38, 19);
            this.MonthDay13CheckBox.TabIndex = 38;
            this.MonthDay13CheckBox.Text = "13";
            this.MonthDay13CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay13CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay12CheckBox
            // 
            this.MonthDay12CheckBox.AutoSize = true;
            this.MonthDay12CheckBox.Location = new System.Drawing.Point(121, 28);
            this.MonthDay12CheckBox.Name = "MonthDay12CheckBox";
            this.MonthDay12CheckBox.Size = new System.Drawing.Size(38, 19);
            this.MonthDay12CheckBox.TabIndex = 37;
            this.MonthDay12CheckBox.Text = "12";
            this.MonthDay12CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay12CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay11CheckBox
            // 
            this.MonthDay11CheckBox.AutoSize = true;
            this.MonthDay11CheckBox.Location = new System.Drawing.Point(84, 28);
            this.MonthDay11CheckBox.Name = "MonthDay11CheckBox";
            this.MonthDay11CheckBox.Size = new System.Drawing.Size(36, 19);
            this.MonthDay11CheckBox.TabIndex = 36;
            this.MonthDay11CheckBox.Text = "11";
            this.MonthDay11CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay11CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay10CheckBox
            // 
            this.MonthDay10CheckBox.AutoSize = true;
            this.MonthDay10CheckBox.Location = new System.Drawing.Point(48, 28);
            this.MonthDay10CheckBox.Name = "MonthDay10CheckBox";
            this.MonthDay10CheckBox.Size = new System.Drawing.Size(38, 19);
            this.MonthDay10CheckBox.TabIndex = 35;
            this.MonthDay10CheckBox.Text = "10";
            this.MonthDay10CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay10CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay9CheckBox
            // 
            this.MonthDay9CheckBox.AutoSize = true;
            this.MonthDay9CheckBox.Location = new System.Drawing.Point(13, 28);
            this.MonthDay9CheckBox.Name = "MonthDay9CheckBox";
            this.MonthDay9CheckBox.Size = new System.Drawing.Size(33, 19);
            this.MonthDay9CheckBox.TabIndex = 34;
            this.MonthDay9CheckBox.Text = "9";
            this.MonthDay9CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay9CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay8CheckBox
            // 
            this.MonthDay8CheckBox.AutoSize = true;
            this.MonthDay8CheckBox.Location = new System.Drawing.Point(270, 6);
            this.MonthDay8CheckBox.Name = "MonthDay8CheckBox";
            this.MonthDay8CheckBox.Size = new System.Drawing.Size(33, 19);
            this.MonthDay8CheckBox.TabIndex = 33;
            this.MonthDay8CheckBox.Text = "8";
            this.MonthDay8CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay8CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay7CheckBox
            // 
            this.MonthDay7CheckBox.AutoSize = true;
            this.MonthDay7CheckBox.Location = new System.Drawing.Point(232, 6);
            this.MonthDay7CheckBox.Name = "MonthDay7CheckBox";
            this.MonthDay7CheckBox.Size = new System.Drawing.Size(32, 19);
            this.MonthDay7CheckBox.TabIndex = 32;
            this.MonthDay7CheckBox.Text = "7";
            this.MonthDay7CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay7CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay6CheckBox
            // 
            this.MonthDay6CheckBox.AutoSize = true;
            this.MonthDay6CheckBox.Location = new System.Drawing.Point(196, 6);
            this.MonthDay6CheckBox.Name = "MonthDay6CheckBox";
            this.MonthDay6CheckBox.Size = new System.Drawing.Size(33, 19);
            this.MonthDay6CheckBox.TabIndex = 31;
            this.MonthDay6CheckBox.Text = "6";
            this.MonthDay6CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay6CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay5CheckBox
            // 
            this.MonthDay5CheckBox.AutoSize = true;
            this.MonthDay5CheckBox.Location = new System.Drawing.Point(160, 6);
            this.MonthDay5CheckBox.Name = "MonthDay5CheckBox";
            this.MonthDay5CheckBox.Size = new System.Drawing.Size(33, 19);
            this.MonthDay5CheckBox.TabIndex = 30;
            this.MonthDay5CheckBox.Text = "5";
            this.MonthDay5CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay5CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay4CheckBox
            // 
            this.MonthDay4CheckBox.AutoSize = true;
            this.MonthDay4CheckBox.Location = new System.Drawing.Point(121, 6);
            this.MonthDay4CheckBox.Name = "MonthDay4CheckBox";
            this.MonthDay4CheckBox.Size = new System.Drawing.Size(33, 19);
            this.MonthDay4CheckBox.TabIndex = 29;
            this.MonthDay4CheckBox.Text = "4";
            this.MonthDay4CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay4CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay3CheckBox
            // 
            this.MonthDay3CheckBox.AutoSize = true;
            this.MonthDay3CheckBox.Location = new System.Drawing.Point(84, 6);
            this.MonthDay3CheckBox.Name = "MonthDay3CheckBox";
            this.MonthDay3CheckBox.Size = new System.Drawing.Size(33, 19);
            this.MonthDay3CheckBox.TabIndex = 28;
            this.MonthDay3CheckBox.Text = "3";
            this.MonthDay3CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay3CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay2CheckBox
            // 
            this.MonthDay2CheckBox.AutoSize = true;
            this.MonthDay2CheckBox.Location = new System.Drawing.Point(48, 6);
            this.MonthDay2CheckBox.Name = "MonthDay2CheckBox";
            this.MonthDay2CheckBox.Size = new System.Drawing.Size(33, 19);
            this.MonthDay2CheckBox.TabIndex = 27;
            this.MonthDay2CheckBox.Text = "2";
            this.MonthDay2CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay2CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // MonthDay1CheckBox
            // 
            this.MonthDay1CheckBox.AutoSize = true;
            this.MonthDay1CheckBox.Location = new System.Drawing.Point(13, 6);
            this.MonthDay1CheckBox.Name = "MonthDay1CheckBox";
            this.MonthDay1CheckBox.Size = new System.Drawing.Size(31, 19);
            this.MonthDay1CheckBox.TabIndex = 26;
            this.MonthDay1CheckBox.Text = "1";
            this.MonthDay1CheckBox.UseVisualStyleBackColor = true;
            this.MonthDay1CheckBox.CheckedChanged += new System.EventHandler(this.MonthDayCheckBox_CheckedChanged);
            // 
            // WeekDaysMonthlyPanel
            // 
            this.WeekDaysMonthlyPanel.Controls.Add(this.SundayMonthlyCheckBox);
            this.WeekDaysMonthlyPanel.Controls.Add(this.SaturdayMonthlyCheckBox);
            this.WeekDaysMonthlyPanel.Controls.Add(this.FridayMonthlyCheckBox);
            this.WeekDaysMonthlyPanel.Controls.Add(this.ThursdayMonthlyCheckBox);
            this.WeekDaysMonthlyPanel.Controls.Add(this.WednesdayMonthlyCheckbox);
            this.WeekDaysMonthlyPanel.Controls.Add(this.TuesdayMonthlyCheckbox);
            this.WeekDaysMonthlyPanel.Controls.Add(this.MondayMonthlyCheckBox);
            this.WeekDaysMonthlyPanel.Location = new System.Drawing.Point(3, 133);
            this.WeekDaysMonthlyPanel.Name = "WeekDaysMonthlyPanel";
            this.WeekDaysMonthlyPanel.Size = new System.Drawing.Size(311, 28);
            this.WeekDaysMonthlyPanel.TabIndex = 47;
            this.WeekDaysMonthlyPanel.Visible = false;
            // 
            // SundayMonthlyCheckBox
            // 
            this.SundayMonthlyCheckBox.AutoSize = true;
            this.SundayMonthlyCheckBox.Location = new System.Drawing.Point(274, 3);
            this.SundayMonthlyCheckBox.Name = "SundayMonthlyCheckBox";
            this.SundayMonthlyCheckBox.Size = new System.Drawing.Size(40, 19);
            this.SundayMonthlyCheckBox.TabIndex = 32;
            this.SundayMonthlyCheckBox.Text = "ВС";
            this.SundayMonthlyCheckBox.UseVisualStyleBackColor = true;
            this.SundayMonthlyCheckBox.CheckedChanged += new System.EventHandler(this.WeekDayMonthlyCheckBox_CheckedChanged);
            // 
            // SaturdayMonthlyCheckBox
            // 
            this.SaturdayMonthlyCheckBox.AutoSize = true;
            this.SaturdayMonthlyCheckBox.Location = new System.Drawing.Point(232, 3);
            this.SaturdayMonthlyCheckBox.Name = "SaturdayMonthlyCheckBox";
            this.SaturdayMonthlyCheckBox.Size = new System.Drawing.Size(40, 19);
            this.SaturdayMonthlyCheckBox.TabIndex = 31;
            this.SaturdayMonthlyCheckBox.Text = "СБ";
            this.SaturdayMonthlyCheckBox.UseVisualStyleBackColor = true;
            this.SaturdayMonthlyCheckBox.CheckedChanged += new System.EventHandler(this.WeekDayMonthlyCheckBox_CheckedChanged);
            // 
            // FridayMonthlyCheckBox
            // 
            this.FridayMonthlyCheckBox.AutoSize = true;
            this.FridayMonthlyCheckBox.Location = new System.Drawing.Point(188, 3);
            this.FridayMonthlyCheckBox.Name = "FridayMonthlyCheckBox";
            this.FridayMonthlyCheckBox.Size = new System.Drawing.Size(42, 19);
            this.FridayMonthlyCheckBox.TabIndex = 30;
            this.FridayMonthlyCheckBox.Text = "ПТ";
            this.FridayMonthlyCheckBox.UseVisualStyleBackColor = true;
            this.FridayMonthlyCheckBox.CheckedChanged += new System.EventHandler(this.WeekDayMonthlyCheckBox_CheckedChanged);
            // 
            // ThursdayMonthlyCheckBox
            // 
            this.ThursdayMonthlyCheckBox.AutoSize = true;
            this.ThursdayMonthlyCheckBox.Location = new System.Drawing.Point(144, 3);
            this.ThursdayMonthlyCheckBox.Name = "ThursdayMonthlyCheckBox";
            this.ThursdayMonthlyCheckBox.Size = new System.Drawing.Size(41, 19);
            this.ThursdayMonthlyCheckBox.TabIndex = 29;
            this.ThursdayMonthlyCheckBox.Text = "ЧТ";
            this.ThursdayMonthlyCheckBox.UseVisualStyleBackColor = true;
            this.ThursdayMonthlyCheckBox.CheckedChanged += new System.EventHandler(this.WeekDayMonthlyCheckBox_CheckedChanged);
            // 
            // WednesdayMonthlyCheckbox
            // 
            this.WednesdayMonthlyCheckbox.AutoSize = true;
            this.WednesdayMonthlyCheckbox.Location = new System.Drawing.Point(101, 3);
            this.WednesdayMonthlyCheckbox.Name = "WednesdayMonthlyCheckbox";
            this.WednesdayMonthlyCheckbox.Size = new System.Drawing.Size(40, 19);
            this.WednesdayMonthlyCheckbox.TabIndex = 28;
            this.WednesdayMonthlyCheckbox.Text = "СР";
            this.WednesdayMonthlyCheckbox.UseVisualStyleBackColor = true;
            this.WednesdayMonthlyCheckbox.CheckedChanged += new System.EventHandler(this.WeekDayMonthlyCheckBox_CheckedChanged);
            // 
            // TuesdayMonthlyCheckbox
            // 
            this.TuesdayMonthlyCheckbox.AutoSize = true;
            this.TuesdayMonthlyCheckbox.Location = new System.Drawing.Point(59, 3);
            this.TuesdayMonthlyCheckbox.Name = "TuesdayMonthlyCheckbox";
            this.TuesdayMonthlyCheckbox.Size = new System.Drawing.Size(40, 19);
            this.TuesdayMonthlyCheckbox.TabIndex = 27;
            this.TuesdayMonthlyCheckbox.Text = "ВТ";
            this.TuesdayMonthlyCheckbox.UseVisualStyleBackColor = true;
            this.TuesdayMonthlyCheckbox.CheckedChanged += new System.EventHandler(this.WeekDayMonthlyCheckBox_CheckedChanged);
            // 
            // MondayMonthlyCheckBox
            // 
            this.MondayMonthlyCheckBox.AutoSize = true;
            this.MondayMonthlyCheckBox.Location = new System.Drawing.Point(13, 3);
            this.MondayMonthlyCheckBox.Name = "MondayMonthlyCheckBox";
            this.MondayMonthlyCheckBox.Size = new System.Drawing.Size(44, 19);
            this.MondayMonthlyCheckBox.TabIndex = 26;
            this.MondayMonthlyCheckBox.Text = "ПН";
            this.MondayMonthlyCheckBox.UseVisualStyleBackColor = true;
            this.MondayMonthlyCheckBox.CheckedChanged += new System.EventHandler(this.WeekDayMonthlyCheckBox_CheckedChanged);
            // 
            // TriggerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 151);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.AddButton);
            this.Controls.Add(this.MonthlyRadioButton);
            this.Controls.Add(this.DailyRadioButton);
            this.Controls.Add(this.WeeklyRadioButton);
            this.Controls.Add(this.OneshotRadioButton);
            this.Controls.Add(this.TriggerTypeLabel);
            this.Controls.Add(this.OneshotPanel);
            this.Controls.Add(this.MonthlyPanel);
            this.Controls.Add(this.WeeklyPanel);
            this.Controls.Add(this.DailyPanel);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TriggerForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Триггер";
            this.OneshotPanel.ResumeLayout(false);
            this.OneshotPanel.PerformLayout();
            this.DailyPanel.ResumeLayout(false);
            this.DailyPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FreauncyDailyNumericUpDown)).EndInit();
            this.WeeklyPanel.ResumeLayout(false);
            this.WeeklyPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FrequancyWeekNumericUpDown)).EndInit();
            this.MonthlyPanel.ResumeLayout(false);
            this.MonthlyPanel.PerformLayout();
            this.MonthDaysPanel.ResumeLayout(false);
            this.MonthDaysPanel.PerformLayout();
            this.WeekDaysMonthlyPanel.ResumeLayout(false);
            this.WeekDaysMonthlyPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label TriggerTypeLabel;
        private System.Windows.Forms.RadioButton OneshotRadioButton;
        private System.Windows.Forms.RadioButton WeeklyRadioButton;
        private System.Windows.Forms.RadioButton DailyRadioButton;
        private System.Windows.Forms.RadioButton MonthlyRadioButton;
        private System.Windows.Forms.Panel OneshotPanel;
        private System.Windows.Forms.Button AddButton;
        private new System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.DateTimePicker OneshotDatePicker;
        private System.Windows.Forms.Label OneshotDateLabel;
        private System.Windows.Forms.DateTimePicker OneshotTimePicker;
        private System.Windows.Forms.Label OneshotTimeLabel;
        private System.Windows.Forms.Panel DailyPanel;
        private System.Windows.Forms.DateTimePicker DailyTimePicker;
        private System.Windows.Forms.Label DailyTimeLabel;
        private System.Windows.Forms.Label DaysDailyLabel;
        private System.Windows.Forms.Label FreauncyDailyLabel;
        private System.Windows.Forms.NumericUpDown FreauncyDailyNumericUpDown;
        private System.Windows.Forms.Panel WeeklyPanel;
        private System.Windows.Forms.Panel MonthlyPanel;
        private System.Windows.Forms.Label WeekDaysLabel;
        private System.Windows.Forms.CheckBox SundayCheckBox;
        private System.Windows.Forms.CheckBox SaturdayCheckBox;
        private System.Windows.Forms.CheckBox FridayCheckBox;
        private System.Windows.Forms.CheckBox ThursdayCheckBox;
        private System.Windows.Forms.CheckBox WednesdayCheckbox;
        private System.Windows.Forms.CheckBox TuesdayCheckbox;
        private System.Windows.Forms.CheckBox MondayCheckBox;
        private System.Windows.Forms.Label WeeksWeeklyLabel;
        private System.Windows.Forms.Label FreauncyWeeklyLabel;
        private System.Windows.Forms.NumericUpDown FrequancyWeekNumericUpDown;
        private System.Windows.Forms.DateTimePicker WeeklyTimePicker;
        private System.Windows.Forms.Label TimeDailyLabel;
        private System.Windows.Forms.Label MonthsLabel;
        private System.Windows.Forms.CheckBox AugustCheckBox;
        private System.Windows.Forms.CheckBox JuleCheckBox;
        private System.Windows.Forms.CheckBox JuneCheckBox;
        private System.Windows.Forms.CheckBox MayСheckBox;
        private System.Windows.Forms.CheckBox AprileCheckBox;
        private System.Windows.Forms.CheckBox FebruaryCheckBox;
        private System.Windows.Forms.CheckBox JanuaryCheckBox;
        private System.Windows.Forms.Label TimeMonthlyLabel;
        private System.Windows.Forms.CheckBox DecemberCheckBox;
        private System.Windows.Forms.CheckBox NonemberCheckBox;
        private System.Windows.Forms.CheckBox OctemberCheckBox;
        private System.Windows.Forms.CheckBox SeptemberCheckBox;
        private System.Windows.Forms.CheckBox MarchCheckBox;
        private System.Windows.Forms.Label TypeMonthlyLabel;
        private System.Windows.Forms.RadioButton MonthDaysRadioButton;
        private System.Windows.Forms.RadioButton WeekDaysRadioButton;
        private System.Windows.Forms.DateTimePicker MonthlyTimePicker;
        private System.Windows.Forms.Panel WeekDaysMonthlyPanel;
        private System.Windows.Forms.CheckBox SundayMonthlyCheckBox;
        private System.Windows.Forms.CheckBox SaturdayMonthlyCheckBox;
        private System.Windows.Forms.CheckBox FridayMonthlyCheckBox;
        private System.Windows.Forms.CheckBox ThursdayMonthlyCheckBox;
        private System.Windows.Forms.CheckBox WednesdayMonthlyCheckbox;
        private System.Windows.Forms.CheckBox TuesdayMonthlyCheckbox;
        private System.Windows.Forms.CheckBox MondayMonthlyCheckBox;
        private System.Windows.Forms.Panel MonthDaysPanel;
        private System.Windows.Forms.CheckBox MonthDay7CheckBox;
        private System.Windows.Forms.CheckBox MonthDay6CheckBox;
        private System.Windows.Forms.CheckBox MonthDay5CheckBox;
        private System.Windows.Forms.CheckBox MonthDay4CheckBox;
        private System.Windows.Forms.CheckBox MonthDay3CheckBox;
        private System.Windows.Forms.CheckBox MonthDay2CheckBox;
        private System.Windows.Forms.CheckBox MonthDay1CheckBox;
        private System.Windows.Forms.CheckBox MonthDay31CheckBox;
        private System.Windows.Forms.CheckBox MonthDay30CheckBox;
        private System.Windows.Forms.CheckBox MonthDay29CheckBox;
        private System.Windows.Forms.CheckBox MonthDay28CheckBox;
        private System.Windows.Forms.CheckBox MonthDay27CheckBox;
        private System.Windows.Forms.CheckBox MonthDay26CheckBox;
        private System.Windows.Forms.CheckBox MonthDay25CheckBox;
        private System.Windows.Forms.CheckBox MonthDay24CheckBox;
        private System.Windows.Forms.CheckBox MonthDay23CheckBox;
        private System.Windows.Forms.CheckBox MonthDay22CheckBox;
        private System.Windows.Forms.CheckBox MonthDay21CheckBox;
        private System.Windows.Forms.CheckBox MonthDay20CheckBox;
        private System.Windows.Forms.CheckBox MonthDay19CheckBox;
        private System.Windows.Forms.CheckBox MonthDay18CheckBox;
        private System.Windows.Forms.CheckBox MonthDay17CheckBox;
        private System.Windows.Forms.CheckBox MonthDay16CheckBox;
        private System.Windows.Forms.CheckBox MonthDay15CheckBox;
        private System.Windows.Forms.CheckBox MonthDay14CheckBox;
        private System.Windows.Forms.CheckBox MonthDay13CheckBox;
        private System.Windows.Forms.CheckBox MonthDay12CheckBox;
        private System.Windows.Forms.CheckBox MonthDay11CheckBox;
        private System.Windows.Forms.CheckBox MonthDay10CheckBox;
        private System.Windows.Forms.CheckBox MonthDay9CheckBox;
        private System.Windows.Forms.CheckBox MonthDay8CheckBox;
    }
}