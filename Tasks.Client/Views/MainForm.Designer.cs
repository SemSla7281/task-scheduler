﻿
namespace Tasks.Client
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ConnectionPanel = new System.Windows.Forms.Panel();
            this.ConnectionStatusLabel = new System.Windows.Forms.Label();
            this.ConnectionLabel = new System.Windows.Forms.Label();
            this.ConnectButton = new System.Windows.Forms.Button();
            this.PortTextBox = new System.Windows.Forms.TextBox();
            this.IPAddressTextBox = new System.Windows.Forms.TextBox();
            this.PortLabel = new System.Windows.Forms.Label();
            this.IPAddressLabel = new System.Windows.Forms.Label();
            this.TaskListPanel = new System.Windows.Forms.Panel();
            this.NewTaskButton = new System.Windows.Forms.Button();
            this.EditTaskButton = new System.Windows.Forms.Button();
            this.DeleteTaskButton = new System.Windows.Forms.Button();
            this.TaskListDataGridView = new System.Windows.Forms.DataGridView();
            this.TaskName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaskEnable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaskListLabel = new System.Windows.Forms.Label();
            this.TaskPanel = new System.Windows.Forms.Panel();
            this.TriggersActionsSplitContainer = new System.Windows.Forms.SplitContainer();
            this.TriggersPanel = new System.Windows.Forms.Panel();
            this.EditTriggerButton = new System.Windows.Forms.Button();
            this.EnableTriggerButton = new System.Windows.Forms.Button();
            this.TriggersLabel = new System.Windows.Forms.Label();
            this.AddTriggerButton = new System.Windows.Forms.Button();
            this.DeleteTriggerButton = new System.Windows.Forms.Button();
            this.TriggersDataGridView = new System.Windows.Forms.DataGridView();
            this.TriggerEnable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TriggerType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Deteils = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActionsPanel = new System.Windows.Forms.Panel();
            this.EditActionButton = new System.Windows.Forms.Button();
            this.EnableActionButton = new System.Windows.Forms.Button();
            this.ActionsLabel = new System.Windows.Forms.Label();
            this.AddActionButton = new System.Windows.Forms.Button();
            this.DeleteActionButton = new System.Windows.Forms.Button();
            this.ActionsDataGridView = new System.Windows.Forms.DataGridView();
            this.ActionEnable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActionPath = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActionArguments = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActionWait = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActionsSpaceTaskPanel = new System.Windows.Forms.Panel();
            this.TaskControlPanel = new System.Windows.Forms.Panel();
            this.AboutTaskLabel = new System.Windows.Forms.Label();
            this.AboutTextBox = new System.Windows.Forms.TextBox();
            this.TaskNameTextBox = new System.Windows.Forms.TextBox();
            this.SendTaskButton = new System.Windows.Forms.Button();
            this.CancelTaskButton = new System.Windows.Forms.Button();
            this.TaskNameLabel = new System.Windows.Forms.Label();
            this.LeftPanel = new System.Windows.Forms.Panel();
            this.ConnectSpaceTaskListlPanel = new System.Windows.Forms.Panel();
            this.SpacingVerticalPanel = new System.Windows.Forms.Panel();
            this.ConnectionPanel.SuspendLayout();
            this.TaskListPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TaskListDataGridView)).BeginInit();
            this.TaskPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TriggersActionsSplitContainer)).BeginInit();
            this.TriggersActionsSplitContainer.Panel1.SuspendLayout();
            this.TriggersActionsSplitContainer.Panel2.SuspendLayout();
            this.TriggersActionsSplitContainer.SuspendLayout();
            this.TriggersPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TriggersDataGridView)).BeginInit();
            this.ActionsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ActionsDataGridView)).BeginInit();
            this.TaskControlPanel.SuspendLayout();
            this.LeftPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ConnectionPanel
            // 
            this.ConnectionPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ConnectionPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ConnectionPanel.Controls.Add(this.ConnectionStatusLabel);
            this.ConnectionPanel.Controls.Add(this.ConnectionLabel);
            this.ConnectionPanel.Controls.Add(this.ConnectButton);
            this.ConnectionPanel.Controls.Add(this.PortTextBox);
            this.ConnectionPanel.Controls.Add(this.IPAddressTextBox);
            this.ConnectionPanel.Controls.Add(this.PortLabel);
            this.ConnectionPanel.Controls.Add(this.IPAddressLabel);
            this.ConnectionPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.ConnectionPanel.Location = new System.Drawing.Point(0, 0);
            this.ConnectionPanel.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ConnectionPanel.Name = "ConnectionPanel";
            this.ConnectionPanel.Size = new System.Drawing.Size(263, 106);
            this.ConnectionPanel.TabIndex = 0;
            // 
            // ConnectionStatusLabel
            // 
            this.ConnectionStatusLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ConnectionStatusLabel.AutoSize = true;
            this.ConnectionStatusLabel.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.ConnectionStatusLabel.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.ConnectionStatusLabel.Location = new System.Drawing.Point(157, 12);
            this.ConnectionStatusLabel.Name = "ConnectionStatusLabel";
            this.ConnectionStatusLabel.Size = new System.Drawing.Size(115, 23);
            this.ConnectionStatusLabel.TabIndex = 6;
            this.ConnectionStatusLabel.Text = "Подключено";
            this.ConnectionStatusLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ConnectionStatusLabel.Visible = false;
            // 
            // ConnectionLabel
            // 
            this.ConnectionLabel.AutoSize = true;
            this.ConnectionLabel.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.ConnectionLabel.Location = new System.Drawing.Point(12, 12);
            this.ConnectionLabel.Name = "ConnectionLabel";
            this.ConnectionLabel.Size = new System.Drawing.Size(124, 23);
            this.ConnectionLabel.TabIndex = 5;
            this.ConnectionLabel.Text = "Подключение";
            // 
            // ConnectButton
            // 
            this.ConnectButton.Location = new System.Drawing.Point(152, 68);
            this.ConnectButton.Name = "ConnectButton";
            this.ConnectButton.Size = new System.Drawing.Size(98, 25);
            this.ConnectButton.TabIndex = 4;
            this.ConnectButton.Text = "Подключиться";
            this.ConnectButton.UseVisualStyleBackColor = true;
            this.ConnectButton.Click += new System.EventHandler(this.ConnectButton_Click);
            // 
            // PortTextBox
            // 
            this.PortTextBox.Location = new System.Drawing.Point(74, 69);
            this.PortTextBox.Name = "PortTextBox";
            this.PortTextBox.Size = new System.Drawing.Size(72, 27);
            this.PortTextBox.TabIndex = 3;
            this.PortTextBox.Text = "1366";
            // 
            // IPAddressTextBox
            // 
            this.IPAddressTextBox.Location = new System.Drawing.Point(74, 40);
            this.IPAddressTextBox.Name = "IPAddressTextBox";
            this.IPAddressTextBox.Size = new System.Drawing.Size(176, 27);
            this.IPAddressTextBox.TabIndex = 2;
            this.IPAddressTextBox.Text = "127.0.0.1";
            // 
            // PortLabel
            // 
            this.PortLabel.AutoSize = true;
            this.PortLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PortLabel.Location = new System.Drawing.Point(12, 72);
            this.PortLabel.Name = "PortLabel";
            this.PortLabel.Size = new System.Drawing.Size(47, 20);
            this.PortLabel.TabIndex = 1;
            this.PortLabel.Text = "Порт:";
            // 
            // IPAddressLabel
            // 
            this.IPAddressLabel.AutoSize = true;
            this.IPAddressLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.IPAddressLabel.Location = new System.Drawing.Point(12, 42);
            this.IPAddressLabel.Name = "IPAddressLabel";
            this.IPAddressLabel.Size = new System.Drawing.Size(70, 20);
            this.IPAddressLabel.TabIndex = 0;
            this.IPAddressLabel.Text = "IP Адрес:";
            // 
            // TaskListPanel
            // 
            this.TaskListPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.TaskListPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TaskListPanel.Controls.Add(this.NewTaskButton);
            this.TaskListPanel.Controls.Add(this.EditTaskButton);
            this.TaskListPanel.Controls.Add(this.DeleteTaskButton);
            this.TaskListPanel.Controls.Add(this.TaskListDataGridView);
            this.TaskListPanel.Controls.Add(this.TaskListLabel);
            this.TaskListPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TaskListPanel.Enabled = false;
            this.TaskListPanel.Location = new System.Drawing.Point(0, 114);
            this.TaskListPanel.Name = "TaskListPanel";
            this.TaskListPanel.Size = new System.Drawing.Size(263, 390);
            this.TaskListPanel.TabIndex = 6;
            // 
            // NewTaskButton
            // 
            this.NewTaskButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.NewTaskButton.Enabled = false;
            this.NewTaskButton.Location = new System.Drawing.Point(12, 355);
            this.NewTaskButton.Name = "NewTaskButton";
            this.NewTaskButton.Size = new System.Drawing.Size(79, 25);
            this.NewTaskButton.TabIndex = 8;
            this.NewTaskButton.Text = "Включить";
            this.NewTaskButton.UseVisualStyleBackColor = true;
            this.NewTaskButton.Click += new System.EventHandler(this.NewTaskButton_Click);
            // 
            // EditTaskButton
            // 
            this.EditTaskButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.EditTaskButton.Enabled = false;
            this.EditTaskButton.Location = new System.Drawing.Point(97, 355);
            this.EditTaskButton.Name = "EditTaskButton";
            this.EditTaskButton.Size = new System.Drawing.Size(74, 25);
            this.EditTaskButton.TabIndex = 7;
            this.EditTaskButton.Text = "Изменить";
            this.EditTaskButton.UseVisualStyleBackColor = true;
            this.EditTaskButton.Click += new System.EventHandler(this.EditTaskButton_Click);
            // 
            // DeleteTaskButton
            // 
            this.DeleteTaskButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.DeleteTaskButton.Enabled = false;
            this.DeleteTaskButton.Location = new System.Drawing.Point(177, 355);
            this.DeleteTaskButton.Name = "DeleteTaskButton";
            this.DeleteTaskButton.Size = new System.Drawing.Size(73, 25);
            this.DeleteTaskButton.TabIndex = 6;
            this.DeleteTaskButton.Text = "Удалить";
            this.DeleteTaskButton.UseVisualStyleBackColor = true;
            this.DeleteTaskButton.Click += new System.EventHandler(this.DeleteTaskButton_Click);
            // 
            // TaskListDataGridView
            // 
            this.TaskListDataGridView.AllowUserToAddRows = false;
            this.TaskListDataGridView.AllowUserToDeleteRows = false;
            this.TaskListDataGridView.AllowUserToResizeColumns = false;
            this.TaskListDataGridView.AllowUserToResizeRows = false;
            this.TaskListDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TaskListDataGridView.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TaskListDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.TaskListDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TaskListDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TaskName,
            this.TaskEnable});
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.TaskListDataGridView.DefaultCellStyle = dataGridViewCellStyle18;
            this.TaskListDataGridView.Location = new System.Drawing.Point(12, 38);
            this.TaskListDataGridView.MultiSelect = false;
            this.TaskListDataGridView.Name = "TaskListDataGridView";
            this.TaskListDataGridView.ReadOnly = true;
            this.TaskListDataGridView.RowHeadersVisible = false;
            this.TaskListDataGridView.RowHeadersWidth = 51;
            this.TaskListDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TaskListDataGridView.Size = new System.Drawing.Size(238, 311);
            this.TaskListDataGridView.TabIndex = 6;
            this.TaskListDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.TaskListDataGridView_CellFormatting);
            this.TaskListDataGridView.SelectionChanged += new System.EventHandler(this.TaskListDataGridView_SelectionChanged);
            this.TaskListDataGridView.DoubleClick += new System.EventHandler(this.TaskListDataGridView_DoubleClick);
            // 
            // TaskName
            // 
            this.TaskName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TaskName.DataPropertyName = "Name";
            this.TaskName.HeaderText = "Название";
            this.TaskName.MinimumWidth = 6;
            this.TaskName.Name = "TaskName";
            this.TaskName.ReadOnly = true;
            // 
            // TaskEnable
            // 
            this.TaskEnable.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.TaskEnable.DataPropertyName = "Enable";
            this.TaskEnable.HeaderText = "Работает";
            this.TaskEnable.MinimumWidth = 6;
            this.TaskEnable.Name = "TaskEnable";
            this.TaskEnable.ReadOnly = true;
            this.TaskEnable.Width = 101;
            // 
            // TaskListLabel
            // 
            this.TaskListLabel.AutoSize = true;
            this.TaskListLabel.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.TaskListLabel.Location = new System.Drawing.Point(12, 12);
            this.TaskListLabel.Name = "TaskListLabel";
            this.TaskListLabel.Size = new System.Drawing.Size(119, 23);
            this.TaskListLabel.TabIndex = 5;
            this.TaskListLabel.Text = "Список задач";
            // 
            // TaskPanel
            // 
            this.TaskPanel.BackColor = System.Drawing.SystemColors.Control;
            this.TaskPanel.Controls.Add(this.TriggersActionsSplitContainer);
            this.TaskPanel.Controls.Add(this.TaskControlPanel);
            this.TaskPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TaskPanel.Enabled = false;
            this.TaskPanel.Location = new System.Drawing.Point(308, 8);
            this.TaskPanel.Name = "TaskPanel";
            this.TaskPanel.Size = new System.Drawing.Size(676, 504);
            this.TaskPanel.TabIndex = 7;
            // 
            // TriggersActionsSplitContainer
            // 
            this.TriggersActionsSplitContainer.BackColor = System.Drawing.SystemColors.Control;
            this.TriggersActionsSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TriggersActionsSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.TriggersActionsSplitContainer.Name = "TriggersActionsSplitContainer";
            this.TriggersActionsSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // TriggersActionsSplitContainer.Panel1
            // 
            this.TriggersActionsSplitContainer.Panel1.Controls.Add(this.TriggersPanel);
            this.TriggersActionsSplitContainer.Panel1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 8);
            this.TriggersActionsSplitContainer.Panel1MinSize = 200;
            // 
            // TriggersActionsSplitContainer.Panel2
            // 
            this.TriggersActionsSplitContainer.Panel2.Controls.Add(this.ActionsPanel);
            this.TriggersActionsSplitContainer.Panel2.Controls.Add(this.ActionsSpaceTaskPanel);
            this.TriggersActionsSplitContainer.Size = new System.Drawing.Size(676, 403);
            this.TriggersActionsSplitContainer.SplitterDistance = 202;
            this.TriggersActionsSplitContainer.TabIndex = 12;
            // 
            // TriggersPanel
            // 
            this.TriggersPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.TriggersPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TriggersPanel.Controls.Add(this.EditTriggerButton);
            this.TriggersPanel.Controls.Add(this.EnableTriggerButton);
            this.TriggersPanel.Controls.Add(this.TriggersLabel);
            this.TriggersPanel.Controls.Add(this.AddTriggerButton);
            this.TriggersPanel.Controls.Add(this.DeleteTriggerButton);
            this.TriggersPanel.Controls.Add(this.TriggersDataGridView);
            this.TriggersPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TriggersPanel.Location = new System.Drawing.Point(0, 0);
            this.TriggersPanel.Name = "TriggersPanel";
            this.TriggersPanel.Size = new System.Drawing.Size(676, 194);
            this.TriggersPanel.TabIndex = 12;
            // 
            // EditTriggerButton
            // 
            this.EditTriggerButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.EditTriggerButton.Enabled = false;
            this.EditTriggerButton.Location = new System.Drawing.Point(188, 156);
            this.EditTriggerButton.Name = "EditTriggerButton";
            this.EditTriggerButton.Size = new System.Drawing.Size(82, 25);
            this.EditTriggerButton.TabIndex = 13;
            this.EditTriggerButton.Text = "Изменить";
            this.EditTriggerButton.UseVisualStyleBackColor = true;
            this.EditTriggerButton.Click += new System.EventHandler(this.EditTriggerButton_Click);
            // 
            // EnableTriggerButton
            // 
            this.EnableTriggerButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.EnableTriggerButton.Enabled = false;
            this.EnableTriggerButton.Location = new System.Drawing.Point(12, 156);
            this.EnableTriggerButton.Name = "EnableTriggerButton";
            this.EnableTriggerButton.Size = new System.Drawing.Size(82, 25);
            this.EnableTriggerButton.TabIndex = 12;
            this.EnableTriggerButton.Text = "Включить";
            this.EnableTriggerButton.UseVisualStyleBackColor = true;
            this.EnableTriggerButton.Click += new System.EventHandler(this.EnableTriggerButton_Click);
            // 
            // TriggersLabel
            // 
            this.TriggersLabel.AutoSize = true;
            this.TriggersLabel.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.TriggersLabel.Location = new System.Drawing.Point(12, 12);
            this.TriggersLabel.Name = "TriggersLabel";
            this.TriggersLabel.Size = new System.Drawing.Size(84, 23);
            this.TriggersLabel.TabIndex = 6;
            this.TriggersLabel.Text = "Триггеры";
            // 
            // AddTriggerButton
            // 
            this.AddTriggerButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.AddTriggerButton.Location = new System.Drawing.Point(581, 156);
            this.AddTriggerButton.Name = "AddTriggerButton";
            this.AddTriggerButton.Size = new System.Drawing.Size(82, 25);
            this.AddTriggerButton.TabIndex = 11;
            this.AddTriggerButton.Text = "Добавить";
            this.AddTriggerButton.UseVisualStyleBackColor = true;
            this.AddTriggerButton.Click += new System.EventHandler(this.AddTriggerButton_Click);
            // 
            // DeleteTriggerButton
            // 
            this.DeleteTriggerButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.DeleteTriggerButton.Enabled = false;
            this.DeleteTriggerButton.Location = new System.Drawing.Point(100, 156);
            this.DeleteTriggerButton.Name = "DeleteTriggerButton";
            this.DeleteTriggerButton.Size = new System.Drawing.Size(82, 25);
            this.DeleteTriggerButton.TabIndex = 9;
            this.DeleteTriggerButton.Text = "Удалить";
            this.DeleteTriggerButton.UseVisualStyleBackColor = true;
            this.DeleteTriggerButton.Click += new System.EventHandler(this.DeleteTriggerButton_Click);
            // 
            // TriggersDataGridView
            // 
            this.TriggersDataGridView.AllowUserToAddRows = false;
            this.TriggersDataGridView.AllowUserToDeleteRows = false;
            this.TriggersDataGridView.AllowUserToResizeColumns = false;
            this.TriggersDataGridView.AllowUserToResizeRows = false;
            this.TriggersDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TriggersDataGridView.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.TriggersDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.TriggersDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TriggersDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TriggerEnable,
            this.TriggerType,
            this.Deteils});
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.TriggersDataGridView.DefaultCellStyle = dataGridViewCellStyle22;
            this.TriggersDataGridView.Location = new System.Drawing.Point(12, 36);
            this.TriggersDataGridView.MultiSelect = false;
            this.TriggersDataGridView.Name = "TriggersDataGridView";
            this.TriggersDataGridView.ReadOnly = true;
            this.TriggersDataGridView.RowHeadersVisible = false;
            this.TriggersDataGridView.RowHeadersWidth = 51;
            this.TriggersDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TriggersDataGridView.Size = new System.Drawing.Size(651, 114);
            this.TriggersDataGridView.TabIndex = 9;
            this.TriggersDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.TriggersDataGridView_CellFormatting);
            this.TriggersDataGridView.SelectionChanged += new System.EventHandler(this.TriggersDataGridView_SelectionChanged);
            // 
            // TriggerEnable
            // 
            this.TriggerEnable.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.TriggerEnable.DataPropertyName = "Enable";
            dataGridViewCellStyle20.NullValue = "...";
            this.TriggerEnable.DefaultCellStyle = dataGridViewCellStyle20;
            this.TriggerEnable.HeaderText = "Работает";
            this.TriggerEnable.MinimumWidth = 6;
            this.TriggerEnable.Name = "TriggerEnable";
            this.TriggerEnable.ReadOnly = true;
            this.TriggerEnable.Width = 101;
            // 
            // TriggerType
            // 
            this.TriggerType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.TriggerType.DataPropertyName = "Type";
            this.TriggerType.HeaderText = "Тип триггера";
            this.TriggerType.MinimumWidth = 6;
            this.TriggerType.Name = "TriggerType";
            this.TriggerType.ReadOnly = true;
            this.TriggerType.Width = 128;
            // 
            // Deteils
            // 
            this.Deteils.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Deteils.DataPropertyName = "Deteil";
            dataGridViewCellStyle21.Format = "...";
            this.Deteils.DefaultCellStyle = dataGridViewCellStyle21;
            this.Deteils.HeaderText = "Детали";
            this.Deteils.MinimumWidth = 6;
            this.Deteils.Name = "Deteils";
            this.Deteils.ReadOnly = true;
            // 
            // ActionsPanel
            // 
            this.ActionsPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ActionsPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ActionsPanel.Controls.Add(this.EditActionButton);
            this.ActionsPanel.Controls.Add(this.EnableActionButton);
            this.ActionsPanel.Controls.Add(this.ActionsLabel);
            this.ActionsPanel.Controls.Add(this.AddActionButton);
            this.ActionsPanel.Controls.Add(this.DeleteActionButton);
            this.ActionsPanel.Controls.Add(this.ActionsDataGridView);
            this.ActionsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ActionsPanel.Location = new System.Drawing.Point(0, 0);
            this.ActionsPanel.Name = "ActionsPanel";
            this.ActionsPanel.Size = new System.Drawing.Size(676, 189);
            this.ActionsPanel.TabIndex = 17;
            // 
            // EditActionButton
            // 
            this.EditActionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.EditActionButton.Enabled = false;
            this.EditActionButton.Location = new System.Drawing.Point(188, 150);
            this.EditActionButton.Name = "EditActionButton";
            this.EditActionButton.Size = new System.Drawing.Size(82, 25);
            this.EditActionButton.TabIndex = 14;
            this.EditActionButton.Text = "Изменить";
            this.EditActionButton.UseVisualStyleBackColor = true;
            this.EditActionButton.Click += new System.EventHandler(this.EditActionButton_Click);
            // 
            // EnableActionButton
            // 
            this.EnableActionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.EnableActionButton.Enabled = false;
            this.EnableActionButton.Location = new System.Drawing.Point(12, 150);
            this.EnableActionButton.Name = "EnableActionButton";
            this.EnableActionButton.Size = new System.Drawing.Size(82, 25);
            this.EnableActionButton.TabIndex = 17;
            this.EnableActionButton.Text = "Включить";
            this.EnableActionButton.UseVisualStyleBackColor = true;
            this.EnableActionButton.Click += new System.EventHandler(this.EnableActionButton_Click);
            // 
            // ActionsLabel
            // 
            this.ActionsLabel.AutoSize = true;
            this.ActionsLabel.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.ActionsLabel.Location = new System.Drawing.Point(12, 12);
            this.ActionsLabel.Name = "ActionsLabel";
            this.ActionsLabel.Size = new System.Drawing.Size(84, 23);
            this.ActionsLabel.TabIndex = 16;
            this.ActionsLabel.Text = "Действия";
            // 
            // AddActionButton
            // 
            this.AddActionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.AddActionButton.Location = new System.Drawing.Point(581, 150);
            this.AddActionButton.Name = "AddActionButton";
            this.AddActionButton.Size = new System.Drawing.Size(82, 25);
            this.AddActionButton.TabIndex = 15;
            this.AddActionButton.Text = "Добавить";
            this.AddActionButton.UseVisualStyleBackColor = true;
            this.AddActionButton.Click += new System.EventHandler(this.AddActionButton_Click);
            // 
            // DeleteActionButton
            // 
            this.DeleteActionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.DeleteActionButton.Enabled = false;
            this.DeleteActionButton.Location = new System.Drawing.Point(100, 150);
            this.DeleteActionButton.Name = "DeleteActionButton";
            this.DeleteActionButton.Size = new System.Drawing.Size(82, 25);
            this.DeleteActionButton.TabIndex = 12;
            this.DeleteActionButton.Text = "Удалить";
            this.DeleteActionButton.UseVisualStyleBackColor = true;
            this.DeleteActionButton.Click += new System.EventHandler(this.DeleteActionButton_Click);
            // 
            // ActionsDataGridView
            // 
            this.ActionsDataGridView.AllowUserToAddRows = false;
            this.ActionsDataGridView.AllowUserToDeleteRows = false;
            this.ActionsDataGridView.AllowUserToResizeColumns = false;
            this.ActionsDataGridView.AllowUserToResizeRows = false;
            this.ActionsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ActionsDataGridView.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ActionsDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.ActionsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ActionsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ActionEnable,
            this.ActionPath,
            this.ActionArguments,
            this.ActionWait});
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ActionsDataGridView.DefaultCellStyle = dataGridViewCellStyle24;
            this.ActionsDataGridView.Location = new System.Drawing.Point(12, 36);
            this.ActionsDataGridView.MultiSelect = false;
            this.ActionsDataGridView.Name = "ActionsDataGridView";
            this.ActionsDataGridView.ReadOnly = true;
            this.ActionsDataGridView.RowHeadersVisible = false;
            this.ActionsDataGridView.RowHeadersWidth = 51;
            this.ActionsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ActionsDataGridView.Size = new System.Drawing.Size(651, 108);
            this.ActionsDataGridView.TabIndex = 13;
            this.ActionsDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.ActionsDataGridView_CellFormatting);
            this.ActionsDataGridView.SelectionChanged += new System.EventHandler(this.ActionsDataGridView_SelectionChanged);
            // 
            // ActionEnable
            // 
            this.ActionEnable.DataPropertyName = "Enable";
            this.ActionEnable.HeaderText = "Работает";
            this.ActionEnable.MinimumWidth = 6;
            this.ActionEnable.Name = "ActionEnable";
            this.ActionEnable.ReadOnly = true;
            this.ActionEnable.Width = 125;
            // 
            // ActionPath
            // 
            this.ActionPath.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ActionPath.DataPropertyName = "Path";
            this.ActionPath.HeaderText = "Путь";
            this.ActionPath.MinimumWidth = 6;
            this.ActionPath.Name = "ActionPath";
            this.ActionPath.ReadOnly = true;
            // 
            // ActionArguments
            // 
            this.ActionArguments.DataPropertyName = "Arguments";
            this.ActionArguments.HeaderText = "Аргументы";
            this.ActionArguments.MinimumWidth = 6;
            this.ActionArguments.Name = "ActionArguments";
            this.ActionArguments.ReadOnly = true;
            this.ActionArguments.Width = 125;
            // 
            // ActionWait
            // 
            this.ActionWait.DataPropertyName = "Wait";
            this.ActionWait.HeaderText = "Ожидать";
            this.ActionWait.MinimumWidth = 6;
            this.ActionWait.Name = "ActionWait";
            this.ActionWait.ReadOnly = true;
            this.ActionWait.Width = 125;
            // 
            // ActionsSpaceTaskPanel
            // 
            this.ActionsSpaceTaskPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ActionsSpaceTaskPanel.Location = new System.Drawing.Point(0, 189);
            this.ActionsSpaceTaskPanel.Name = "ActionsSpaceTaskPanel";
            this.ActionsSpaceTaskPanel.Size = new System.Drawing.Size(676, 8);
            this.ActionsSpaceTaskPanel.TabIndex = 18;
            // 
            // TaskControlPanel
            // 
            this.TaskControlPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.TaskControlPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TaskControlPanel.Controls.Add(this.AboutTaskLabel);
            this.TaskControlPanel.Controls.Add(this.AboutTextBox);
            this.TaskControlPanel.Controls.Add(this.TaskNameTextBox);
            this.TaskControlPanel.Controls.Add(this.SendTaskButton);
            this.TaskControlPanel.Controls.Add(this.CancelTaskButton);
            this.TaskControlPanel.Controls.Add(this.TaskNameLabel);
            this.TaskControlPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TaskControlPanel.Location = new System.Drawing.Point(0, 403);
            this.TaskControlPanel.Name = "TaskControlPanel";
            this.TaskControlPanel.Size = new System.Drawing.Size(676, 101);
            this.TaskControlPanel.TabIndex = 16;
            // 
            // AboutTaskLabel
            // 
            this.AboutTaskLabel.AutoSize = true;
            this.AboutTaskLabel.Font = new System.Drawing.Font("Segoe UI Semibold", 10F);
            this.AboutTaskLabel.Location = new System.Drawing.Point(8, 41);
            this.AboutTaskLabel.Name = "AboutTaskLabel";
            this.AboutTaskLabel.Size = new System.Drawing.Size(154, 23);
            this.AboutTaskLabel.TabIndex = 17;
            this.AboutTaskLabel.Text = "Описание задачи:";
            // 
            // AboutTextBox
            // 
            this.AboutTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AboutTextBox.Location = new System.Drawing.Point(139, 38);
            this.AboutTextBox.Multiline = true;
            this.AboutTextBox.Name = "AboutTextBox";
            this.AboutTextBox.Size = new System.Drawing.Size(436, 53);
            this.AboutTextBox.TabIndex = 16;
            this.AboutTextBox.TextChanged += new System.EventHandler(this.AboutTextBox_TextChanged);
            // 
            // TaskNameTextBox
            // 
            this.TaskNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TaskNameTextBox.Location = new System.Drawing.Point(139, 9);
            this.TaskNameTextBox.Name = "TaskNameTextBox";
            this.TaskNameTextBox.Size = new System.Drawing.Size(524, 27);
            this.TaskNameTextBox.TabIndex = 15;
            this.TaskNameTextBox.TextChanged += new System.EventHandler(this.TaskNameTextBox_TextChanged);
            // 
            // SendTaskButton
            // 
            this.SendTaskButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SendTaskButton.Location = new System.Drawing.Point(584, 67);
            this.SendTaskButton.Name = "SendTaskButton";
            this.SendTaskButton.Size = new System.Drawing.Size(79, 25);
            this.SendTaskButton.TabIndex = 9;
            this.SendTaskButton.Text = "Добавить";
            this.SendTaskButton.UseVisualStyleBackColor = true;
            this.SendTaskButton.Click += new System.EventHandler(this.SendTaskButton_Click);
            // 
            // CancelTaskButton
            // 
            this.CancelTaskButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelTaskButton.Location = new System.Drawing.Point(584, 38);
            this.CancelTaskButton.Name = "CancelTaskButton";
            this.CancelTaskButton.Size = new System.Drawing.Size(79, 25);
            this.CancelTaskButton.TabIndex = 13;
            this.CancelTaskButton.Text = "Очистить";
            this.CancelTaskButton.UseVisualStyleBackColor = true;
            this.CancelTaskButton.Click += new System.EventHandler(this.CancelTaskButton_Click);
            // 
            // TaskNameLabel
            // 
            this.TaskNameLabel.AutoSize = true;
            this.TaskNameLabel.Font = new System.Drawing.Font("Segoe UI Semibold", 10F);
            this.TaskNameLabel.Location = new System.Drawing.Point(8, 10);
            this.TaskNameLabel.Name = "TaskNameLabel";
            this.TaskNameLabel.Size = new System.Drawing.Size(152, 23);
            this.TaskNameLabel.TabIndex = 14;
            this.TaskNameLabel.Text = "Название задачи:";
            // 
            // LeftPanel
            // 
            this.LeftPanel.BackColor = System.Drawing.SystemColors.Control;
            this.LeftPanel.Controls.Add(this.TaskListPanel);
            this.LeftPanel.Controls.Add(this.ConnectSpaceTaskListlPanel);
            this.LeftPanel.Controls.Add(this.ConnectionPanel);
            this.LeftPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftPanel.Location = new System.Drawing.Point(8, 8);
            this.LeftPanel.Name = "LeftPanel";
            this.LeftPanel.Size = new System.Drawing.Size(263, 504);
            this.LeftPanel.TabIndex = 8;
            // 
            // ConnectSpaceTaskListlPanel
            // 
            this.ConnectSpaceTaskListlPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.ConnectSpaceTaskListlPanel.Location = new System.Drawing.Point(0, 106);
            this.ConnectSpaceTaskListlPanel.Name = "ConnectSpaceTaskListlPanel";
            this.ConnectSpaceTaskListlPanel.Size = new System.Drawing.Size(263, 8);
            this.ConnectSpaceTaskListlPanel.TabIndex = 0;
            // 
            // SpacingVerticalPanel
            // 
            this.SpacingVerticalPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.SpacingVerticalPanel.Location = new System.Drawing.Point(271, 8);
            this.SpacingVerticalPanel.Name = "SpacingVerticalPanel";
            this.SpacingVerticalPanel.Size = new System.Drawing.Size(37, 504);
            this.SpacingVerticalPanel.TabIndex = 9;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(992, 520);
            this.Controls.Add(this.TaskPanel);
            this.Controls.Add(this.SpacingVerticalPanel);
            this.Controls.Add(this.LeftPanel);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(700, 500);
            this.Name = "MainForm";
            this.Padding = new System.Windows.Forms.Padding(8);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Главное окно";
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyUp);
            this.ConnectionPanel.ResumeLayout(false);
            this.ConnectionPanel.PerformLayout();
            this.TaskListPanel.ResumeLayout(false);
            this.TaskListPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TaskListDataGridView)).EndInit();
            this.TaskPanel.ResumeLayout(false);
            this.TriggersActionsSplitContainer.Panel1.ResumeLayout(false);
            this.TriggersActionsSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TriggersActionsSplitContainer)).EndInit();
            this.TriggersActionsSplitContainer.ResumeLayout(false);
            this.TriggersPanel.ResumeLayout(false);
            this.TriggersPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TriggersDataGridView)).EndInit();
            this.ActionsPanel.ResumeLayout(false);
            this.ActionsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ActionsDataGridView)).EndInit();
            this.TaskControlPanel.ResumeLayout(false);
            this.TaskControlPanel.PerformLayout();
            this.LeftPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel ConnectionPanel;
        private System.Windows.Forms.TextBox IPAddressTextBox;
        private System.Windows.Forms.Label PortLabel;
        private System.Windows.Forms.Label IPAddressLabel;
        private System.Windows.Forms.TextBox PortTextBox;
        private System.Windows.Forms.Button ConnectButton;
        private System.Windows.Forms.Label ConnectionLabel;
        private System.Windows.Forms.Panel TaskListPanel;
        private System.Windows.Forms.Label TaskListLabel;
        private System.Windows.Forms.DataGridView TaskListDataGridView;
        private System.Windows.Forms.Panel TaskPanel;
        private System.Windows.Forms.Panel LeftPanel;
        private System.Windows.Forms.Panel ConnectSpaceTaskListlPanel;
        private System.Windows.Forms.Panel SpacingVerticalPanel;
        private System.Windows.Forms.Button DeleteTaskButton;
        private System.Windows.Forms.Button EditTaskButton;
        private System.Windows.Forms.Label TriggersLabel;
        private System.Windows.Forms.DataGridView TriggersDataGridView;
        private System.Windows.Forms.Button AddTriggerButton;
        private System.Windows.Forms.Button DeleteTriggerButton;
        private System.Windows.Forms.SplitContainer TriggersActionsSplitContainer;
        private System.Windows.Forms.Button SendTaskButton;
        private System.Windows.Forms.Label ActionsLabel;
        private System.Windows.Forms.Button DeleteActionButton;
        private System.Windows.Forms.Button AddActionButton;
        private System.Windows.Forms.DataGridView ActionsDataGridView;
        private System.Windows.Forms.Button CancelTaskButton;
        private System.Windows.Forms.TextBox TaskNameTextBox;
        private System.Windows.Forms.Label TaskNameLabel;
        private System.Windows.Forms.Panel TriggersPanel;
        private System.Windows.Forms.Panel ActionsPanel;
        private System.Windows.Forms.Panel ActionsSpaceTaskPanel;
        private System.Windows.Forms.Panel TaskControlPanel;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActionEnable;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActionPath;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActionArguments;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActionWait;
        private System.Windows.Forms.Label AboutTaskLabel;
        private System.Windows.Forms.TextBox AboutTextBox;
        private System.Windows.Forms.Button NewTaskButton;
        private System.Windows.Forms.Button EnableTriggerButton;
        private System.Windows.Forms.Button EnableActionButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn TriggerEnable;
        private System.Windows.Forms.DataGridViewTextBoxColumn TriggerType;
        private System.Windows.Forms.DataGridViewTextBoxColumn Deteils;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaskName;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaskEnable;
        private System.Windows.Forms.Label ConnectionStatusLabel;
        private System.Windows.Forms.Button EditTriggerButton;
        private System.Windows.Forms.Button EditActionButton;
    }
}

