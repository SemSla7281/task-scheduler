﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tasks.Client.Model;

namespace Tasks.Client
{
    public partial class TriggerForm : Form
    {
        private bool animating;     
        private readonly bool[] months;
        private readonly bool[] weekDays;
        private readonly bool[] weekDaysMonthly;
        private readonly bool[] monthDays;
        private System.Threading.Tasks.Task animationTask;

        private readonly List<string> monthShortNames = new List<string>
        {
            "Янв.", "Фев.", "Мар.", "Апр.",  "Май", "Июн.",
            "Июл.", "Авг.", "Сен.", "Окт.", "Ноя.", "Дек." 
        };

        private readonly List<string> weekDayShortNames = new List<string>
        {
            "ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ", "ВС" 
        };

        public Trigger Trigger { private set; get; }

        public TriggerForm()
        {
            InitializeComponent();
            animating = false;
            animationTask = null;
            Trigger = null;
            months = Enumerable.Repeat(false, 12).ToArray();
            weekDays = Enumerable.Repeat(false, 7).ToArray();
            weekDaysMonthly = Enumerable.Repeat(false, 7).ToArray();
            monthDays = Enumerable.Repeat(false, 31).ToArray();
        }

        public TriggerForm(Trigger trigger)
        {
            InitializeComponent();
            animating = false;
            animationTask = null;
            Trigger = null;
            months = Enumerable.Repeat(false, 12).ToArray();
            weekDays = Enumerable.Repeat(false, 7).ToArray();
            weekDaysMonthly = Enumerable.Repeat(false, 7).ToArray();
            monthDays = Enumerable.Repeat(false, 31).ToArray();
            switch (trigger.Type)
            {
                case TriggerType.Oneshot:
                    Height = 190;
                    OneshotDatePicker.Value = trigger.Date;
                    OneshotTimePicker.Value = trigger.Time;
                    OneshotRadioButton.Checked = true;
                    break;
                case TriggerType.Daily:
                    Height = 190;
                    DailyTimePicker.Value = trigger.Time;
                    FreauncyDailyNumericUpDown.Value = trigger.Frequancy;
                    DailyRadioButton.Checked = true;
                    break;
                case TriggerType.Weekly:
                    Height = 240;
                    MondayCheckBox.Checked = trigger.WeekDays[0];
                    TuesdayCheckbox.Checked = trigger.WeekDays[1];
                    WednesdayCheckbox.Checked = trigger.WeekDays[2];
                    ThursdayCheckBox.Checked = trigger.WeekDays[3];
                    FridayCheckBox.Checked = trigger.WeekDays[4];
                    SaturdayCheckBox.Checked = trigger.WeekDays[5];
                    SundayCheckBox.Checked = trigger.WeekDays[6];
                    WeeklyTimePicker.Value = trigger.Time;
                    FrequancyWeekNumericUpDown.Value = trigger.Frequancy;
                    WeeklyRadioButton.Checked = true;
                    break;
                case TriggerType.Monthly:
                    MonthlyTimePicker.Value = trigger.Time;
                    JanuaryCheckBox.Checked = trigger.Months[0];
                    FebruaryCheckBox.Checked = trigger.Months[1];
                    MarchCheckBox.Checked = trigger.Months[2];
                    AprileCheckBox.Checked = trigger.Months[3];
                    MayСheckBox.Checked = trigger.Months[4];
                    JuneCheckBox.Checked = trigger.Months[5];
                    JuleCheckBox.Checked = trigger.Months[6];
                    AugustCheckBox.Checked = trigger.Months[7];
                    SeptemberCheckBox.Checked = trigger.Months[8];
                    OctemberCheckBox.Checked = trigger.Months[9];
                    NonemberCheckBox.Checked = trigger.Months[10];
                    DecemberCheckBox.Checked = trigger.Months[11];
                    switch (trigger.MonthType)
                    {
                        case TriggerMonthType.WeekDays:
                            Height = 305;
                            MondayMonthlyCheckBox.Checked = trigger.WeekMonthlyDays[0];
                            TuesdayMonthlyCheckbox.Checked = trigger.WeekMonthlyDays[1];
                            WednesdayMonthlyCheckbox.Checked = trigger.WeekMonthlyDays[2];
                            ThursdayMonthlyCheckBox.Checked = trigger.WeekMonthlyDays[3];
                            FridayMonthlyCheckBox.Checked = trigger.WeekMonthlyDays[4];
                            SaturdayMonthlyCheckBox.Checked = trigger.WeekMonthlyDays[5];
                            SundayMonthlyCheckBox.Checked = trigger.WeekMonthlyDays[6];
                            WeekDaysRadioButton.Checked = true;
                            break;
                        case TriggerMonthType.MonthDays:
                            Height = 390;
                            MonthDay1CheckBox.Checked = trigger.MonthDays[0];
                            MonthDay2CheckBox.Checked = trigger.MonthDays[1];
                            MonthDay3CheckBox.Checked = trigger.MonthDays[2];
                            MonthDay4CheckBox.Checked = trigger.MonthDays[3];
                            MonthDay5CheckBox.Checked = trigger.MonthDays[4];
                            MonthDay6CheckBox.Checked = trigger.MonthDays[5];
                            MonthDay7CheckBox.Checked = trigger.MonthDays[6];
                            MonthDay8CheckBox.Checked = trigger.MonthDays[7];
                            MonthDay9CheckBox.Checked = trigger.MonthDays[8];
                            MonthDay10CheckBox.Checked = trigger.MonthDays[9];
                            MonthDay11CheckBox.Checked = trigger.MonthDays[10];
                            MonthDay12CheckBox.Checked = trigger.MonthDays[11];
                            MonthDay13CheckBox.Checked = trigger.MonthDays[12];
                            MonthDay14CheckBox.Checked = trigger.MonthDays[13];
                            MonthDay15CheckBox.Checked = trigger.MonthDays[14];
                            MonthDay16CheckBox.Checked = trigger.MonthDays[15];
                            MonthDay17CheckBox.Checked = trigger.MonthDays[16];
                            MonthDay18CheckBox.Checked = trigger.MonthDays[17];
                            MonthDay19CheckBox.Checked = trigger.MonthDays[18];
                            MonthDay20CheckBox.Checked = trigger.MonthDays[19];
                            MonthDay21CheckBox.Checked = trigger.MonthDays[20];
                            MonthDay22CheckBox.Checked = trigger.MonthDays[21];
                            MonthDay23CheckBox.Checked = trigger.MonthDays[22];
                            MonthDay24CheckBox.Checked = trigger.MonthDays[23];
                            MonthDay25CheckBox.Checked = trigger.MonthDays[24];
                            MonthDay26CheckBox.Checked = trigger.MonthDays[25];
                            MonthDay27CheckBox.Checked = trigger.MonthDays[26];
                            MonthDay28CheckBox.Checked = trigger.MonthDays[27];
                            MonthDay29CheckBox.Checked = trigger.MonthDays[28];
                            MonthDay30CheckBox.Checked = trigger.MonthDays[29];
                            MonthDay31CheckBox.Checked = trigger.MonthDays[30];
                            MonthDaysRadioButton.Checked = true;
                            break;
                    }
                    MonthlyRadioButton.Checked = true;
                    break;
            }
            AddButton.Text = "Изменить";
        }

        private void AnimateHeight(int height)
        {
            System.Threading.Tasks.Task.Run(() =>
            {
                if (animating)
                {
                    animating = false;
                    if (animationTask != null)
                        animationTask.Wait();
                }
                animating = true;
                animationTask = System.Threading.Tasks.Task.Run(() =>
                {
                    while (animating)
                    {
                        int currentHeight = 0;
                        try
                        {
                            Invoke(new System.Action(() => currentHeight = Height));
                            if (Math.Abs(currentHeight - height) > 5)
                                Invoke(new System.Action(() => Height += currentHeight - height < 0 ? 5 : -5));
                            else
                                break;
                            Thread.Sleep(1);
                        }
                        catch (Exception)
                        {
                            break;
                        }
                    }
                    animating = false;
                });
            });
        }

        private void OneshotRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (OneshotRadioButton.Checked)
            {
                AnimateHeight(190);
                DailyPanel.Hide();
                WeeklyPanel.Hide();
                MonthlyPanel.Hide();
                OneshotPanel.Show();
            }
        }

        private void DailyRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (DailyRadioButton.Checked)
            {
                AnimateHeight(190);
                OneshotPanel.Hide();
                WeeklyPanel.Hide();
                MonthlyPanel.Hide();
                DailyPanel.Show();
            }
        }

        private void WeeklyRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (WeeklyRadioButton.Checked)
            {
                AnimateHeight(240);
                OneshotPanel.Hide();         
                MonthlyPanel.Hide();
                DailyPanel.Hide();
                WeeklyPanel.Show();
            }
        }

        private void MonthlyRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (MonthlyRadioButton.Checked)
            {
                AnimateHeight(390);
                OneshotPanel.Hide();
                WeeklyPanel.Hide();
                DailyPanel.Hide();
                MonthlyPanel.Show();
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            var type = OneshotRadioButton.Checked ? TriggerType.Oneshot :
                DailyRadioButton.Checked ? TriggerType.Daily :
                WeeklyRadioButton.Checked ? TriggerType.Weekly : TriggerType.Monthly;
            switch (type)
            {
                case TriggerType.Oneshot:
                    {
                        var date = OneshotDatePicker.Value;
                        var time = OneshotTimePicker.Value;
                        Trigger = new Trigger
                        {
                            Type = type,
                            Frequancy = 0,
                            Date = date,
                            Time = time
                        };
                    }
                    break;
                case TriggerType.Daily:
                    {
                        var frequancy = (int)FreauncyDailyNumericUpDown.Value;
                        var time = DailyTimePicker.Value;
                        Trigger = new Trigger
                        {
                            Type = type,
                            Frequancy = frequancy,
                            Time = time
                        };
                    }
                    break;
                case TriggerType.Weekly:
                    {
                        var frequancy = (int)FrequancyWeekNumericUpDown.Value;
                        var time = WeeklyTimePicker.Value;
                        if (!weekDays.Contains(true))
                        {
                            MessageBox.Show("Не выбран не один день недели!", 
                                "Некорректный ввод!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }    
                        Trigger = new Trigger
                        {
                            Type = type,
                            Frequancy = frequancy,
                            Time = time,
                            WeekDays = weekDays
                        };
                    }
                    break;
                case TriggerType.Monthly:
                    {
                        var time = WeeklyTimePicker.Value;
                        var monthType = WeekDaysRadioButton.Checked ? 
                            TriggerMonthType.WeekDays : TriggerMonthType.MonthDays;
                        if (!months.Contains(true))
                        {
                            MessageBox.Show("Не выбран не один месяц!",
                                "Некорректный ввод!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        if (monthType == TriggerMonthType.MonthDays)
                        {
                            if (!monthDays.Contains(true))
                            {
                                MessageBox.Show("Не выбран не один день месяца!",
                                    "Некорректный ввод!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                        }
                        else
                        {
                            if (!weekDaysMonthly.Contains(true))
                            {
                                MessageBox.Show("Не выбран не один день недели!",
                                    "Некорректный ввод!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                        }
                        switch (monthType)
                        {
                            case TriggerMonthType.WeekDays:
                                Trigger = new Trigger
                                {
                                    Type = type,
                                    Time = time,
                                    Months = months,
                                    MonthType = monthType,
                                    WeekMonthlyDays = weekDaysMonthly
                                };
                                break;
                            case TriggerMonthType.MonthDays:
                                Trigger = new Trigger
                                {
                                    Type = type,
                                    Time = time,
                                    Months = months,
                                    MonthType = monthType,
                                    MonthDays = monthDays
                                };
                                break;
                        }
                    }
                    break;
            }
            DialogResult = DialogResult.OK;
            Close();
        }

        private void MonthDaysRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (MonthDaysRadioButton.Checked)
            {
                AnimateHeight(390);
                WeekDaysMonthlyPanel.Hide();
                MonthDaysPanel.Show();
            }
        }

        private void WeekDaysRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (WeekDaysRadioButton.Checked)
            {
                AnimateHeight(305);
                MonthDaysPanel.Hide();
                WeekDaysMonthlyPanel.Show();
            }
        }

        private void MonthCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            var checkBox = (CheckBox)sender;
            months[monthShortNames.IndexOf(checkBox.Text)] = checkBox.Checked;
        }

        private void WeekDayMonthlyCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            var checkBox = (CheckBox)sender;
            weekDaysMonthly[weekDayShortNames.IndexOf(checkBox.Text)] = checkBox.Checked;
        }

        private void MonthDayCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            var checkBox = (CheckBox)sender;
            monthDays[int.Parse(checkBox.Text) - 1] = checkBox.Checked;
        }

        private void WeekDayCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            var checkBox = (CheckBox)sender;
            weekDays[weekDayShortNames.IndexOf(checkBox.Text)] = checkBox.Checked;
        }
    }
}
