﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tasks.Client.Controls;

namespace Tasks.Client
{
    public partial class MainForm : Form
    {
        private ClientController clientController;
        private readonly BindingList<Model.Task> tasks;
        private Model.Task currentTask;
        private string currentTaskName;
        private bool edit;

        public MainForm()
        {
            InitializeComponent();
            clientController = new ClientController();
            tasks = new BindingList<Model.Task>();
            TaskListDataGridView.AutoGenerateColumns = false;
            TaskListDataGridView.DataSource = tasks;
            currentTask = new Model.Task();
            edit = false;
            TriggersDataGridView.AutoGenerateColumns = false;
            TriggersDataGridView.DataSource = currentTask.Triggers;
            ActionsDataGridView.AutoGenerateColumns = false;
            ActionsDataGridView.DataSource = currentTask.Actions;
            TaskNameTextBox.Text = currentTask.Name;
            AboutTextBox.Text = currentTask.About;
        }

        private void AddTriggerButton_Click(object sender, EventArgs e)
        {
            using (var form = new TriggerForm())
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    var trigger = form.Trigger;
                    currentTask.Triggers.Add(trigger);
                } 
            }
        }

        private void AddActionButton_Click(object sender, EventArgs e)
        {
            using (var form = new ActionForm())
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    var action = form.Action;
                    currentTask.Actions.Add(action);
                }
            }
        }

        private void HandleClient()
        {
            while (clientController.Connected)
            {
                var command = clientController.AcceptCommand();
                if (command.Success)
                {
                    switch (command.Value)
                    {
                        case Command.TakeTaskList:
                            {
                                var listSize = clientController.AcceptInt();
                                if (listSize.Success)
                                {
                                    Invoke(new System.Action(() => tasks.Clear()));
                                    for (int i = 0; i < listSize.Value; i++)
                                    {
                                        var task = clientController.AcceptTask();
                                        if (task.Success)
                                            Invoke(new System.Action(() => tasks.Add(task.Value)));
                                        else
                                            return;
                                    }
                                }
                                else
                                    return;
                            }
                            break;
                        case Command.TakeTask:
                            {
                                var task = clientController.AcceptTask();
                                if (task.Success)
                                {
                                    currentTask = task.Value;
                                    currentTaskName = task.Value.Name;
                                    edit = true;
                                    Invoke(new System.Action(() =>
                                    {
                                        SendTaskButton.Text = "Сохранить";
                                        CancelTaskButton.Text = "Закрыть";
                                        TriggersDataGridView.AutoGenerateColumns = false;
                                        TriggersDataGridView.DataSource = currentTask.Triggers;
                                        ActionsDataGridView.AutoGenerateColumns = false;
                                        ActionsDataGridView.DataSource = currentTask.Actions;
                                        TaskNameTextBox.Text = currentTask.Name;
                                        AboutTextBox.Text = currentTask.About;
                                    }));
                                }
                            }
                            break;
                        case Command.TaskAdded:
                            {
                                currentTask = new Model.Task();
                                Invoke(new System.Action(() =>
                                {
                                    TriggersDataGridView.AutoGenerateColumns = false;
                                    TriggersDataGridView.DataSource = currentTask.Triggers;
                                    ActionsDataGridView.AutoGenerateColumns = false;
                                    ActionsDataGridView.DataSource = currentTask.Actions;
                                    TaskNameTextBox.Text = currentTask.Name;
                                    AboutTextBox.Text = currentTask.About;
                                    CancelTaskButton.Text = "Очистить";
                                    SendTaskButton.Text = "Добавить";
                                }));
                            }
                            break;
                        case Command.TaskEdited:
                            {
                                currentTask = new Model.Task();
                                Invoke(new System.Action(() =>
                                {
                                    TriggersDataGridView.AutoGenerateColumns = false;
                                    TriggersDataGridView.DataSource = currentTask.Triggers;
                                    ActionsDataGridView.AutoGenerateColumns = false;
                                    ActionsDataGridView.DataSource = currentTask.Actions;
                                    TaskNameTextBox.Text = currentTask.Name;
                                    AboutTextBox.Text = currentTask.About;
                                    CancelTaskButton.Text = "Очистить";
                                    SendTaskButton.Text = "Добавить";
                                }));
                            }
                            break;
                        case Command.TaskDeleted:
                            {
                                var deletedName = clientController.AcceptString();
                                if (deletedName.Success)
                                {
                                    if (deletedName.Value == currentTask.Name)
                                    {
                                        Invoke(new System.Action(() =>
                                        {
                                            currentTask = new Model.Task();
                                            TriggersDataGridView.AutoGenerateColumns = false;
                                            TriggersDataGridView.DataSource = currentTask.Triggers;
                                            ActionsDataGridView.AutoGenerateColumns = false;
                                            ActionsDataGridView.DataSource = currentTask.Actions;
                                            TaskNameTextBox.Text = currentTask.Name;
                                            AboutTextBox.Text = currentTask.About;
                                            SendTaskButton.Text = "Добавить";
                                            CancelTaskButton.Text = "Очистить";
                                            edit = false;
                                        }));
                                    }
                                }
                            }
                            break;
                        case Command.TaskNotAdded:
                            MessageBox.Show("Не удалось добавить задачу (возможно задача с данным именем уже существует)", 
                                "Не удалось добавить", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            break;
                        case Command.TaskNotEdited:
                            MessageBox.Show("Не удалось изменить задачу (возможно задача с данным именем уже существует)",
                                "Не удалось изменить", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            break;
                        case Command.TaskNotDeleted:
                            MessageBox.Show("Не удалось удалить задачу (возможно задачи с данным именем не существует)",
                              "Не удалось удалить", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            break;
                    }
                }
                else
                    break;
            }
        }

        private void ConnectButton_Click(object sender, EventArgs e)
        {
            IPAddress ip;
            int port;

            if (ConnectButton.Text == "Подключиться")
            {
                ConnectButton.Text = "Подключение";
                Task.Run(() =>
                {
                    try
                    {
                        ip = IPAddress.Parse(IPAddressTextBox.Text);
                        port = int.Parse(PortTextBox.Text);
                    }
                    catch
                    {
                        MessageBox.Show("Некорректный ввод IP или порт.", "Некорректный ввод",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Invoke(new System.Action(() => ConnectButton.Text = "Подключиться"));
                        return;
                    }

                    try
                    {
                        clientController.Connect(ip, port);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Не удалось подключиться к серверу.", "Ошибка подключения",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Invoke(new System.Action(() => ConnectButton.Text = "Подключиться"));
                        return;
                    }

                    Invoke(new System.Action(() =>
                    {
                        ConnectionStatusLabel.Visible = true;
                        TaskPanel.Enabled = true;
                        TaskListPanel.Enabled = true;
                        Invoke(new System.Action(() => ConnectButton.Text = "Отключиться"));
                    }));

                    try
                    {
                        HandleClient();                       
                        clientController.Client.Disconnect(false);
                        clientController = new ClientController();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Ошибка передачи данных. Соединение разорвано.", "Ошибка передачи данных",
                               MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Invoke(new System.Action(() =>
                        {
                            ConnectButton.Text = "Подключиться";
                            CancelTaskButton_Click(this, EventArgs.Empty);
                            tasks.Clear();
                            ConnectionStatusLabel.Visible = false;
                            TaskPanel.Enabled = false;
                            TaskListPanel.Enabled = false;
                        }));
                    }
                });
            }
            else if (ConnectButton.Text == "Отключиться")
            {
                if (clientController.Connected)
                {
                    try
                    {
                        clientController.Client.Disconnect(false);
                    }
                    catch
                    {}
                    clientController = new ClientController();
                }
                ConnectButton.Text = "Подключиться";
                TaskPanel.Enabled = false;
                ConnectionStatusLabel.Visible = false;
                TaskListPanel.Enabled = false;
                CancelTaskButton_Click(this, EventArgs.Empty);
                tasks.Clear();
            }
        }

        private void EditTaskButton_Click(object sender, EventArgs e)
        {
            if (TaskListDataGridView.SelectedRows.Count > 0)
            {
                var selected = TaskListDataGridView.SelectedRows[0].Cells[0].Value.ToString();
                clientController.SendCommand(Command.GetTaskByName);
                clientController.SendString(selected);
            }
        }

        private void SendTaskButton_Click(object sender, EventArgs e)
        {
            if (TaskNameTextBox.Text == "")
            {
                MessageBox.Show("Введите имя задачи!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (currentTask.Triggers.Count == 0)
            {
                MessageBox.Show("Создайте триггеры!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (currentTask.Actions.Count == 0)
            {
                MessageBox.Show("Создайте действия!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (edit)
            {
                clientController.SendCommand(Command.EditTask);
                clientController.SendString(currentTaskName);
                clientController.SendTask(currentTask);
            }
            else
            {
                clientController.SendCommand(Command.AddTask);
                clientController.SendTask(currentTask);
            }
        }

        private void DeleteTaskButton_Click(object sender, EventArgs e)
        {
            if (TaskListDataGridView.SelectedRows.Count > 0)
            {
                var selected = TaskListDataGridView.SelectedRows[0].Cells[0].Value.ToString();
                clientController.SendCommand(Command.DeleteTask);
                clientController.SendString(selected);
            }
        }

        private void CancelTaskButton_Click(object sender, EventArgs e)
        {
            TaskListDataGridView.ClearSelection();
            currentTask = new Model.Task();
            TriggersDataGridView.AutoGenerateColumns = false;
            TriggersDataGridView.DataSource = currentTask.Triggers;
            ActionsDataGridView.AutoGenerateColumns = false;
            ActionsDataGridView.DataSource = currentTask.Actions;
            TaskNameTextBox.Text = currentTask.Name;
            AboutTextBox.Text = currentTask.About;
            edit = false;
            CancelTaskButton.Text = "Очистить";
            SendTaskButton.Text = "Добавить";
        }

        private void TaskNameTextBox_TextChanged(object sender, EventArgs e)
        {
            currentTask.Name = TaskNameTextBox.Text;
        }

        private void AboutTextBox_TextChanged(object sender, EventArgs e)
        {
            currentTask.About = AboutTextBox.Text;
        }

        private void EnableTriggerButton_Click(object sender, EventArgs e)
        {
            if (ActionsDataGridView.SelectedRows.Count > 0)
            {
                var selected = TriggersDataGridView.Rows.IndexOf(TriggersDataGridView.SelectedRows[0]);
                currentTask.Triggers[selected].Enable = !currentTask.Triggers[selected].Enable;
                TriggersDataGridView.Refresh();
                EnableTriggerButton.Text = currentTask.Triggers[selected].Enable ? "Выключить" : "Включить";
            }
        }

        private void EnableActionButton_Click(object sender, EventArgs e)
        {
            if (TriggersDataGridView.SelectedRows.Count > 0)
            {
                var selected = ActionsDataGridView.Rows.IndexOf(ActionsDataGridView.SelectedRows[0]);
                currentTask.Actions[selected].Enable = !currentTask.Actions[selected].Enable;
                ActionsDataGridView.Refresh();
                EnableActionButton.Text = currentTask.Actions[selected].Enable ? "Выключить" : "Включить";
            }
        }

        private void TaskListDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            EditTaskButton.Enabled = TaskListDataGridView.SelectedRows.Count > 0;
            DeleteTaskButton.Enabled = TaskListDataGridView.SelectedRows.Count > 0;
            NewTaskButton.Enabled = TaskListDataGridView.SelectedRows.Count > 0;
            if (TaskListDataGridView.SelectedRows.Count > 0)
            {
                var selected = TaskListDataGridView.Rows.IndexOf(TaskListDataGridView.SelectedRows[0]);
                NewTaskButton.Text = tasks[selected].Enable ? "Выключить" : "Включить";
            }
            else
                NewTaskButton.Text = "Включить";
        }

        private void MainForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                CancelTaskButton_Click(this, EventArgs.Empty);
        }

        private void TaskListDataGridView_DoubleClick(object sender, EventArgs e)
        {
            EditTaskButton_Click(this, EventArgs.Empty);
        }

        private void TriggersDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (TriggersDataGridView.SelectedRows.Count > 0)
            {
                var selected = TriggersDataGridView.Rows.IndexOf(TriggersDataGridView.SelectedRows[0]);
                EnableTriggerButton.Enabled = true;
                DeleteTriggerButton.Enabled = true;
                EditTriggerButton.Enabled = true;
                EnableTriggerButton.Text = currentTask.Triggers[selected].Enable ? "Выключить" : "Включить";
            }
            else
            {
                EnableTriggerButton.Enabled = false;
                DeleteTriggerButton.Enabled = false;
                EditTriggerButton.Enabled = false;
            }
        }

        private void ActionsDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (ActionsDataGridView.SelectedRows.Count > 0)
            {
                var selected = ActionsDataGridView.Rows.IndexOf(ActionsDataGridView.SelectedRows[0]);
                EnableActionButton.Enabled = true;
                DeleteActionButton.Enabled = true;
                EditActionButton.Enabled = true;
                EnableActionButton.Text = currentTask.Actions[selected].Enable ? "Выключить" : "Включить";
            }
            else
            {
                EditActionButton.Enabled = false;
                EnableActionButton.Enabled = false;
                DeleteActionButton.Enabled = false;
            }
        }

        private void TriggersDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (TriggersDataGridView.Columns[e.ColumnIndex].Name == "TriggerType")
            {
                var enumValue = (Model.TriggerType)e.Value;
                switch (enumValue)
                {
                    case Model.TriggerType.Oneshot:
                        e.Value = "Одноразовый";
                        break;
                    case Model.TriggerType.Daily:
                        e.Value = "Дни";
                        break;
                    case Model.TriggerType.Weekly:
                        e.Value = "Недели";
                        break;
                    case Model.TriggerType.Monthly:
                        e.Value = "Месяцы";
                        break;
                }
            } 
            else if (TriggersDataGridView.Columns[e.ColumnIndex].Name == "TriggerEnable")
            {
                var enumValue = (bool)e.Value;
                e.Value = enumValue ? "Да" : "Нет";
            }
        }

        private void ActionsDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (ActionsDataGridView.Columns[e.ColumnIndex].Name == "ActionEnable" ||
                ActionsDataGridView.Columns[e.ColumnIndex].Name == "ActionWait" ||
                ActionsDataGridView.Columns[e.ColumnIndex].Name == "ActionShow")
            {
                var enumValue = (bool)e.Value;
                e.Value = enumValue ? "Да" : "Нет";
            }
        }

        private void DeleteTriggerButton_Click(object sender, EventArgs e)
        {
            if (TriggersDataGridView.SelectedRows.Count > 0)
            {
                var selected = TriggersDataGridView.Rows.IndexOf(TriggersDataGridView.SelectedRows[0]);
                currentTask.Triggers.RemoveAt(selected);
            }
        }

        private void DeleteActionButton_Click(object sender, EventArgs e)
        {
            if (ActionsDataGridView.SelectedRows.Count > 0)
            {
                var selected = ActionsDataGridView.Rows.IndexOf(ActionsDataGridView.SelectedRows[0]);
                currentTask.Actions.RemoveAt(selected);
            }
        }

        private void TaskListDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (TaskListDataGridView.Columns[e.ColumnIndex].Name == "TaskEnable")
            {
                var enumValue = (bool)e.Value;
                e.Value = enumValue ? "Да" : "Нет";
            }
        }

        private void NewTaskButton_Click(object sender, EventArgs e)
        {
            if (TaskListDataGridView.SelectedRows.Count > 0)
            {
                var selected = TaskListDataGridView.Rows.IndexOf(TaskListDataGridView.SelectedRows[0]);
                tasks[selected].Enable = !tasks[selected].Enable;
                TaskListDataGridView.Refresh();
                NewTaskButton.Text = tasks[selected].Enable ? "Выключить" : "Включить";
            }
        }

        private void EditTriggerButton_Click(object sender, EventArgs e)
        {
            if (TriggersDataGridView.SelectedRows.Count > 0)
            {
                var selected = TriggersDataGridView.Rows.IndexOf(TriggersDataGridView.SelectedRows[0]);
                if (selected != -1)
                {
                    using (var form = new TriggerForm(currentTask.Triggers[selected]))
                    {
                        if (form.ShowDialog() == DialogResult.OK)
                        {                  
                            var trigger = form.Trigger;
                            currentTask.Triggers[selected] = trigger;
                        }
                    }
                }
            }
        }

        private void EditActionButton_Click(object sender, EventArgs e)
        {
            if (ActionsDataGridView.SelectedRows.Count > 0)
            {
                var selected = ActionsDataGridView.Rows.IndexOf(ActionsDataGridView.SelectedRows[0]);
                if (selected != -1)
                {
                    using (var form = new ActionForm(currentTask.Actions[selected]))
                    {
                        if (form.ShowDialog() == DialogResult.OK)
                        {               
                            var action = form.Action;
                            currentTask.Actions[selected] = action;
                        }
                    }
                }
            }
        }
    }
}
