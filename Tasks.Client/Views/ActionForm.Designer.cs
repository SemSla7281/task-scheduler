﻿
namespace Tasks.Client
{
    partial class ActionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.PathTextBox = new System.Windows.Forms.TextBox();
            this.ChangeFileButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.BinaryRadioButton = new System.Windows.Forms.RadioButton();
            this.ScriptRadioButton = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.ArgumentsTextBox = new System.Windows.Forms.TextBox();
            this.WaitCheckBox = new System.Windows.Forms.CheckBox();
            this.AddButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.AsUserCheckBox = new System.Windows.Forms.CheckBox();
            this.UserTextBox = new System.Windows.Forms.TextBox();
            this.PasswordTextBox = new System.Windows.Forms.TextBox();
            this.LoginLabel = new System.Windows.Forms.Label();
            this.PasswordLabel = new System.Windows.Forms.Label();
            this.DomainLabel = new System.Windows.Forms.Label();
            this.DomainTextBox = new System.Windows.Forms.TextBox();
            this.ShowWindowsCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Полный путь:";
            // 
            // PathTextBox
            // 
            this.PathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PathTextBox.Location = new System.Drawing.Point(101, 18);
            this.PathTextBox.Name = "PathTextBox";
            this.PathTextBox.Size = new System.Drawing.Size(258, 23);
            this.PathTextBox.TabIndex = 1;
            // 
            // ChangeFileButton
            // 
            this.ChangeFileButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ChangeFileButton.Location = new System.Drawing.Point(358, 17);
            this.ChangeFileButton.Name = "ChangeFileButton";
            this.ChangeFileButton.Size = new System.Drawing.Size(34, 25);
            this.ChangeFileButton.TabIndex = 2;
            this.ChangeFileButton.Text = "...";
            this.ChangeFileButton.UseVisualStyleBackColor = true;
            this.ChangeFileButton.Click += new System.EventHandler(this.ChangeFileButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Тип файла:";
            // 
            // BinaryRadioButton
            // 
            this.BinaryRadioButton.AutoSize = true;
            this.BinaryRadioButton.Checked = true;
            this.BinaryRadioButton.Location = new System.Drawing.Point(101, 47);
            this.BinaryRadioButton.Name = "BinaryRadioButton";
            this.BinaryRadioButton.Size = new System.Drawing.Size(137, 19);
            this.BinaryRadioButton.TabIndex = 4;
            this.BinaryRadioButton.TabStop = true;
            this.BinaryRadioButton.Text = "Исполняемый файл";
            this.BinaryRadioButton.UseVisualStyleBackColor = true;
            // 
            // ScriptRadioButton
            // 
            this.ScriptRadioButton.AutoSize = true;
            this.ScriptRadioButton.Location = new System.Drawing.Point(244, 46);
            this.ScriptRadioButton.Name = "ScriptRadioButton";
            this.ScriptRadioButton.Size = new System.Drawing.Size(64, 19);
            this.ScriptRadioButton.TabIndex = 5;
            this.ScriptRadioButton.Text = "Скрипт";
            this.ScriptRadioButton.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Аргументы:";
            // 
            // ArgumentsTextBox
            // 
            this.ArgumentsTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ArgumentsTextBox.Location = new System.Drawing.Point(101, 71);
            this.ArgumentsTextBox.Multiline = true;
            this.ArgumentsTextBox.Name = "ArgumentsTextBox";
            this.ArgumentsTextBox.Size = new System.Drawing.Size(291, 119);
            this.ArgumentsTextBox.TabIndex = 7;
            // 
            // WaitCheckBox
            // 
            this.WaitCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.WaitCheckBox.AutoSize = true;
            this.WaitCheckBox.Location = new System.Drawing.Point(101, 196);
            this.WaitCheckBox.Name = "WaitCheckBox";
            this.WaitCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.WaitCheckBox.Size = new System.Drawing.Size(147, 19);
            this.WaitCheckBox.TabIndex = 8;
            this.WaitCheckBox.Text = "Ожидать завершения";
            this.WaitCheckBox.UseVisualStyleBackColor = true;
            // 
            // AddButton
            // 
            this.AddButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.AddButton.Location = new System.Drawing.Point(314, 331);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(78, 25);
            this.AddButton.TabIndex = 9;
            this.AddButton.Text = "Добавить";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelButton.Location = new System.Drawing.Point(230, 331);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(78, 25);
            this.CancelButton.TabIndex = 11;
            this.CancelButton.Text = "Отмена";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // AsUserCheckBox
            // 
            this.AsUserCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.AsUserCheckBox.AutoSize = true;
            this.AsUserCheckBox.Location = new System.Drawing.Point(101, 221);
            this.AsUserCheckBox.Name = "AsUserCheckBox";
            this.AsUserCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.AsUserCheckBox.Size = new System.Drawing.Size(222, 19);
            this.AsUserCheckBox.TabIndex = 12;
            this.AsUserCheckBox.Text = "Выполнить от имени пользователя";
            this.AsUserCheckBox.UseVisualStyleBackColor = true;
            this.AsUserCheckBox.CheckedChanged += new System.EventHandler(this.AsUserCheckBox_CheckedChanged);
            // 
            // UserTextBox
            // 
            this.UserTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UserTextBox.Enabled = false;
            this.UserTextBox.Location = new System.Drawing.Point(100, 246);
            this.UserTextBox.Name = "UserTextBox";
            this.UserTextBox.Size = new System.Drawing.Size(291, 23);
            this.UserTextBox.TabIndex = 13;
            // 
            // PasswordTextBox
            // 
            this.PasswordTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PasswordTextBox.Enabled = false;
            this.PasswordTextBox.Location = new System.Drawing.Point(101, 299);
            this.PasswordTextBox.Name = "PasswordTextBox";
            this.PasswordTextBox.Size = new System.Drawing.Size(291, 23);
            this.PasswordTextBox.TabIndex = 14;
            // 
            // LoginLabel
            // 
            this.LoginLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LoginLabel.AutoSize = true;
            this.LoginLabel.Location = new System.Drawing.Point(12, 249);
            this.LoginLabel.Name = "LoginLabel";
            this.LoginLabel.Size = new System.Drawing.Size(44, 15);
            this.LoginLabel.TabIndex = 15;
            this.LoginLabel.Text = "Логин:";
            // 
            // PasswordLabel
            // 
            this.PasswordLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.PasswordLabel.AutoSize = true;
            this.PasswordLabel.Location = new System.Drawing.Point(12, 302);
            this.PasswordLabel.Name = "PasswordLabel";
            this.PasswordLabel.Size = new System.Drawing.Size(52, 15);
            this.PasswordLabel.TabIndex = 16;
            this.PasswordLabel.Text = "Пароль:";
            // 
            // DomainLabel
            // 
            this.DomainLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.DomainLabel.AutoSize = true;
            this.DomainLabel.Location = new System.Drawing.Point(12, 276);
            this.DomainLabel.Name = "DomainLabel";
            this.DomainLabel.Size = new System.Drawing.Size(48, 15);
            this.DomainLabel.TabIndex = 17;
            this.DomainLabel.Text = "Домен:";
            // 
            // DomainTextBox
            // 
            this.DomainTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DomainTextBox.Enabled = false;
            this.DomainTextBox.Location = new System.Drawing.Point(101, 273);
            this.DomainTextBox.Name = "DomainTextBox";
            this.DomainTextBox.Size = new System.Drawing.Size(291, 23);
            this.DomainTextBox.TabIndex = 18;
            // 
            // ShowWindowsCheckBox
            // 
            this.ShowWindowsCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ShowWindowsCheckBox.AutoSize = true;
            this.ShowWindowsCheckBox.Location = new System.Drawing.Point(268, 196);
            this.ShowWindowsCheckBox.Name = "ShowWindowsCheckBox";
            this.ShowWindowsCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ShowWindowsCheckBox.Size = new System.Drawing.Size(123, 19);
            this.ShowWindowsCheckBox.TabIndex = 19;
            this.ShowWindowsCheckBox.Text = "Отображать окна";
            this.ShowWindowsCheckBox.UseVisualStyleBackColor = true;
            // 
            // ActionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 368);
            this.Controls.Add(this.ShowWindowsCheckBox);
            this.Controls.Add(this.DomainTextBox);
            this.Controls.Add(this.DomainLabel);
            this.Controls.Add(this.PasswordLabel);
            this.Controls.Add(this.LoginLabel);
            this.Controls.Add(this.PasswordTextBox);
            this.Controls.Add(this.UserTextBox);
            this.Controls.Add(this.AsUserCheckBox);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.AddButton);
            this.Controls.Add(this.WaitCheckBox);
            this.Controls.Add(this.ArgumentsTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ScriptRadioButton);
            this.Controls.Add(this.BinaryRadioButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ChangeFileButton);
            this.Controls.Add(this.PathTextBox);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(414, 203);
            this.Name = "ActionForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Действие";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox PathTextBox;
        private System.Windows.Forms.Button ChangeFileButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton BinaryRadioButton;
        private System.Windows.Forms.RadioButton ScriptRadioButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox ArgumentsTextBox;
        private System.Windows.Forms.CheckBox WaitCheckBox;
        private System.Windows.Forms.Button AddButton;
        private new System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.CheckBox AsUserCheckBox;
        private System.Windows.Forms.TextBox UserTextBox;
        private System.Windows.Forms.TextBox PasswordTextBox;
        private System.Windows.Forms.Label LoginLabel;
        private System.Windows.Forms.Label PasswordLabel;
        private System.Windows.Forms.Label DomainLabel;
        private System.Windows.Forms.TextBox DomainTextBox;
        private System.Windows.Forms.CheckBox ShowWindowsCheckBox;
    }
}