#include "Data.h"

#include <fstream>

int StringLength(wchar_t* string)
{
    wchar_t* current = string;
    while (*current != L'\0')
        current++;
    return (int)(current - string);
}

void WriteBool(std::ofstream& file, bool flag)
{
    char data[sizeof(bool)];
    memcpy(data, &flag, sizeof(bool));
    file.write(data, sizeof(bool));
}

bool ReadBool(std::ifstream& file)
{
    char data[sizeof(bool)];
    file.read(data, sizeof(bool));
    bool flag;
    memcpy(&flag, data, sizeof(bool));
    return flag;
}

void WriteInt(std::ofstream& file, int number)
{
    char data[sizeof(int)];
    memcpy(data, &number, sizeof(int));
    file.write(data, sizeof(int));
}

int ReadInt(std::ifstream& file)
{
    char data[sizeof(int)];
    file.read(data, sizeof(int));
    int number;
    memcpy(&number, data, sizeof(int));
    return number;
}

void WriteString(std::ofstream& file, wchar_t* string)
{
    int len = StringLength(string);
    WriteInt(file, len);
    file.write((char*)string, len * sizeof(wchar_t));
}

wchar_t* ReadString(std::ifstream& file)
{
    int len = ReadInt(file);
    wchar_t* string = new wchar_t[(size_t)len + 1];
    file.read((char*)string, len * sizeof(wchar_t));
    string[len] = L'\0';
    return string;
}