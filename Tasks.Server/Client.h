#pragma once

#include "DateTime.h"

#include <winsock2.h>

enum class Command : int
{
	GetTaskList = 0,
	AddTask = 1,
	EditTask = 2,
	DeleteTask = 3,
	TakeTaskList = 4,
	GetTaskByName = 5,
	TakeTask = 6,
	TaskAdded = 7,
	TaskEdited = 8,
	TaskDeleted = 9,
	TaskNotAdded = 10,
	TaskNotEdited = 11,
	TaskNotDeleted = 12,
	Disconnect = 13
};

Command AcceptCommand(SOCKET socket);
void SendCommand(SOCKET socket, Command command);
bool AcceptBool(SOCKET socket);
void SendBool(SOCKET socket, bool number);
int AcceptInt(SOCKET socket);
void SendInt(SOCKET socket, int number);
void SendDateTime(SOCKET socket, DateTime dateTime);
DateTime AcceptDateTime(SOCKET socket);
wchar_t* AcceptString(SOCKET socket);
void SendString(SOCKET socket, wchar_t* string);