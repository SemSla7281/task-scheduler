#define WIN32_LEAN_AND_MEAN

#include "Client.h"
#include "Log.h"
#include "Task.h"
#include "Server.h"
#include "Parameters.h"
#include "TaskExecutor.h"

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")
#pragma comment (lib, "Ws2_32.lib")

#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "1366"

int main(int argc, char** argv)
{
    WSADATA wsaData;
    SOCKET client = INVALID_SOCKET;
    struct addrinfo* result = NULL, * ptr = NULL, hints;
    int iResult;
    int recvbuflen = DEFAULT_BUFLEN;
    string address = "127.0.0.1";

    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != 0)
    {
        printf("WSAStartup failed with error: %d\n", iResult);
        return 1;
    }

    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    iResult = getaddrinfo(address.c_str(), DEFAULT_PORT, &hints, &result);
    if (iResult != 0)
    {
        printf("getaddrinfo failed with error: %d\n", iResult);
        WSACleanup();
        return 1;
    }

    for (ptr = result; ptr != NULL; ptr = ptr->ai_next)
    {
        client = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
        if (client == INVALID_SOCKET)
        {
            printf("socket failed with error: %ld\n", WSAGetLastError());
            WSACleanup();
            return 1;
        }

        iResult = connect(client, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (iResult == SOCKET_ERROR)
        {
            closesocket(client);
            client = INVALID_SOCKET;
            continue;
        }
        break;
    }

    freeaddrinfo(result);

    if (client == INVALID_SOCKET)
    {
        printf("Unable to connect to server!\n");
        WSACleanup();
        return 1;
    }

    printf("Add task test start...\n");
    DateTime dateTime;
    dateTime.day = 14;
    dateTime.hour = 22;
    dateTime.minute = 12;
    dateTime.year = 2013;
    dateTime.month = 6;
    dateTime.second = 0;

    Trigger trigger;
    trigger.comlite = false;
    trigger.date = dateTime;
    trigger.enable = true;
    trigger.frequancy = 1;
    trigger.type = TriggerType::Oneshot;

    Action action;
    action.type = ActionType::Binary;
    action.enable = true;
    action.path = (wchar_t*)L"C:\\Windows\\System32\\calc.exe";
    action.arguments = (wchar_t*)L"";
    action.show = true;
    action.user = (wchar_t*)L"";
    action.password = (wchar_t*)L"";
    action.domain = (wchar_t*)L"";

    Task task;
    task.name = (wchar_t*)L"TestTask";
    task.about = (wchar_t*)L"TestTask About";
    task.enable = true;
    task.triggers.push_back(trigger);
    task.actions.push_back(action);

    auto command = AcceptCommand(client);
    if (command == Command::TakeTaskList)
    {
        int size = AcceptInt(client);
        if (size != 0)
        {
            printf("Server request error (task.size)!\n");
            WSACleanup();
            return 1;
        }
    }

    SendCommand(client, Command::AddTask);
    SendTask(client, task);
    printf("Add task test complite\n");
    command = AcceptCommand(client);
    if (command != Command::TaskAdded)
    {
        printf("Server request error!\n");
        WSACleanup();
        return 1;
    }

    printf("Get task list start...\n");
    command = AcceptCommand(client);
    if (command == Command::TakeTaskList)
    {
        int size = AcceptInt(client);
        if (size == 1)
        {
            auto reTask = AcceptTask(client);
            if (reTask.enable != task.enable)
            {
                printf("Server request error (task.enable)!\n");
                WSACleanup();
                return 1;
            }
            if (reTask.triggers.size() != task.triggers.size())
            {
                printf("Server request error (task.triggers.size)!\n");
                WSACleanup();
                return 1;
            }
            else
            {
                for (int i = 0; i < reTask.triggers.size(); i++)
                {
                    if (reTask.triggers[i].enable != task.triggers[i].enable)
                    {
                        printf("Server request error (task.trigger.enable)!\n");
                        WSACleanup();
                        return 1;
                    }
                    if (reTask.triggers[i].comlite != task.triggers[i].comlite)
                    {
                        printf("Server request error (task.trigger.comlite)!\n");
                        WSACleanup();
                        return 1;
                    }
                    if (reTask.triggers[i].frequancy != task.triggers[i].frequancy)
                    {
                        printf("Server request error (task.trigger.frequancy)!\n");
                        WSACleanup();
                        return 1;
                    }
                }
            }
            if (reTask.actions.size() != task.actions.size())
            {
                printf("Server request error (task.actions.size)!\n");
                WSACleanup();
                return 1;
            }
            else
            {
                for (int i = 0; i < reTask.actions.size(); i++)
                {
                    if (reTask.actions[i].enable != task.actions[i].enable)
                    {
                        printf("Server request error (task.action.enable)!\n");
                        WSACleanup();
                        return 1;
                    }
                    if (reTask.actions[i].show != task.actions[i].show)
                    {
                        printf("Server request error (task.action.show)!\n");
                        WSACleanup();
                        return 1;
                    }
                    if (reTask.actions[i].type != task.actions[i].type)
                    {
                        printf("Server request error (task.action.type)!\n");
                        WSACleanup();
                        return 1;
                    }
                }
            }
            if (wcscmp(reTask.name, task.name) != 0)
            {
                printf("Server request error (task.name)!\n");
                WSACleanup();
                return 1;
            }
            if (wcscmp(reTask.about, task.about) != 0)
            {
                printf("Server request error (task.about)!\n");
                WSACleanup();
                return 1;
            }
        }
        else
        {
            printf("Server request error!\n");
            WSACleanup();
            return 1;
        }
    }
    else
    {
        printf("Server request error!\n");
        WSACleanup();
        return 1;
    }
    printf("Get task list complite\n");

    task.about = (wchar_t*)L"New about";

    printf("Edit task test start...\n");
    SendCommand(client, Command::EditTask);
    SendString(client, task.name);
    SendTask(client, task);

    command = AcceptCommand(client);
    if (command != Command::TaskEdited)
    {
        printf("Server request error!\n");
        WSACleanup();
        return 1;
    }
    command = AcceptCommand(client);
    if (command == Command::TakeTaskList)
    {
        int size = AcceptInt(client);
        if (size == 1)
        {
            auto reTask = AcceptTask(client);
            if (reTask.enable != task.enable)
            {
                printf("Server request error (task.enable)!\n");
                WSACleanup();
                return 1;
            }
            if (reTask.triggers.size() != task.triggers.size())
            {
                printf("Server request error (task.triggers.size)!\n");
                WSACleanup();
                return 1;
            }
            else
            {
                for (int i = 0; i < reTask.triggers.size(); i++)
                {
                    if (reTask.triggers[i].enable != task.triggers[i].enable)
                    {
                        printf("Server request error (task.trigger.enable)!\n");
                        WSACleanup();
                        return 1;
                    }
                    if (reTask.triggers[i].comlite != task.triggers[i].comlite)
                    {
                        printf("Server request error (task.trigger.comlite)!\n");
                        WSACleanup();
                        return 1;
                    }
                    if (reTask.triggers[i].frequancy != task.triggers[i].frequancy)
                    {
                        printf("Server request error (task.trigger.frequancy)!\n");
                        WSACleanup();
                        return 1;
                    }
                }
            }
            if (reTask.actions.size() != task.actions.size())
            {
                printf("Server request error (task.actions.size)!\n");
                WSACleanup();
                return 1;
            }
            else
            {
                for (int i = 0; i < reTask.actions.size(); i++)
                {
                    if (reTask.actions[i].enable != task.actions[i].enable)
                    {
                        printf("Server request error (task.action.enable)!\n");
                        WSACleanup();
                        return 1;
                    }
                    if (reTask.actions[i].show != task.actions[i].show)
                    {
                        printf("Server request error (task.action.show)!\n");
                        WSACleanup();
                        return 1;
                    }
                    if (reTask.actions[i].type != task.actions[i].type)
                    {
                        printf("Server request error (task.action.type)!\n");
                        WSACleanup();
                        return 1;
                    }
                }
            }
            if (wcscmp(reTask.name, task.name) != 0)
            {
                printf("Server request error (task.name)!\n");
                WSACleanup();
                return 1;
            }
            if (wcscmp(reTask.about, task.about) != 0)
            {
                printf("Server request error (task.about)!\n");
                WSACleanup();
                return 1;
            }
        }
        else
        {
            printf("Server request error!\n");
            WSACleanup();
            return 1;
        }
    }
    else
    {
        printf("Server request error!\n");
        WSACleanup();
        return 1;
    }
    printf("Edit task test complite\n");

    printf("Delete task test start ...\n");
    SendCommand(client, Command::DeleteTask);
    SendString(client, task.name);

    command = AcceptCommand(client);
    if (command != Command::TaskDeleted)
    {
        printf("Server request error!\n");
        WSACleanup();
        return 1;
    }
    auto name = AcceptString(client);

    command = AcceptCommand(client);
    if (command == Command::TakeTaskList)
    {
        int size = AcceptInt(client);
        if (size != 0)
        {
            printf("Server request error!\n");
            WSACleanup();
            return 1;
        }
    }

    printf("Delete task test complite\n");

    closesocket(client);
    WSACleanup();

    return 0;
}
