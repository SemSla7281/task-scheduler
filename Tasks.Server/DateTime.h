#pragma once

#include <cstdint>
#include <time.h>
#include <ctime>

/// <summary>
/// ���� � �����
/// </summary>
typedef struct
{
    /// <summary>
    /// ����
    /// </summary>
    int day;

    /// <summary>
    /// �����
    /// </summary>
    int month;

    /// <summary>
    /// ���
    /// </summary>
    int year;

    /// <summary>
    /// ���
    /// </summary>
    int hour;

    /// <summary>
    /// ������
    /// </summary>
    int minute;

    /// <summary>
    /// �������
    /// </summary>
    int second;
} DateTime;

/// <summary>
/// �������� ���� � �����
/// </summary>
/// <param name="left">������ ��������� ���� � �������</param>
/// <param name="right">������ ��������� ���� � �������</param>
/// <returns>��������� ���������</returns>
bool Compare(DateTime left, DateTime right);

/// <summary>
/// �������� ����
/// </summary>
/// <param name="left">������ ��������� ���� � �������</param>
/// <param name="right">������ ��������� ���� � �������</param>
/// <returns>��������� ���������</returns>
bool CompareDate(DateTime left, DateTime right);

/// <summary>
/// �������� �����
/// </summary>
/// <param name="left">������ ��������� ���� � �������</param>
/// <param name="right">������ ��������� ���� � �������</param>
/// <returns>��������� ���������</returns>
bool CompareTime(DateTime left, DateTime right);

/// <summary>
/// �������� ������� ���� � �����
/// </summary>
/// <returns>���� � �����</returns>
DateTime Now();

/// <summary>
/// �������� ������� ���� ������
/// </summary>
/// <returns>���� ������</returns>
int WeekDay();
