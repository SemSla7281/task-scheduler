#include "TaskExecutor.h"
#include "Log.h"
#include "Parameters.h"

BOOL ExecuteAction(const Action& action)
{
    Log(L"[Function] ExecuteAction");
    STARTUPINFOW startupInfo;
    PROCESS_INFORMATION processInfo;
    ZeroMemory(&startupInfo, sizeof(startupInfo));
    startupInfo.cb = sizeof(startupInfo);
    ZeroMemory(&processInfo, sizeof(processInfo));
    if (!action.show)
        startupInfo.wShowWindow = SW_HIDE;

    if (action.asUser)
    {
        HANDLE token = NULL;
        if (LogonUser(action.user, action.domain, action.password, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, &token))
        {
            Log(L"[Invoke] CreateProcessAsUser \"" + std::wstring(action.path) + L"\"");
            if (CreateProcessAsUser(token, (LPCWSTR)action.path, (LPWSTR)action.arguments, NULL, NULL, FALSE,
                action.show ? CREATE_NEW_CONSOLE : CREATE_NO_WINDOW, NULL, NULL, &startupInfo, &processInfo))
            {
                Log(L"[Result] Process created");
                if (action.wait)
                {
                    Log(L"[Info] Wait for finish process");
                    WaitForSingleObject(processInfo.hProcess, INFINITE);
                    Log(L"[Info] Wait process complite");
                }
                CloseHandle(processInfo.hProcess);
                CloseHandle(processInfo.hThread);
                Log(L"[Info] Action complite");
                return TRUE;
            }
        }
        else
        {
            DWORD i = GetLastError();
            return FALSE;
        }
    }
    else
    {
        Log(L"[Invoke] CreateProcess \"" + std::wstring(action.path) + L"\"");
        if (CreateProcess((LPCWSTR)action.path, (LPWSTR)action.arguments, NULL, NULL, FALSE, 
            action.show ? CREATE_NEW_CONSOLE : CREATE_NO_WINDOW, NULL, NULL, &startupInfo, &processInfo))
        {
            Log(L"[Result] Process created");
            if (action.wait)
            {
                Log(L"[Info] Wait for finish process");
                WaitForSingleObject(processInfo.hProcess, INFINITE);
                Log(L"[Info] Wait process complite");
            }
            CloseHandle(processInfo.hProcess);
            CloseHandle(processInfo.hThread);
            Log(L"[Info] Action complite");
            return TRUE;
        }
    }
    int error = GetLastError();
    Log(L"[Error] Create process error: " + std::to_wstring(error));
    return FALSE;
}

BOOL ExecuteTask(Task task, Trigger trigger)
{
    BOOL complite = true;
    Log(L"[Function] ExecuteTask");
    for (const Action& action : task.actions)
        if (action.enable)
            complite = ExecuteAction(action) && complite;
    if (complite)
        Log(L"[Info] Task complite successfully");
    else
        Log(L"[Info] Task complite with errors");
    return TRUE;
}

DWORD WINAPI TaskExecutorThread(LPVOID argument)
{
    TETArgument tetArgument = *((TETArgument*)argument);
    HANDLE serviceStopEvent = tetArgument.serviceStopEvent;
    std::vector<Task>& tasks = *tetArgument.tasks;

    Log(L"[Function] TasksWorkerThread");
    while (WaitForSingleObject(serviceStopEvent, 0) != WAIT_OBJECT_0)
    {
        for (Task& task : tasks)
        {
            if (task.enable)
            {
                for (Trigger& trigger : task.triggers)
                {
                    if (trigger.enable)
                    {
                        switch (trigger.type)
                        {
                        case Oneshot:
                        {
                            DateTime triggerDate = trigger.date;
                            DateTime triggerTime = trigger.time;
                            DateTime nowTime = Now();
                            if (CompareDate(nowTime, triggerDate) && CompareTime(nowTime, triggerTime))
                                if (!trigger.comlite)
                                    if (ExecuteTask(task, trigger))
                                        trigger.comlite = TRUE;
                        }
                        break;

                        case Daily:
                        {
                            DateTime triggerTime = trigger.time;
                            DateTime nowTime = Now();
                            if (trigger.prewDay > nowTime.day)
                                nowTime.day = 31 + nowTime.day;
                            if ((nowTime.day - trigger.prewDay - 1) == trigger.frequancy)
                            {
                                if (CompareTime(nowTime, triggerTime))
                                {
                                    if (!trigger.comlite)
                                    {
                                        trigger.prewDay = nowTime.day;
                                        if (ExecuteTask(task, trigger))
                                            trigger.comlite = TRUE;
                                    }
                                }
                                else
                                {
                                    if (trigger.comlite)
                                        trigger.comlite = FALSE;
                                }
                            }
                        }
                        break;

                        case Weekly:
                        {
                            DateTime triggerTime = trigger.time;
                            DateTime nowTime = Now();
                            int nowWeekDay = WeekDay();
                            if (trigger.weekDays[nowWeekDay])
                            {
                                if (CompareTime(nowTime, triggerTime))
                                {
                                    if (!trigger.comlite)
                                    {
                                        if (ExecuteTask(task, trigger))
                                            trigger.comlite = TRUE;
                                    }
                                }
                                else
                                {
                                    if (trigger.comlite)
                                        trigger.comlite = FALSE;
                                }
                            }
                        }
                        break;

                        case Monthly:
                        {
                            DateTime triggerTime = trigger.time;
                            DateTime nowTime = Now();
                            int nowWeekDay = WeekDay();
                            int nowMonth = nowTime.month;
                            int nowMonthDay = nowTime.day;
                            if (trigger.months[nowMonth] &&
                                (trigger.monthType == TriggerMonthType::MonthDays
                                    ? trigger.monthDays[nowMonthDay] : trigger.weekMonthlyDays[nowWeekDay]))
                            {
                                if (CompareTime(nowTime, triggerTime))
                                {
                                    if (!trigger.comlite)
                                    {
                                        if (ExecuteTask(task, trigger))
                                            trigger.comlite = TRUE;
                                    }
                                }
                                else
                                {
                                    if (trigger.comlite)
                                        trigger.comlite = FALSE;
                                }
                            }
                        }
                        break;
                        }
                    }
                }
            }
        }
        Sleep(100);
    }
    return 0;
}