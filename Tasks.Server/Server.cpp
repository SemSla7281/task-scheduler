#include "Server.h"
#include "Task.h"
#include "Parameters.h"

#include <ws2def.h>
#include <ws2tcpip.h>
#include <WinSock2.h>

DWORD WINAPI ClientThread(LPVOID argument)
{
    bool disconnect = false;
    CTArgument& ctArgument = *((CTArgument*)argument);
    SOCKET clientSocket = ctArgument.clientSocket;
    HANDLE serviceStopEvent = ctArgument.serviceStopEvent;
    std::vector<Task>& tasks = *ctArgument.tasks;
    delete (CTArgument*)argument;

    Log(L"[Info] Handle client thread started");

    SendCommand(clientSocket, Command::TakeTaskList);
    SendInt(clientSocket, (int)tasks.size());
    for (size_t i = 0; i < tasks.size(); i++)
        SendTask(clientSocket, tasks[i]);

    while (WaitForSingleObject(serviceStopEvent, 0) != WAIT_OBJECT_0 && !disconnect)
    {
        auto command = (Command)AcceptCommand(clientSocket);
        switch (command)
        {
        case Command::Disconnect:
            disconnect = true;
            break;
        case Command::GetTaskByName:
        {
            auto name = AcceptString(clientSocket);
            for (size_t i = 0; i < tasks.size(); i++)
            {
                if (std::wstring(tasks[i].name) == std::wstring(name))
                {
                    SendCommand(clientSocket, Command::TakeTask);
                    SendTask(clientSocket, tasks[i]);
                    break;
                }
            }
        }
        break;
        case Command::AddTask:
        {
            auto task = AcceptTask(clientSocket);
            bool found = false;
            for (size_t i = 0; i < tasks.size(); i++)
            {
                if (std::wstring(tasks[i].name) == std::wstring(task.name))
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                SendCommand(clientSocket, Command::TaskAdded);
                tasks.push_back(task);
                SendCommand(clientSocket, Command::TakeTaskList);
                SendInt(clientSocket, (int)tasks.size());
                for (size_t i = 0; i < tasks.size(); i++)
                    SendTask(clientSocket, tasks[i]);
                SaveTasks(L"C:/ProgramData/TasksCreator/Data.bin", tasks);
            }
            else
                SendCommand(clientSocket, Command::TaskNotAdded);
        }
        break;
        case Command::EditTask:
        {
            auto name = AcceptString(clientSocket);
            auto task = AcceptTask(clientSocket);
            int found = -1;
            for (size_t i = 0; i < tasks.size(); i++)
            {
                if (std::wstring(tasks[i].name) == std::wstring(name))
                {
                    found = (int)i;
                    break;
                }
            }
            if (found == -1)
                SendCommand(clientSocket, Command::TaskNotEdited);
            else
            {
                SendCommand(clientSocket, Command::TaskEdited);
                tasks[found] = task;
                SendCommand(clientSocket, Command::TakeTaskList);
                SendInt(clientSocket, (int)tasks.size());
                for (size_t i = 0; i < tasks.size(); i++)
                    SendTask(clientSocket, tasks[i]);
                SaveTasks(L"C:/ProgramData/TasksCreator/Data.bin", tasks);
            }
        }
        break;
        case Command::DeleteTask:
        {
            auto name = AcceptString(clientSocket);
            int found = -1;
            for (size_t i = 0; i < tasks.size(); i++)
            {
                if (std::wstring(tasks[i].name) == std::wstring(name))
                {
                    found = (int)i;
                    break;
                }
            }
            if (found == -1)
                SendCommand(clientSocket, Command::TaskNotDeleted);
            else
            {
                SendCommand(clientSocket, Command::TaskDeleted);
                SendString(clientSocket, tasks[found].name);
                tasks.erase(tasks.begin() + found);
                SendCommand(clientSocket, Command::TakeTaskList);
                SendInt(clientSocket, (int)tasks.size());
                for (size_t i = 0; i < tasks.size(); i++)
                    SendTask(clientSocket, tasks[i]);
                SaveTasks(L"C:/ProgramData/TasksCreator/Data.bin", tasks);
            }
        }
        break;
        }
    }
    Log(L"[Info] Handle client thread finished");
    return ERROR_SUCCESS;
}

DWORD WINAPI AcceptThread(LPVOID argument)
{
    ATArgument &atParameter = *((ATArgument*)argument);
    SOCKET listenSocket = atParameter.listenSocket;
    HANDLE serviceStopEvent = atParameter.serviceStopEvent;
    std::vector<HANDLE> threads;
    SOCKET clientSocket;
    DWORD threadId;
    CTArgument *ctArgument;

    while (WaitForSingleObject(serviceStopEvent, 0) != WAIT_OBJECT_0)
    {
        Log(L"[Info] Accepting client...");
        clientSocket = accept(listenSocket, NULL, NULL);
        if (clientSocket == INVALID_SOCKET)
        {
            Log(L"[Error] Server accept failed with error: " + std::to_wstring(WSAGetLastError()));
            closesocket(listenSocket);
            WSACleanup();
            return ERROR;
        }
        Log(L"[Info] Client accepted");

        // ������ ������ �������
        ctArgument = new CTArgument;
        ctArgument->serviceStopEvent = atParameter.serviceStopEvent;
        ctArgument->tasks = atParameter.tasks;
        ctArgument->clientSocket = clientSocket;
        threads.push_back(CreateThread(NULL, 0, ClientThread, ctArgument, 0, &threadId));
    }
    return ERROR_SUCCESS;
}

DWORD WINAPI ServerThread(LPVOID argument)
{
    Log(L"[Function] ServiceWorkerThread");

    STArgument& stArgument = *((STArgument*)argument);
    HANDLE serviceStopEvent = stArgument.serviceStopEvent;
    HANDLE acceptThread;
    SOCKET listenSocket = INVALID_SOCKET;
    SOCKET clientSocket = INVALID_SOCKET;
    std::vector<Task>& tasks = *stArgument.tasks;
    DWORD dwThreadIdArray;
    WSADATA wsaData;
    string port = "1366";
    struct addrinfo* addrinfoResult = NULL;
    struct addrinfo hints;
    int result;

    ifstream portFile("C:/ProgramData/TasksCreator/Port.txt");
    if (!portFile.fail())
    {
        portFile >> port;
        portFile.close();
    }

    Log(L"[Invoke] WSAStartup");
    result = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (result != 0)
    {
        Log(L"[Error] WSAStartup failed with error: " + std::to_wstring(result));
        return ERROR;
    }

    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE;

    Log(L"[Invoke] Getaddrinfo");
    result = getaddrinfo(NULL, port.c_str(), &hints, &addrinfoResult);
    if (result != 0)
    {
        Log(L"[Error] Server getaddrinfo failed with error: " + std::to_wstring(result));
        WSACleanup();
        return ERROR;
    }

    Log(L"[Invoke] Socket");
    listenSocket = socket(addrinfoResult->ai_family, addrinfoResult->ai_socktype, addrinfoResult->ai_protocol);
    if (listenSocket == INVALID_SOCKET)
    {
        Log(L"[Error] Server socket failed with error: " + std::to_wstring(WSAGetLastError()));
        freeaddrinfo(addrinfoResult);
        WSACleanup();
        return ERROR;
    }

    Log(L"[Invoke] Bind");
    result = bind(listenSocket, addrinfoResult->ai_addr, (int)addrinfoResult->ai_addrlen);
    if (result == SOCKET_ERROR)
    {
        Log(L"[Error] Server bind failed with error: %d\n" + std::to_wstring(WSAGetLastError()));
        freeaddrinfo(addrinfoResult);
        closesocket(listenSocket);
        WSACleanup();
        return 1;
    }

    freeaddrinfo(addrinfoResult);

    Log(L"[Invoke] Listen");
    result = listen(listenSocket, SOMAXCONN);
    if (result == SOCKET_ERROR)
    {
        Log(L"[Error] Server listen failed with error: " + std::to_wstring(WSAGetLastError()));
        closesocket(listenSocket);
        WSACleanup();
        return ERROR;
    }

    ATArgument atArgument;
    atArgument.tasks = stArgument.tasks;
    atArgument.listenSocket = listenSocket;
    atArgument.serviceStopEvent = stArgument.serviceStopEvent;

    acceptThread = CreateThread(NULL, 0, AcceptThread, &atArgument, 0, &dwThreadIdArray);
    while (WaitForSingleObject(serviceStopEvent, 0) != WAIT_OBJECT_0)
        Sleep(1000);

    Log(L"[Info] Close listen socket");
    closesocket(listenSocket);

    Log(L"[Invoke] Shutdown");
    result = shutdown(clientSocket, SD_SEND);
    if (result == SOCKET_ERROR)
    {
        Log(L"[Error] Server shutdown failed with error: " + std::to_wstring(WSAGetLastError()));
        closesocket(clientSocket);
        WSACleanup();
        return 1;
    }

    Log(L"[Invoke] CloseSocket");
    closesocket(clientSocket);

    Log(L"[Invoke] WSACleanup");
    WSACleanup();

    return ERROR_SUCCESS;
}