#pragma once

#include <vector>
#include <fstream>

/// <summary>
/// �������� ����� ������
/// </summary>
/// <param name="string">������</param>
/// <returns>����� ������</returns>
int StringLength(wchar_t* string);

/// <summary>
/// �������� �������� ���������� ���� bool � ���� �������
/// </summary>
/// <param name="file">����</param>
/// <param name="flag">Bool ����������</param>
void WriteBool(std::ofstream& file, bool flag);

/// <summary>
/// ��������� �������� ���������� ���� bool �� ����� �������
/// </summary>
/// <param name="file">����</param>
/// <returns>Bool ��������</returns>
bool ReadBool(std::ifstream& file);

/// <summary>
/// �������� �������� ���������� ���� int � ���� �������
/// </summary>
/// <param name="file">����</param>
/// <param name="number">Int ����������</param>
void WriteInt(std::ofstream& file, int number);

/// <summary>
/// ��������� �������� ���������� ���� int �� ����� �������
/// </summary>
/// <param name="file">����</param>
/// <returns>Int ��������</returns>
int ReadInt(std::ifstream& file);

/// <summary>
/// �������� ������ � ���� �������
/// </summary>
/// <param name="file">����</param>
/// <param name="string">������</param>
void WriteString(std::ofstream& file, wchar_t* string);

/// <summary>
/// ��������� ������ �� ����� �������
/// </summary>
/// <param name="file">����</param>
/// <returns>������</returns>
wchar_t* ReadString(std::ifstream& file);