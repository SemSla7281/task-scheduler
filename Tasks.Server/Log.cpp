#include "Log.h"

#include <iostream>
#include <fstream>
#include <time.h>

#define DTTMFMT "%d.%m.%Y %H:%M:%S"
#define DTTMSZ 21

std::wofstream logFile;

static char* DateTimeFormatted(char* buff) 
{
    time_t t = time(0);
    tm timeDem;
    localtime_s(&timeDem, &t);
    strftime(buff, DTTMSZ, DTTMFMT, &timeDem);
    return buff;
}

void LogInit()
{
    logFile.open("C:\\ProgramData\\TasksCreator\\Log.txt", std::ios::out | std::ios::app);
    char timeFormatted[32];
    DateTimeFormatted(timeFormatted);
    logFile << "\n[Launch] " << timeFormatted << '\n';
}

void Log(std::wstring data)
{
    logFile << data << '\n';
    std::wcout << data << '\n';
    logFile.flush();
}