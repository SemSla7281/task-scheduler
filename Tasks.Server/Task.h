#pragma once

#include "Action.h"
#include "Trigger.h"

#include <vector>
#include <fstream>

using namespace std;

/// <summary>
/// ������
/// </summary>
typedef struct
{
    /// <summary>
    /// �������� ������
    /// </summary>
    std::vector<Trigger> triggers;

    /// <summary>
    /// �������� ������
    /// </summary>
    std::vector<Action> actions;

    /// <summary>
    /// �������� �� ������
    /// </summary>
    int enable;

    /// <summary>
    /// �������� ������
    /// </summary>
    wchar_t* name;

    /// <summary>
    /// �������� ������
    /// </summary>
    wchar_t* about;
} Task;

using Tasks = std::vector<Task>;

/// <summary>
/// �������� ������ � ���� �������
/// </summary>
/// <param name="file">����</param>
/// <param name="task">������</param>
void WriteTask(std::ofstream& file, Task task);  

/// <summary>
/// ��������� ������ �� ����� �������
/// </summary>
/// <param name="file">����</param>
/// <returns>������</returns>
Task ReadTask(std::ifstream& file);

/// <summary>
/// ��������� ������ ����� � ����
/// </summary>
/// <param name="path">���� � �����</param>
/// <param name="tasks">������ �����</param>
/// <returns>������� �� ���������</returns>
BOOL SaveTasks(const wchar_t* path, const std::vector<Task>& tasks);

/// <summary>
/// ��������� ������ ����� �� �����
/// </summary>
/// <param name="path">���� � �����</param>
/// <param name="tasks">������ �����</param>
/// <returns>������� �� ���������</returns>
BOOL LoadTasks(const wchar_t* path, std::vector<Task>& tasks);

/// <summary>
/// �������� ������ �� ������� �� ������
/// </summary>
/// <param name="socket">�����</param>
/// <returns>������</returns>
Task AcceptTask(SOCKET socket);

/// <summary>
/// ��������� ������ ������� �� ������
/// </summary>
/// <param name="socket">�����</param>
/// <param name="task">������</param>
void SendTask(SOCKET socket, Task task);