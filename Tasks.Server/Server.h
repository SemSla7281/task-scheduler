#pragma once

#include "Client.h"
#include "Log.h"

#include <windef.h>

/// <summary>
/// ������� ������ ��������� �������
/// </summary>
/// <param name="argument">�������� ���������� ������</param>
/// <returns>��������� ���������� ������</returns>
DWORD WINAPI ClientThread(LPVOID argument);

/// <summary>
/// ������� ������ ����������� ��������
/// </summary>
/// <param name="argument">�������� ���������� ������</param>
/// <returns>��������� ���������� ������</returns>
DWORD WINAPI AcceptThread(LPVOID argument);

/// <summary>
/// ������� ������ �������
/// </summary>
/// <param name="argument">�������� ���������� ������</param>
/// <returns>��������� ���������� ������</returns>
DWORD WINAPI ServerThread(LPVOID argument);