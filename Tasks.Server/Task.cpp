#include "Task.h"
#include "Client.h"

#include <Windows.h>
#include "Log.h"

void WriteTask(std::ofstream& file, Task task)
{
    WriteBool(file, task.enable);
    WriteString(file, task.name);
    WriteString(file, task.about);
    WriteInt(file, (int)task.triggers.size());
    for (size_t i = 0; i < task.triggers.size(); i++)
        WriteTrigger(file, task.triggers[i]);
    WriteInt(file, (int)task.actions.size());
    for (size_t i = 0; i < task.actions.size(); i++)
        WriteAction(file, task.actions[i]);
}

Task ReadTask(std::ifstream& file)
{
    Task task;
    task.enable = ReadBool(file);
    task.name = ReadString(file);
    task.about = ReadString(file);
    int triggersSize = ReadInt(file);
    for (int i = 0; i < triggersSize; i++)
        task.triggers.push_back(ReadTrigger(file));
    int actionsSize = ReadInt(file);
    for (int i = 0; i < actionsSize; i++)
        task.actions.push_back(ReadAction(file));
    return task;
}

BOOL SaveTasks(const wchar_t* path, const std::vector<Task>& tasks)
{
    ofstream database(path);
    if (!database.fail())
    {
        WriteInt(database, (int)tasks.size());
        for (size_t i = 0; i < tasks.size(); i++)
            WriteTask(database, tasks[i]);
        database.close();
        return TRUE;
    }
    return FALSE;
}

BOOL LoadTasks(const wchar_t* path, std::vector<Task>& tasks)
{
    Log(L"[Function] LoadTasks");
    ifstream database(path);
    if (!database.fail())
    {
        tasks.clear();
        int size = ReadInt(database);
        for (size_t i = 0; i < size; i++)
        {
            Task task = ReadTask(database);
            tasks.push_back(task);
        }
        database.close();
        Log(L"\n[Info] Tasks loaded");
        return TRUE;
    }
    return FALSE;
}

Task AcceptTask(SOCKET socket)
{
    Task task;
    task.enable = AcceptBool(socket);
    task.name = AcceptString(socket);
    task.about = AcceptString(socket);
    int triggerSize = AcceptInt(socket);
    for (int i = 0; i < triggerSize; i++)
        task.triggers.push_back(AcceptTrigger(socket));
    int actionSize = AcceptInt(socket);
    for (int i = 0; i < actionSize; i++)
        task.actions.push_back(AcceptAction(socket));
    return task;
}

void SendTask(SOCKET socket, Task task)
{
    SendBool(socket, task.enable);
    SendString(socket, task.name);
    SendString(socket, task.about);
    SendInt(socket, (int)task.triggers.size());
    for (size_t i = 0; i < task.triggers.size(); i++)
        SendTrigger(socket, task.triggers[i]);
    SendInt(socket, (int)task.actions.size());
    for (size_t i = 0; i < task.actions.size(); i++)
        SendAction(socket, task.actions[i]);
}