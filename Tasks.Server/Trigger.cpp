#include "Trigger.h"
#include "Client.h"
#include "Data.h"

#include <fstream>
#include <winsock.h>

void WriteTrigger(std::ofstream& file, Trigger trigger)
{
    WriteBool(file, trigger.enable);
    WriteBool(file, trigger.comlite);
    WriteInt(file, trigger.date.day);
    WriteInt(file, trigger.date.month);
    WriteInt(file, trigger.date.year);
    WriteInt(file, trigger.date.second);
    WriteInt(file, trigger.date.minute);
    WriteInt(file, trigger.date.hour);
    WriteInt(file, trigger.frequancy);
    WriteInt(file, trigger.type);
    WriteInt(file, trigger.monthType);
    WriteInt(file, trigger.prewDay);
    for (int i = 0; i < 7; i++)
        WriteInt(file, trigger.weekDays[i]);
    for (int i = 0; i < 7; i++)
        WriteInt(file, trigger.weekMonthlyDays[i]);
    for (int i = 0; i < 12; i++)
        WriteInt(file, trigger.months[i]);
    for (int i = 0; i < 31; i++)
        WriteInt(file, trigger.monthDays[i]);
}

Trigger ReadTrigger(std::ifstream& file)
{
    Trigger trigger;
    trigger.enable = ReadBool(file);
    trigger.comlite = ReadBool(file);
    trigger.date.day = ReadInt(file);
    trigger.date.month = ReadInt(file);
    trigger.date.year = ReadInt(file);
    trigger.date.second = ReadInt(file);
    trigger.date.minute = ReadInt(file);
    trigger.date.hour = ReadInt(file);
    trigger.frequancy = ReadInt(file);
    trigger.type = (TriggerType)ReadInt(file);
    trigger.monthType = (TriggerMonthType)ReadInt(file);
    trigger.prewDay = ReadInt(file);
    for (int i = 0; i < 7; i++)
        trigger.weekDays[i] = ReadInt(file);
    for (int i = 0; i < 7; i++)
        trigger.weekMonthlyDays[i] = ReadInt(file);
    for (int i = 0; i < 12; i++)
        trigger.months[i] = ReadInt(file);
    for (int i = 0; i < 31; i++)
        trigger.monthDays[i] = ReadInt(file);
    return trigger;
}

Trigger AcceptTrigger(SOCKET socket)
{
    Trigger trigger;
    trigger.comlite = false;
    trigger.prewDay = Now().day - 1;
    trigger.enable = AcceptBool(socket);
    trigger.type = (TriggerType)AcceptInt(socket);
    trigger.monthType = (TriggerMonthType)AcceptInt(socket);
    trigger.time = AcceptDateTime(socket);
    trigger.date = AcceptDateTime(socket);
    trigger.frequancy = AcceptInt(socket);
    for (int i = 0; i < 7; i++)
        trigger.weekDays[i] = AcceptBool(socket);
    for (int i = 0; i < 7; i++)
        trigger.weekMonthlyDays[i] = AcceptBool(socket);
    for (int i = 0; i < 31; i++)
        trigger.monthDays[i] = AcceptBool(socket);
    for (int i = 0; i < 12; i++)
        trigger.months[i] = AcceptBool(socket);
    return trigger;
}

void SendTrigger(SOCKET socket, Trigger trigger)
{
    SendBool(socket, trigger.enable);
    SendInt(socket, trigger.type);
    SendInt(socket, trigger.monthType);
    SendDateTime(socket, trigger.time);
    SendDateTime(socket, trigger.date);
    SendInt(socket, trigger.frequancy);
    for (int i = 0; i < 7; i++)
        SendBool(socket, trigger.weekDays[i]);
    for (int i = 0; i < 7; i++)
        SendBool(socket, trigger.weekMonthlyDays[i]);
    for (int i = 0; i < 31; i++)
        SendBool(socket, trigger.monthDays[i]);
    for (int i = 0; i < 12; i++)
        SendBool(socket, trigger.months[i]);
}