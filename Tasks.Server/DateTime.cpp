#include "DateTime.h"

bool Compare(DateTime left, DateTime right)
{
    return left.year == right.year &&
        left.month == right.month &&
        left.day == right.day &&
        left.hour == right.hour &&
        left.minute == right.minute;
}

bool CompareDate(DateTime left, DateTime right)
{
    return  left.year == right.year &&
        left.month == right.month &&
        left.day == right.day;
}

bool CompareTime(DateTime left, DateTime right)
{
    return left.hour == right.hour &&
        left.minute == right.minute;
}

DateTime Now()
{
    time_t nowStdTime = time(0);
    DateTime nowTime;
    struct tm* timeinfo = new tm;
    localtime_s(timeinfo, &nowStdTime);
    nowTime.year = timeinfo->tm_year + 1900;
    nowTime.month = timeinfo->tm_mon + 1;
    nowTime.day = timeinfo->tm_mday;
    nowTime.hour = timeinfo->tm_hour;
    nowTime.minute = timeinfo->tm_min;
    nowTime.second = timeinfo->tm_sec;
    return nowTime;
}

int WeekDay()
{
    DateTime now = Now();
    std::tm time_in = { 0, 0, 0,
     now.day + 1, now.month, now.year - 1900 };
    std::time_t time_temp = std::mktime(&time_in);
    std::tm time_out;
    localtime_s(&time_out, &time_temp);
    return time_out.tm_wday;
}