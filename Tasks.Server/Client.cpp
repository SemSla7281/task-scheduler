#include "Client.h"
#include "DateTime.h"
#include "Trigger.h"
#include "Action.h"

#include <locale>
#include <winsock.h>

Command AcceptCommand(SOCKET socket)
{
	char buffer[sizeof(int)];
	int rec = recv(socket, buffer, sizeof(int), 0);
	if (rec == SOCKET_ERROR)
		return Command::Disconnect;
	int number = *((int*)(void*)buffer);
	return (Command)number;
}

void SendCommand(SOCKET socket, Command command)
{
	char buffer[sizeof(int)];
	memcpy(buffer, &command, sizeof(int));
	send(socket, buffer, sizeof(int), 0);
}

bool AcceptBool(SOCKET socket)
{
	char buffer[sizeof(bool)];
	recv(socket, buffer, sizeof(bool), 0);
	bool number = *((bool*)(void*)buffer);
	return number;
}

void SendBool(SOCKET socket, bool number)
{
	char buffer[sizeof(bool)];
	memcpy(buffer, &number, sizeof(bool));
	send(socket, buffer, sizeof(bool), 0);
}

int AcceptInt(SOCKET socket)
{
	char buffer[sizeof(int)];
	recv(socket, buffer, sizeof(int), 0);
	int number = *((int*)(void*)buffer);
	return number;
}

void SendInt(SOCKET socket, int number)
{
	char buffer[sizeof(int)];
	memcpy(buffer, &number, sizeof(int));
	send(socket, buffer, sizeof(int), 0);
}

void SendDateTime(SOCKET socket, DateTime dateTime)
{
	if (dateTime.year < 1 || dateTime.year > 9999)
		SendInt(socket, 1);
	else
		SendInt(socket, dateTime.year);
	if (dateTime.month < 1 || dateTime.month > 12)
		SendInt(socket, 1);
	else
		SendInt(socket, dateTime.month);
	if (dateTime.day < 1 || dateTime.day > 31)
		SendInt(socket, 1);
	else
		SendInt(socket, dateTime.day);
	if (dateTime.hour < 0 || dateTime.hour > 23)
		SendInt(socket, 0);
	else
		SendInt(socket, dateTime.hour);
	if (dateTime.minute < 0 || dateTime.minute > 59)
		SendInt(socket, 0);
	else
		SendInt(socket, dateTime.minute);
	if (dateTime.second < 0 || dateTime.second > 59)
		SendInt(socket, 0);
	else
		SendInt(socket, dateTime.second);
}

DateTime AcceptDateTime(SOCKET socket)
{
	DateTime dateTime;
	dateTime.year = AcceptInt(socket);
	dateTime.month = AcceptInt(socket);
	dateTime.day = AcceptInt(socket);
	dateTime.hour = AcceptInt(socket);
	dateTime.minute = AcceptInt(socket);
	dateTime.second = AcceptInt(socket);
	return dateTime;
}

wchar_t* AcceptString(SOCKET socket)
{
	int lenght = AcceptInt(socket);
	int wLenght = lenght / sizeof(wchar_t);
	wchar_t* buffer = new wchar_t[wLenght + 1];
	if (lenght > 0)
		recv(socket, (char*)buffer, lenght, 0);
	buffer[wLenght] = 0;
	return buffer;
}

void SendString(SOCKET socket, wchar_t* string)
{
	int wLenght = lstrlen((LPCWSTR)string);
	int lenght = wLenght * sizeof(wchar_t);
	SendInt(socket, lenght);
	if (lenght > 0)
		send(socket, (char*)string, lenght, 0);
}