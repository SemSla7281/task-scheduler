#include "Action.h"
#include "Data.h"
#include "Client.h"

#include <winsock.h>

void WriteAction(std::ofstream& file, Action action)
{
    WriteBool(file, action.enable);
    WriteString(file, action.path);
    WriteString(file, action.arguments);
    WriteInt(file, action.type);
    WriteBool(file, action.wait);
	WriteBool(file, action.show);
    WriteBool(file, action.asUser);
    WriteString(file, action.user);
    WriteString(file, action.domain);
    WriteString(file, action.password);
}

Action ReadAction(std::ifstream& file)
{
    Action action;
    action.enable = ReadBool(file);
    action.path = ReadString(file);
    action.arguments = ReadString(file);
    action.type = (ActionType)ReadInt(file);
    action.wait = ReadBool(file);
	action.show = ReadBool(file);
    action.asUser = ReadBool(file);
    action.user = ReadString(file);
    action.domain = ReadString(file);
    action.password = ReadString(file);
    return action;
}

Action AcceptAction(SOCKET socket)
{
	Action action;
	action.enable = AcceptBool(socket);
	action.type = (ActionType)AcceptInt(socket);
	action.path = AcceptString(socket);
	action.arguments = AcceptString(socket);
	action.wait = AcceptBool(socket);
	action.show = AcceptBool(socket);
	action.asUser = AcceptBool(socket);
	action.user = AcceptString(socket);
	action.domain = AcceptString(socket);
	action.password = AcceptString(socket);
	return action;
}

void SendAction(SOCKET socket, Action action)
{
	SendBool(socket, action.enable);
	SendInt(socket, (int)action.type);
	SendString(socket, action.path);
	SendString(socket, action.arguments);
	SendBool(socket, action.wait);
	SendBool(socket, action.show);
	SendBool(socket, action.asUser);
	SendString(socket, action.user);
	SendString(socket, action.domain);
	SendString(socket, action.password);
}