#pragma once

#include "DateTime.h"

#include <vector>
#include <fstream>
#include <winsock2.h>

/// <summary>
/// ��� ��������
/// </summary>
enum TriggerType
{
    /// <summary>
    /// �����������
    /// </summary>
    Oneshot = 0,

    /// <summary>
    /// ���
    /// </summary>
    Daily = 1,

    /// <summary>
    /// ������
    /// </summary>
    Weekly = 2,

    /// <summary>
    /// ������
    /// </summary>
    Monthly = 3
};

/// <summary>
/// ��� �������� �� �������
/// </summary>
enum TriggerMonthType
{
    /// <summary>
    /// ��� ������
    /// </summary>
    WeekDays = 0,

    /// <summary>
    /// ��� ������
    /// </summary>
    MonthDays = 1
};

/// <summary>
/// �������
/// </summary>
typedef struct
{
    /// <summary>
    /// ������� �� �������
    /// </summary>
    bool enable;

    /// <summary>
    /// ��� ��������
    /// </summary>
    TriggerType type;

    /// <summary>
    /// ��� �������� �� �������
    /// </summary>
    TriggerMonthType monthType;

    /// <summary>
    /// ����� ��������
    /// </summary>
    DateTime time;

    /// <summary>
    /// ���� ��������
    /// </summary>
    DateTime date;

    /// <summary>
    /// ��� ������ ��������
    /// </summary>
    bool weekDays[7];

    /// <summary>
    /// ��� ������ ��� ������ ��������
    /// </summary>
    bool weekMonthlyDays[7];

    /// <summary>
    /// ������ ��������
    /// </summary>
    bool months[12];

    /// <summary>
    /// ��� ������ ��������
    /// </summary>
    bool monthDays[31];

    /// <summary>
    /// ������� ��������
    /// </summary>
    int frequancy;

    /// <summary>
    /// ���������� ���� ��������
    /// </summary>
    int prewDay;

    /// <summary>
    /// �������� �� �������
    /// </summary>
    int comlite;
} Trigger;

/// <summary>
/// �������� ������� � ���� �������
/// </summary>
/// <param name="file">����</param>
/// <param name="trigger">�������</param>
void WriteTrigger(std::ofstream& file, Trigger trigger);

/// <summary>
/// ��������� ������� �� ����� �������
/// </summary>
/// <param name="file"></param>
/// <returns></returns>
Trigger ReadTrigger(std::ifstream& file);

/// <summary>
/// �������� ������� �� ������� �� ������
/// </summary>
/// <param name="socket">�����</param>
/// <returns>�������</returns>
Trigger AcceptTrigger(SOCKET socket);

/// <summary>
/// ��������� ������� ������� �� ������
/// </summary>
/// <param name="socket">�����</param>
/// <param name="trigger">�������</param>
void SendTrigger(SOCKET socket, Trigger trigger);
 