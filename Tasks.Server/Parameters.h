#pragma once

#include "Task.h"

#include <vector>
#include <fstream>

/// <summary>
/// �������� ������ ��������� �������
/// </summary>
typedef struct
{
    /// <summary>
    /// ������ �����
    /// </summary>
    Tasks* tasks;

    /// <summary>
    /// ������� ��������� ������
    /// </summary>
    HANDLE serviceStopEvent;
} STArgument;

/// <summary>
/// �������� ������ ����������� ��������
/// </summary>
typedef struct
{
    /// <summary>
    /// ������ �����
    /// </summary>
    Tasks* tasks;

    /// <summary>
    /// ����� �������������
    /// </summary>
    SOCKET listenSocket;

    /// <summary>
    /// ������� ��������� ������
    /// </summary>
    HANDLE serviceStopEvent;
} ATArgument;

/// <summary>
/// �������� ������ �������
/// </summary>
typedef struct
{
    /// <summary>
    /// ������ �����
    /// </summary>
    Tasks* tasks;

    /// <summary>
    /// ����� �������
    /// </summary>
    SOCKET clientSocket;

    /// <summary>
    /// ������� ��������� ������
    /// </summary>
    HANDLE serviceStopEvent;
} CTArgument;

/// <summary>
/// �������� ������ ���������� �����
/// </summary>
typedef struct
{
    /// <summary>
    /// ������ �����
    /// </summary>
    Tasks* tasks;

    /// <summary>
    /// ������� ��������� ������
    /// </summary>
    HANDLE serviceStopEvent;
} TETArgument;
