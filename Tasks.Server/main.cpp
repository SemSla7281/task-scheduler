#define WIN32_LEAN_AND_MEAN

#include "Client.h"
#include "Log.h"
#include "Task.h"
#include "Server.h"
#include "Parameters.h"
#include "TaskExecutor.h"

#include <stdlib.h>
#include <stdio.h>
#include <Windows.h>
#include <string>
#include <cstdio>
#include <vector>
#include <time.h>
#include <fstream>
#include <iostream>

#pragma comment (lib, "Ws2_32.lib")

SERVICE_STATUS serviceStatus = { 0 };
SERVICE_STATUS_HANDLE statusHandle = NULL;
HANDLE serviceStopEvent;
Tasks tasks;

int main(int argc, char** argv)
{
    CreateDirectory(L"C:/ProgramData/TasksCreator/", NULL);
    LogInit();
    Log(L"[Function] Main");
    LoadTasks(L"C:/ProgramData/TasksCreator/Data.bin", tasks);
    DWORD serverThreadId;
    DWORD taslExecutorThreadId;
    STArgument stArgument;
    TETArgument tetArgument;
    serviceStopEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    stArgument.tasks = &tasks;
    tetArgument.tasks = &tasks;
    stArgument.serviceStopEvent = serviceStopEvent;
    tetArgument.serviceStopEvent = serviceStopEvent;
    HANDLE serverThread = CreateThread(NULL, 0, ServerThread, &stArgument, 0, &serverThreadId);
    HANDLE taskExecutorThread = CreateThread(NULL, 0, TaskExecutorThread, &tetArgument, 0, &taslExecutorThreadId);
    if (serverThread == NULL)
        Log(L"[Error] Create server thread returned error");
    else if (taskExecutorThread == NULL)
        Log(L"[Error] Create task executor thread returned error");
    if (serverThread != NULL)
        WaitForSingleObject(serverThread, INFINITE);
    if (taskExecutorThread != NULL)
        WaitForSingleObject(taskExecutorThread, INFINITE);
    Log(L"[Return] 0");
    return 0;
}
