#pragma once

#include "Action.h"
#include "Trigger.h"
#include "Task.h"

#include <windef.h>

BOOL ExecuteAction(const Action& action);
BOOL ExecuteTask(Task task, Trigger trigger);
DWORD WINAPI TaskExecutorThread(LPVOID lpParam);