#pragma once

#include "Data.h"

#include <fstream>
#include <winsock2.h>

/// <summary>
/// ��� ��������
/// </summary>
enum ActionType
{
    /// <summary>
    /// ����������� ����
    /// </summary>
    Binary,

    /// <summary>
    /// ������
    /// </summary>
    Script
};

/// <summary>
/// ��������
/// </summary>
typedef struct
{
    /// <summary>
    /// �������� �� ��������
    /// </summary>
    bool enable;

    /// <summary>
    /// ���� � ����� ��������
    /// </summary>
    wchar_t* path;

    /// <summary>
    /// ��������� ��������
    /// </summary>
    wchar_t* arguments;

    /// <summary>
    /// ��� ��������
    /// </summary>
    ActionType type;

    /// <summary>
    /// ������� �� ��������
    /// </summary>
    bool wait;

    /// <summary>
    /// ���������� �� ���� ��������
    /// </summary>
    bool show;

    /// <summary>
    /// ��������� �� �������� �� ����� ������������
    /// </summary>
    bool asUser;

    /// <summary>
    /// ��� ������������
    /// </summary>
    wchar_t* user;

    /// <summary>
    /// ����� Windows
    /// </summary>
    wchar_t* domain;

    /// <summary>
    /// ������ ������������
    /// </summary>
    wchar_t* password;
} Action;

/// <summary>
/// ������ �������� � ���� �������
/// </summary>
/// <param name="file">����</param>
/// <param name="action">��������</param>
void WriteAction(std::ofstream& file, Action action);

/// <summary>
/// ��������� �������� �� ����� �������
/// </summary>
/// <param name="file">����</param>
/// <returns>��������</returns>
Action ReadAction(std::ifstream& file);

/// <summary>
/// �������� �������� �� ������� �� ������
/// </summary>
/// <param name="socket">�����</param>
/// <returns>��������</returns>
Action AcceptAction(SOCKET socket);

/// <summary>
/// ��������� �������� ������� �� ������
/// </summary>
/// <param name="socket">�����</param>
/// <param name="socket">��������</param>
void SendAction(SOCKET socket, Action action);